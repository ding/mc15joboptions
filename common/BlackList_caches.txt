AtlasProduction, 19.2.3.8, MadGraph 
AtlasProduction, 19.2.3.9, Powheg
AtlasProduction, 19.2.3.10, Powheg
MCProd,          19.2.3.10.2, EvtGen
MCProd,          19.2.3.10.3, EvtGen
AtlasProduction, 19.2.4.1, EvtGen
MCProd,          19.2.4.3.1, MadGraph
AtlasProduction, 19.2.4.4, MadGraph
AtlasProduction, 19.2.4.5, MadGraph
AtlasProduction, 19.2.4.11, Powheg, https://its.cern.ch/jira/browse/AGENE-1058
MCProd,          19.2.4.16.4, Pythia8pdf6plugin, pdf6plugin did not work, so still LHAPDF5 functionality
AtlasProduction, 19.2.5.1, Herwigpp
AtlasProduction, 19.2.5.2, Herwigpp
AtlasProduction, 19.2.5.3, Herwigpp
AtlasProduction, 19.2.5.4, Herwigpp
AtlasProduction, 19.2.5.5, Herwigpp
AtlasProduction, 19.2.5.6, Herwigpp
AtlasProduction, 19.2.5.7, Herwigpp
AtlasProduction, 19.2.5.8, Herwigpp
AtlasProduction, 19.2.5.9, Herwigpp
AtlasProduction, 19.2.5.10, Herwigpp
AtlasProduction, 19.2.5.11, Herwigpp
AtlasProduction, 19.2.5.12, Herwigpp
AtlasProduction, 19.2.5.13, Herwigpp
AtlasProduction, 19.2.5.14, Herwigpp
MCProd,          19.2.5.14.3, MadGraph,aMC@NLO, issue of afs access in case of aMC@NLO and MG5 which uses lhapdf and if they compile their process on-the-fly
MCProd,          19.2.4.10.2, MadGraph
MCProd,          19.2.4.16.3, MadGraph
MCProd,          19.2.4.16.4, MadGraph
MCProd,          19.2.4.16.5, MadGraph
MCProd,          19.2.4.16.6, MadGraph
MCProd,          19.2.4.19.2, MadGraph
AtlasProduction, 19.2.4.19,   MadGraph
MCProd,          19.2.4.8.1,  MadGraph
MCProd,          19.2.4.9.1,  MadGraph
MCProd,          19.2.4.9.3,  MadGraph
MCProd,          19.2.5.1.1,  MadGraph
MCProd,          19.2.5.1.2,  MadGraph
AtlasProduction, 19.2.5.10,   MadGraph
MCProd,          19.2.5.11.1, MadGraph
MCProd,          19.2.5.11.2, MadGraph
AtlasProduction, 19.2.5.11,   MadGraph
MCProd,          19.2.5.12.1, MadGraph
MCProd,          19.2.5.12.3, MadGraph
AtlasProduction, 19.2.5.12,   MadGraph
AtlasProduction, 19.2.5.1,    MadGraph
AtlasProduction, 19.2.5.2,    MadGraph
MCProd,          19.2.5.3.1,  MadGraph
MCProd,          19.2.5.3.2,  MadGraph
MCProd,          19.2.5.3.3,  MadGraph
AtlasProduction, 19.2.5.3,    MadGraph
AtlasProduction, 19.2.5.4,    MadGraph
AtlasProduction, 19.2.5.5,    MadGraph
AtlasProduction, 19.2.5.6,    MadGraph
MCProd,          19.2.5.7.1,  MadGraph
AtlasProduction, 19.2.5.7,    MadGraph
MCProd,          19.2.5.8.1,  MadGraph
AtlasProduction, 19.2.5.8,    MadGraph
MCProd,          19.2.5.9.1,  MadGraph
MCProd,          19.2.5.9.2,  MadGraph
AtlasProduction, 19.2.5.9,    MadGraph
MCProd,          19.2.5.16.1, Sherpa,    only for multicore running tests 
MCProd,          20.7.9.1.1,     , only for Tauolapp validation - AGENE1324
MCProd,          19.2.5.20.3, Sherpa,    only for multicore running tests
MCProd,          19.2.5.31.1, Herwig7,   the recompiletion of ThePEG forgotten
MCProd,          20.7.9.9.8,  Herwig7, problem in the transform that does not allow for uncompresed inputs
MCProd,          20.7.9.9.8,  Powheg, problem in the transform that does not allow for uncompresed inputs
AtlasProduction, 19.2.5.33,  , ATLMCPROD-5791 ATLASJT-375
MCProd,          19.2.5.31.1, , ATLMCPROD-5791 ATLASJT-375
MCProd,          19.2.5.32.1, , ATLMCPROD-5791 ATLASJT-375
MCProd,          19.2.5.32.2, , ATLMCPROD-5791 ATLASJT-375
MCProd,          19.2.5.33.1, , ATLMCPROD-5791 ATLASJT-375
MCProd,          19.2.5.33.2, , ATLMCPROD-5791 ATLASJT-375
MCProd,          20.7.9.9.22, MadGraph, MadPath not set correctly
AtlasProduction, 19.2.5.37, MadSpin
AthGeneration,   21.6.0,  , Duplication in event numbering
AthGeneration,   21.6.1,  , Duplication in event numbering
AthGeneration,   21.6.2,  , Duplication in event numbering
AthGeneration,   21.6.3,  , Duplication in event numbering
AthGeneration,   21.6.4,  , Duplication in event numbering
AthGeneration,   21.6.5,  , Duplication in event numbering
AthGeneration,   21.6.6,  , Duplication in event numbering
AthGeneration,   21.6.7,  , Duplication in event numbering
AthGeneration,   21.6.8,  , Duplication in event numbering
AthGeneration,   21.6.9,  , Duplication in event numbering
AthGeneration,   21.6.10,  , Duplication in event numbering
AthGeneration,   21.6.11,  , Duplication in event numbering
AthGeneration,   21.6.12,  , Duplication in event numbering
AthGeneration,   21.6.13,  , Duplication in event numbering
AthGeneration,   21.6.14,  , Duplication in event numbering
AthGeneration,   21.6.15,  , Duplication in event numbering
AthGeneration,   21.6.16, Herwig7 , problematic fix for matchbox+herwig7 muons that travel large distances

