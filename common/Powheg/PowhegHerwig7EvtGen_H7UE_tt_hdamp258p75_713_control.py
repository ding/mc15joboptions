#---------------------------------------------------------------------------
# Title: PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_713_control.py
#-----------------------------------------------------------------------------

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.description = 'PowhegBox+Herwig7 7.1 ttbar production with Powheg hdamp equal top mass, H7.1-Default tune, zero lepton filter, with EvtGen'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.tune        = "H7.1-Default"
evgenConfig.contact     = [ 'aknue@cern.ch']


#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("MC15JobOptions/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# Semi-leptonic decay filter
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# Depending on the DSID, set up MET/HT filters
thisDSID = runArgs.runNumber
MET200_filter   = [407357]
MET300_filter   = [407358]
MET400_filter   = [407359]

if thisDSID in MET200_filter:
    include('MC15JobOptions/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 200*GeV
    filtSeq.MissingEtFilterUpperCut.METCut = 300*GeV

if thisDSID in MET300_filter:
    include('MC15JobOptions/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 300*GeV
    filtSeq.MissingEtFilterUpperCut.METCut = 400*GeV

if thisDSID in MET400_filter:
    include('MC15JobOptions/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 400*GeV

HTcuts = {
    407354: (1500, 9000),
    407355: (1000, 1500),
    407356: ( 600, 1000),
    410432: ( 500, 1000),
    410433: (1000, 1500),
    410446: (1500, 9000),
    411338: (1000, 1500),
    411339: (1500, 9000)    
}

# Provide the HT cuts in GeV
def setupStandardHTFilter(HTrange):
    HTmin,HTmax = HTrange
    include('MC15JobOptions/HTFilter.py')
    htfilt = filtSeq.HTFilter
    htfilt.MinJetPt = 35.*GeV # Min pT to consider jet in HT
    htfilt.MaxJetEta = 2.5 # Max eta to consider jet in HT
    htfilt.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
    htfilt.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
    htfilt.MaxLeptonEta = 2.5 # Max eta to consider muon in HT
    filtSeq.HTFilter.MinHT = HTmin*GeV # Min HT to keep event
    filtSeq.HTFilter.MaxHT = HTmax*GeV # Max HT to keep event

if thisDSID in HTcuts.keys():
    setupStandardHTFilter(HTcuts[thisDSID])

isAllHad = thisDSID in [410432,410433,410446,411338,411339]
if isAllHad:
    filtSeq.Expression = "((not TTbarWToLeptonFilter) and HTFilter)"
