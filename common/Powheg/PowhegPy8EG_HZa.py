#--------------------------------------------------------------
# Use LHE files as input
#--------------------------------------------------------------

# In 20.7.9.9.6, LHE merging means this is no longer needed
#evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Defining the function to extract parameters
#--------------------------------------------------------------
dict_pdgIds = {}
dict_pdgIds["b"]   = 5
dict_pdgIds["mu"]  = 13
dict_pdgIds["e"]   = 11
dict_pdgIds["tau"] = 15
dict_pdgIds["g"]   = 21
dict_pdgIds["y"]   = 22

def getParameters():
    import re

    #--- Read parts of the job option
    #--- Assumes the form MC15.111111.PowhegPy8EG_ggH_H125_Z_XX_aM_YY.py
    #--- allowing also    MC15.111111.PowhegPy8EG_..._AZNLO_ggH_H125_Z_XX_aM_YY.py
    jonamelist = str(runArgs.jobConfig[0]).replace(".py", "").split("_") 
    tune=0
    count=0
    for substr in jonamelist:
        if "AZNLO" in substr:
            tune = count
            break
        count += 1
    process = jonamelist[tune+1]
    ma = float(jonamelist[tune+5].split("a")[-1].replace("p", "."))
    decayChanZ = str(jonamelist[tune+4])
    decayChanA = str(jonamelist[tune+6])
    partFilter = None
    if len(jonamelist)>(tune+7):
      partFilter = str(jonamelist[tune+5])

    #--- list of decays, e.g. [mu, tau] for 2mu2tau
    decayProductsZ = []
    decayProductsA = []
    decayChan = decayChanZ+decayChanA
    for part in dict_pdgIds.keys():
        decay = re.findall("[1-4]%s" % part, decayChanZ)
        if len(decay)>0:
          print "Decay of Z = ", decay
          decayProductsZ.append(decay[0][1:]) # remove the number in front of the letter
          print "Decay Products Z = ", decayProductsZ
        decay = re.findall("[1-4]%s" % part, decayChanA)
        if len(decay)>0:
          print "Decay of A= ", decay
          decayProductsA.append(decay[0][1:]) # remove the number in front of the letter
          print "Decay Products A = ", decayProductsA
    return process, ma, decayChan, decayProductsZ, decayProductsA, partFilter

process, ma, decayChan, decayProductsZ, decayProductsA, partFilter = getParameters()
print "Parameters: "
print process, ma, decayChan, decayProductsZ, decayProductsA, partFilter

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')


#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if process=="ggH":
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']
elif process=="VBF":
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']


#--------------------------------------------------------------
# Higgs->aa at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',
                            '35:m0 = 125',
                            '35:mWidth = 0.00407',
                            '35:doForceWidth = off',
                            '35:onMode = off',
                            '35:onIfMatch = 36 23', # h->Za
                            '36:onMode = off',
                            '23:onMode = off',
                            ]


#--------------------------------------------------------------
# a->XX at Pythia8
#--------------------------------------------------------------

if len(decayProductsZ + decayProductsA)==2: # a->2X, Z->2Y
    sign = -1
    genSeq.Pythia8.Commands += [
                                '23:mMin = 2.0',
                                '23:mMax = %.1f' % (125 - ma),
                                '23:onIfAny = %d %d' % (dict_pdgIds[decayProductsZ[0]], sign*dict_pdgIds[decayProductsZ[0]]),
                                '36:onIfAny = %d %d' % (dict_pdgIds[decayProductsA[0]], sign*dict_pdgIds[decayProductsA[0]]),
                                '36:m0 %.1f' % ma, #scalar mass
                                '36:mMin %.1f' % (ma-0.5), #scalar mass
                                '36:mMax %.1f' % (ma+0.5), #scalar mass
                                '36:mWidth 0.001', # narrow width
                                '36:tau0 0', #scalarlife time
                                ]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]

if process=="ggH" and (decayChan=="2mu2b"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->Za->(Z->2mu, a->2b) mh=125 GeV"
    evgenConfig.process     = "ggH H->Za->(Z->2mu, a->2b)"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ljiljana.morvaj@cern.ch' ]

if process=="VBF" and (decayChan=="2mu2b"):
    evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->Za->(Z->2mu, a->2b) mh=125 GeV"
    evgenConfig.process     = "VBF H->Za->(Z->2mu, a->2b)"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ljiljana.morvaj@cern.ch' ]

if process=="ggH" and (decayChan=="2e2b"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->Za->(Z->2e, a->2b) mh=125 GeV"
    evgenConfig.process     = "ggH H->Za->(Z->2e, a->2b)"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ljiljana.morvaj@cern.ch' ]

if process=="VBF" and (decayChan=="2e2b"):
    evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->Za->(Z->2e, a->2b) mh=125 GeV"
    evgenConfig.process     = "VBF H->Za->(Z->2e, a->2b)"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ljiljana.morvaj@cern.ch' ]

if process=="ggH" and (decayChan=="2b2mu"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune, H->Za->(Z->2b, a->2mu) mh=125 GeV"
    evgenConfig.process     = "ggH H->Za->(Z->2b, a->2mu)"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ljiljana.morvaj@cern.ch' ]

if process=="VBF" and (decayChan=="2b2mu"):
    evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->Za->(Z->2b, a->2mu) mh=125 GeV"
    evgenConfig.process     = "VBF H->Za->(Z->2b, a->2mu)"
    evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ljiljana.morvaj@cern.ch' ]
