from MadGraphControl.MadGraphUtils import *

# for details about the HC model, please check here: http://feynrules.irmp.ucl.ac.be/attachment/wiki/HiggsCharacterisation/README

# General settings
nevents=5500
gridpack_dir=None
gridpack_mode=True
#cluster_type='condor'
#cluster_queue='tomorrow'
cluster_type='lsf'
cluster_queue='8nh'
mode=2
njobs=132
runName='run_01'

tx0jb_4fl = [346533, 346534, 346535, 346536, 346537, 346538, 346539, 346540, 346541, 346542, 346543, 346544, 346676, 346677, 346764, 346765, 346766, 346767, 346772, 346773, 346774, 346775, 346799]

tx0jb_4fl_CPalpha_0  = [346676, 346677, 346799]
tx0jb_4fl_CPalpha_15 = [346533, 346539]
tx0jb_4fl_CPalpha_30 = [346534, 346540]
tx0jb_4fl_CPalpha_45 = [346535, 346541]
tx0jb_4fl_CPalpha_60 = [346536, 346542]
tx0jb_4fl_CPalpha_75 = [346537, 346543]
tx0jb_4fl_CPalpha_90 = [346538, 346544]

tx0jb_yt_minus1_CPalpha_0 = [346764, 346772]
tx0jb_yt_plus0p5_CPalpha_0 = [346765, 346773]
tx0jb_yt_plus2_CPalpha_0 = [346766, 346774]
tx0jb_yt_plus2_CPalpha_45 = [346767, 346775]

if runArgs.runNumber in tx0jb_4fl:
    
    mgproc="generate p p > x0 t1 b1 j $$ w+ w- [QCD]"
    name='tx0jb_4fl'
    process="pp>tx0bj"
    topdecay='''decay t > w+ b, w+ > all all
    decay t~ > w- b~, w- > all all'''
    gridpack_mode=True
    gridpack_dir='madevent/'

    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model HC_NLO_X0_UFO-4Fnoyb
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define t1 = t t~
    define b1 = b b~
    """+mgproc+"""
    output -f
    """)
    fcard.close()

    extras = {'pdlabel'        : "'lhapdf'",
              'lhaid'          : 260400,
              'parton_shower'  :'PYTHIA8',
              'reweight_scale' : 'True',
              'reweight_PDF'   : 'True',
              'PDF_set_min'    : 260401,
              'PDF_set_max'    : 260500,
              'bwcutoff'       : 50.,
              'dynamical_scale_choice' : 3 }
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)


runName = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack=gridpack_dir)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)


madspin_card_loc='madspin_card.dat'

if not hasattr(runArgs, 'inputGenConfFile'):
    fMadSpinCard = open('madspin_card.dat','w')
    fMadSpinCard.write('import Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir MadSpin\n')
else:
    os.unlink(gridpack_dir+'Cards/madspin_card.dat')
    fMadSpinCard = open(gridpack_dir+'Cards/madspin_card.dat','w')
    fMadSpinCard.write('import '+gridpack_dir+'Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+gridpack_dir+'MadSpin\n')
    fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
fMadSpinCard.write('''set Nevents_for_max_weigth 2000 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event (default: 400)
'''+topdecay+'''
launch''')
fMadSpinCard.close()

# do not set default, otherwise will not see an error if a DSID is not defined
mod_paramcard_name = ''

## the generate call differs for the cases of using a nominal(SM) or modified(BSM) param card
if runArgs.runNumber in tx0jb_4fl_CPalpha_0:
    mod_paramcard_name = 'param_card_CPalpha_0.dat'
if runArgs.runNumber in tx0jb_4fl_CPalpha_15:
    mod_paramcard_name = 'param_card_CPalpha_15.dat'
if runArgs.runNumber in tx0jb_4fl_CPalpha_30:
    mod_paramcard_name = 'param_card_CPalpha_30.dat'
if runArgs.runNumber in tx0jb_4fl_CPalpha_45:
    mod_paramcard_name = 'param_card_CPalpha_45.dat'
if runArgs.runNumber in tx0jb_4fl_CPalpha_60:
    mod_paramcard_name = 'param_card_CPalpha_60.dat'
if runArgs.runNumber in tx0jb_4fl_CPalpha_75:
    mod_paramcard_name = 'param_card_CPalpha_75.dat'
if runArgs.runNumber in tx0jb_4fl_CPalpha_90:
    mod_paramcard_name = 'param_card_CPalpha_90.dat'
if runArgs.runNumber in tx0jb_yt_minus1_CPalpha_0:
    mod_paramcard_name = 'param_card_yt_minus1_CPalpha_0.dat'
if runArgs.runNumber in tx0jb_yt_plus0p5_CPalpha_0:
    mod_paramcard_name = 'param_card_yt_plus0p5_CPalpha_0.dat'
if runArgs.runNumber in tx0jb_yt_plus2_CPalpha_0:
    mod_paramcard_name = 'param_card_yt_plus2_CPalpha_0.dat'
if runArgs.runNumber in tx0jb_yt_plus2_CPalpha_45:
    mod_paramcard_name = 'param_card_yt_plus2_CPalpha_45.dat'


mod_paramcard = subprocess.Popen(['get_files','-data',mod_paramcard_name]).communicate()
if not os.access(mod_paramcard_name, os.R_OK):
    print 'ERROR: Could not get param card'
    raise RuntimeError("parameter card '%s' missing!"%mod_paramcard_name)

generate(run_card_loc='run_card.dat',param_card_loc=mod_paramcard_name,mode=mode,njobs=njobs,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed,required_accuracy=0.001,cluster_type=cluster_type,cluster_queue=cluster_queue)


### common to all jobs
outputDS=arrange_output(run_name=runName,proc_dir=process_dir,lhe_version=3,saveProcDir=True)

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

evgenConfig.description = 'aMcAtNlo tx0jb'
evgenConfig.keywords+=['Higgs', 'tHiggs']
evgenConfig.inputfilecheck = outputDS
evgenConfig.contact = ['maria.moreno.llacer@cern.ch', 'aknue@cern.ch' ]
runArgs.inputGeneratorFile=outputDS

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py") 
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")

