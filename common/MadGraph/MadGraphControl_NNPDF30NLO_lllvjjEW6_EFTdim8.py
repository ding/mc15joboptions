from MadGraphControl.MadGraphUtils import *

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]

evgenConfig.keywords = ['SM', 'diboson', 'VBS', 'WW', 'sameSign', 'electroweak', '2lepton', '2jet']
evgenConfig.contact = ['stefanie.todt@cern.ch']

gridpack_dir='madevent/'

# ---------------------------------------------------------------------------
# Process type based on runNumber:
# ---------------------------------------------------------------------------
if runArgs.runNumber == 367491:
    runName = 'lvlljj_ss_EW6_LSMT_sm'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 S0=0 S1=0 S2=0 T0=0 T1=0 T2=0 T3=0 T4=0 T5=0 T6=0 T7=0 T8=0 T9=0 M0=0 M1=0 M2=0 M3=0 M4=0 M5=0 M6=0 M7=0
add process p p > l- vl~ l- vl~ j j QCD=0 S0=0 S1=0 S2=0 T0=0 T1=0 T2=0 T3=0 T4=0 T5=0 T6=0 T7=0 T8=0 T9=0 M0=0 M1=0 M2=0 M3=0 M4=0 M5=0 M6=0 M7=0
"""
elif runArgs.runNumber == 367492:
    runName = 'lvlljj_ss_EW6_LSMT_S02_8_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 S0^2==1 S2=0
add process p p > l- vl~ l- vl~ j j QCD=0 S0^2==1 S2=0
add process p p > l+ vl l+ vl j j QCD=0 S2^2==1 S0=0
add process p p > l- vl~ l- vl~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367493:
    runName = 'lvlljj_ss_EW6_LSMT_S02_8_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 S0^2==2 S2=0
add process p p > l- vl~ l- vl~ j j QCD=0 S0^2==2 S2=0
add process p p > l+ vl l+ vl j j QCD=0 S2^2==2 S0=0
add process p p > l- vl~ l- vl~ j j QCD=0 S2^2==2 S0=0
add process p p > l+ vl l+ vl j j QCD=0 S0^2==1 S2^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 S0^2==1 S2^2==1
"""
elif runArgs.runNumber == 367494:
    runName = 'lvlvjj_ss_EW6_LSMT_S1_20_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 S1^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367495:
    ruName = 'lvlvjj_ss_EW6_LSMT_S1_20_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 S1^2==2
add process p p > l- vl~ l- vl~ j j QCD=0 S1^2==2
"""
elif runArgs.runNumber == 367496:
    runName = 'lvlvjj_ss_EW6_LSMT_S02_8_S1_20_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 S0^2==1 S1^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 S0^2==1 S1^2==1
add process p p > l+ vl l+ vl j j QCD=0 S2^2==1 S1^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 S2^2==1 S1^2==1
"""
elif runArgs.runNumber == 367497:
    runName = 'lvlvjj_ss_EW6_LSMT_M0_6_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M0^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367498:
    runName = 'lvlvjj_ss_EW6_LSMT_M0_6_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M0^2==2
add process p p > l- vl~ l- vl~ j j QCD=0 M0^2==2
"""
elif runArgs.runNumber == 367499:
    runName = 'lvlvjj_ss_EW6_LSMT_M1_10_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M1^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367500:
    runName = 'lvlvjj_ss_EW6_LSMT_M1_10_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M1^2==2
add process p p > l- vl~ l- vl~ j j QCD=0 M1^2==2
"""
elif runArgs.runNumber == 367501:
    runName = 'lvlvjj_ss_EW6_LSMT_M7_13_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M7^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 M7^2==1
"""
elif runArgs.runNumber == 367502:
    runName = 'lvlvjj_ss_EW6_LSMT_M7_13_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M7^2==2
add process p p > l- vl~ l- vl~ j j QCD=0 M7^2==2
"""
elif runArgs.runNumber == 367503:
    runName = 'lvlvjj_ss_EW6_LSMT_M0_6_M1_10_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M0^2==1 M1^2==1 
add process p p > l- vl~ l- vl~ j j QCD=0 M0^2==1 M1^2==1 
"""
elif runArgs.runNumber == 367504:
    runName = 'lvlvjj_ss_EW6_LSMT_M0_6_M7_13_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M0^2==1 M7^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 M0^2==1 M7^2==1
"""
elif runArgs.runNumber == 367505:
    runName = 'lvlvjj_ss_EW6_LSMT_M1_10_M7_13_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 M1^2==1 M7^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 M1^2==1 M7^2==1
"""
elif runArgs.runNumber == 367506:
    runName = 'lvlvjj_ss_EW6_LSMT_T0_0p60_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T0^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367507:
    runName = 'lvlvjj_ss_EW6_LSMT_T0_0p60_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T0^2==2
add process p p > l- vl~ l- vl~ j j QCD=0 T0^2==2
"""
elif runArgs.runNumber == 367508:
    runName = 'lvlvjj_ss_EW6_LSMT_T1_0p30_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T1^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367509:
    runName = 'lvlvjj_ss_EW6_LSMT_T1_0p30_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T1^2==2
add process p p > l- vl~ l- vl~ j j QCD=0 T1^2==2
"""
elif runArgs.runNumber == 367510:
    runName = 'lvlvjj_ss_EW6_LSMT_T2_1_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T2^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367511:
    runName = 'lvlvjj_ss_EW6_LSMT_T2_1_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T2^2==2
add process p p > l- vl~ l- vl~ j j QCD=0 T2^2==2
"""
elif runArgs.runNumber == 367512:
    runName = 'lvlvjj_ss_EW6_LSMT_T0_0p60_T1_0p30_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T0^2==1 T1^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T0^2==1 T1^2==1
"""
elif runArgs.runNumber == 367513:
    runName = 'lvlvjj_ss_EW6_LSMT_T0_0p60_T2_1_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T0^2==1 T2^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T0^2==1 T2^2==1
"""
elif runArgs.runNumber == 367514:
    runName = 'lvlvjj_ss_EW6_LSMT_T1_0p30_T2_1_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T1^2==1 T2^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T1^2==1 T2^2==1
"""
else:
    raise RuntimeError(
        "runNumber %i not recognised in these jobOptions." % runArgs.runNumber)

evgenConfig.description = description

# ---------------------------------------------------------------------------
# write MG5 Proc card
# ---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat', 'w')
fcard.write("""
import model SM_LSMT_Ind5_UFO
define l+ = e+ mu+ ta+
define  vl = ve vm vt
define l- = e- mu- ta-
define vl~ = ve~ vm~ vt~
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
""" + mgproc + """
output -f
""")
fcard.close()

# ----------------------------------------------------------------------------
# Random Seed
# ----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs, 'randomSeed'):
    randomSeed = runArgs.randomSeed

# ----------------------------------------------------------------------------
# Beam energy
# ----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# ---------------------------------------------------------------------------
# Number of Events
# ---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs, 'maxEvents') and runArgs.maxEvents > 0:
    nevents = int(int(runArgs.maxEvents) * safefactor)
else:
    nevents = int(5000 * safefactor)

#Fetch default LO run_card.dat and set parameters
extras = {
    'gridpack': '.true.',
    'pdlabel': "'lhapdf'", 
    'lhaid': 260000, 
    'dynamical_scale_choice': 2, 
    'maxjetflavor': 5, 
    'asrwgtflavor': 5, 
    'auto_ptj_mjj': False, 
    'cut_decays': True, 
    'ptl': 4.0, 
    'ptj': 15.0, 
    'ptb': 15.0, 
    'drbb': 0.2, 
    'etal': 3.0, 
    'etaj': 5.5, 
    'etab': 5.5, 
    'mmll': 0.0, 
    'dral': 0.1, 
    'drbj': 0.2, 
    'drll': 0.2, 
    'drbl': 0.2, 
    'drjl': 0.2, 
    'drjj': 0.2,
    'use_syst': True, 
    'systematics_program': 'systematics',  
    'systematics_arguments': "['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1,1,2,3,4', '--pdf=errorset,NNPDF30_nlo_as_0119@0,NNPDF30_nlo_as_0117@0,CT14nlo@0,MMHT2014nlo68clas118@0,PDF4LHC15_nlo_30_pdfas']"}

process_dir = new_process(grid_pack=gridpack_dir)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat', xqcut=0,
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy)
modify_run_card(run_card='run_card.dat',
                run_card_backup='run_card_backup.dat',
                settings=extras)
print_cards()

param_card_name = 'param_card_' + str(runArgs.runNumber) + '.dat' 

generate(required_accuracy=0.001,run_card_loc='run_card.dat',
         param_card_loc=param_card_name,mode=0,njobs=1,cluster_type=None,
         cluster_queue=None,proc_dir=process_dir,run_name=runName,grid_pack=True,
         gridpack_compile=False,gridpack_dir=gridpack_dir,
         nevents=nevents,random_seed=runArgs.randomSeed)

# replaced output_suffix+'._00001.events.tar.gz' with runArgs.outputTXTFile
arrange_output(run_name=runName,proc_dir=process_dir,
               outputDS=runArgs.outputTXTFile,lhe_version=3,saveProcDir=True)

## Provide config information for Pythia8
runArgs.inputGeneratorFile=runArgs.outputTXTFile

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
# fix for VBS processes (requires version>=8.230)
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch=1"]
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxFudge=1"]
genSeq.Pythia8.Commands += ["SpaceShower:MEcorrections=off"]
genSeq.Pythia8.Commands += ["TimeShower:pTmaxMatch=1"]
genSeq.Pythia8.Commands += ["TimeShower:pTmaxFudge=1"]
genSeq.Pythia8.Commands += ["TimeShower:MEcorrections=off"]
genSeq.Pythia8.Commands += ["TimeShower:globalRecoil=on"]
genSeq.Pythia8.Commands += ["TimeShower:limitPTmaxGlobal=on"]
genSeq.Pythia8.Commands += ["TimeShower:nMaxGlobalRecoil=1"]
genSeq.Pythia8.Commands += ["TimeShower:globalRecoilMode=2"]
genSeq.Pythia8.Commands += ["TimeShower:nMaxGlobalBranch=1"]
genSeq.Pythia8.Commands += ["TimeShower:weightGluonToQuark=1"]
genSeq.Pythia8.Commands += ["Check:epTolErr=1e-2"]
include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")
