import sys

new_file_name = sys.argv[1]

new_file = open(new_file_name, 'r')

for current_line in new_file.splitlines():
    currentlength = len(current_line.split(','))
    if currentlength != 3 and currentlength != 4:
        print "The following line does not have the proper number of arguments"
        print current_line

    if ':' in current_line:
        print "The following line contains a forbidden character"
        print curreint_line
