evgenConfig.description = "Pythia8 gammajet sample. jetjet, gamma+jet events with at least one hard process or parton shower photon with 2500 < pT < 3000 (GeV)"
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.minevents = 100

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qq2qq = on",
                            "HardQCD:qqbar2gg = on",
                            "HardQCD:qqbar2qqbarNew = on",
                            "PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 1625"] 	# use 0.65 * Ptmin

include("MC15JobOptions/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.Ptmin = 2500000.
filtSeq.DirectPhotonFilter.Ptmax = 3000000.
