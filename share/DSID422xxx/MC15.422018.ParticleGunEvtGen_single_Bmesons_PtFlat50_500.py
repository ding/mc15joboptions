evgenConfig.description = "Single charged B+ with flat phi, eta in [-3.0, 3.0], and flat pT [50,500] GeV"
evgenConfig.keywords = ["singleParticle","Bplus"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (521)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[50000,500000], eta=[-3.0, 3.0],mass=5.2791700e+03)


evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.blackList=[]
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"
genSeq.EvtInclusiveDecay.isfHerwig=True
