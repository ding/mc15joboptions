#################################################################################
# job options fragment for inclusive W^{+}->nutau10->mu4mu2mu1 for run2
#################################################################################
# Not only qq->W production, qq->Wg and gq->Wq production channels are considered
# in this fragments.
# thresholds: mu1>4GeV, mu2>2GeV, mu3>1GeV and tau>10GeV
#################################################################################

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
evgenConfig.description  = "PosW->nutau->3mu production"
evgenConfig.keywords     = [ "W", "muon", "tau", "BSM" ]
evgenConfig.minevents    = 500
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "pp>W>nutau>3munu"

genSeq.Pythia8.Commands       += [ 'WeakSingleBoson:ffbar2W = on' ]
genSeq.Pythia8.Commands       += [ 'WeakBosonAndParton:qqbar2Wg = on' ]
genSeq.Pythia8.Commands       += [ 'WeakBosonAndParton:qg2Wq = on' ]

genSeq.Pythia8.Commands += [ ' 24:onMode = 3' ]
genSeq.Pythia8.Commands += [ ' 24:onIfMatch = -15 16' ]
genSeq.Pythia8.Commands += [ ' 15:onMode = 2' ]
genSeq.Pythia8.Commands += [ ' 15:addChannel = 3 1.0 0 13 -13 13' ]

#Add the Filters:
from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter
genSeq += TripletChainFilter()

TripletChainFilter = genSeq.TripletChainFilter
TripletChainFilter.NTriplet       = 1
TripletChainFilter.PdgId1         = 13
TripletChainFilter.PdgId2         = -13
TripletChainFilter.PdgId3         = -13
TripletChainFilter.NStep1         = 1
TripletChainFilter.NStep2         = 1
TripletChainFilter.NStep3         = 1
TripletChainFilter.PtMin1         = 4000
TripletChainFilter.PtMin2         = 2000
TripletChainFilter.PtMin3         = 1000
TripletChainFilter.EtaMax1        = 3.0
TripletChainFilter.EtaMax2        = 3.0
TripletChainFilter.EtaMax3        = 3.0
TripletChainFilter.TripletPdgId   = -15
TripletChainFilter.TripletPtMin   = 10000
TripletChainFilter.TripletEtaMax  = 100
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000


