##############################################################
# Sample for B to lepton-lepton X study
# Pythia8B_i generation with ISR/FSR for decay:
#  Bsbar -> phi mumu 
##############################################################
f = open("Bsbar_phi_MUMU_USER.DEC","w") 
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_phi phi\n")
f.write("Decay  my_phi\n")
f.write("1.0000   K+ K-   VSS;\n")
f.write("Enddecay\n")
f.write("Decay anti-B_s0\n")
f.write("1.0000   my_phi  mu+ mu-  PHOTOS BTOSLLALI;\n")
f.write("Enddecay\n") 
f.write("End\n") 
f.close() 
##############################################################
evgenConfig.description = "Exclusive Bsbar -> phi mumu  production"
evgenConfig.keywords    = ["exclusive", "bottom", "2muon"]
evgenConfig.minevents   = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7']
genSeq.Pythia8B.NDecayLoops = 1
genSeq.Pythia8B.NHadronizationLoops = 6
        
genSeq.Pythia8B.QuarkPtCut      = 7.5
genSeq.Pythia8B.QuarkEtaCut     = 3.0
genSeq.Pythia8B.AntiQuarkPtCut  = 0.0
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [-531]
genSeq.EvtInclusiveDecay.userDecayFile = "Bsbar_phi_MUMU_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.7
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.7
       
filtSeq.BSignalFilter.LVL1MuonCutPT  = 4000.0
filtSeq.BSignalFilter.LVL2MuonCutPT  = 4000.0
filtSeq.BSignalFilter.B_PDGCode = -531
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7
