##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B_d -> Jpsi(->mumu) Kstar(->Kpi)
##############################################################
f = open("Bd_JpsiMuMu_KstarKpi_USER.DEC","w") #define file name
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Define PKHplus  0.159\n") #added copying the DECAY.DEC file
f.write("Define PKHzero  0.775\n") #added copying the DECAY.DEC file
f.write("Define PKHminus 0.612\n") #added copying the DECAY.DEC file
f.write("Define PKphHplus  1.563\n") #added copying the DECAY.DEC file
f.write("Define PKphHzero  0.0\n") #added copying the DECAY.DEC file
f.write("Define PKphHminus 2.712\n") #added copying the DECAY.DEC file
f.write("Alias myJ/psi J/psi\n")
f.write("Alias myAntiKstar   anti-K*0\n")
f.write("Decay anti-B0\n")
f.write("1.0000    myJ/psi  myAntiKstar        SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus;\n") #copied decay stuff from DECAY.DEC file
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+     mu-        VLL;\n")
f.write("Enddecay\n")
f.write("Decay myAntiKstar\n")
f.write("1.0000    K-     pi+        VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################
##############################################################

evgenConfig.description = "Exclusive anti-Bd -> Jpsi (mu mu) anti-Kstar (pi K) production"
evgenConfig.keywords = ["exclusive","Bd","Jpsi","2muon"]
evgenConfig.minevents = 100

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")

### Put the content of MC15JobOptions/Pythia8B_exclusiveB_Common.py
### except actual closing B decays

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 5.']

genSeq.Pythia8B.QuarkPtCut =  7.0 # was 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0 # was 7.0
genSeq.Pythia8B.QuarkEtaCut = 2.6 # was 102.5
genSeq.Pythia8B.AntiQuarkEtaCut =  102.5 # was 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 1

# Make Pythia select the events with anti-Bd
genSeq.Pythia8B.SignalPDGCodes = [-511]

genSeq.EvtInclusiveDecay.userDecayFile = "Bd_JpsiMuMu_KstarKpi_USER.DEC"

# Final state selections
filtSeq.BSignalFilter.B_PDGCode = -511
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 800.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 3500
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 3500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6
