# based on the JobOptions MC15.429313

evgenConfig.description = "Herwig7 BuiltinME W Inclusive with MMHT2014 LO PDF and H7UE tune"
evgenConfig.generators  = ["Herwig7"] 
evgenConfig.keywords    = ["SM", "W"]
evgenConfig.contact     = ['paolo.francavilla@cern.ch', 'Shu.Li@cern.ch', "daniel.rauch@desy.de"]
evgenConfig.minevents   = 500

# initialize Herwig7 generator configuration for built-in matrix elements
include("MC15JobOptions/Herwig7_BuiltinME.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")
Herwig7Config.tune_commands()
import os
if "HERWIG7VER" in os.environ:
       version = os.getenv("HERWIG7VER")
       verh7 = version.split(".")[1]
else:
       verh7 = 0
       
if int(verh7 == 0):
       Herwig7Config.add_commands("""
cd /Herwig/MatrixElements
#Inclusive W
insert SimpleQCD:MatrixElements[0] MEqq2W2ff
set MEqq2W2ff:Process Electron
#Switch here for Muon W events instead
#set MEqq2W2ff:Process Muon
""")
else:
   Herwig7Config.add_commands("""
cd /Herwig/MatrixElements
#Inclusive W
insert SimpleQCD:MatrixElements[0] MEqq2W2ff
set MEqq2W2ff:Process Electron
#Switch here for Muon W events instead
#set MEqq2W2ff:Process Muon
""")

# run Herwig7
Herwig7Config.run()
