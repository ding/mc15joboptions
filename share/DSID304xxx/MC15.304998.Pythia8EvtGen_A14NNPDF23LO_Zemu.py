evgenConfig.description= "Z Lepton Violation"
evgenConfig.process= "Z -> e+- mu-+"
evgenConfig.contact= [ "bhood@cern.ch" ]
evgenConfig.keywords= [ "exotic", "SM" ]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "WeakZ0:gmZmode = 2",
                            "PhaseSpace:mHatMin = 60.",       # lower invariant mass
                            "23:onMode = off",                # switch off all Z decays
                            "23:mWidth = 2.4952",
                            "23:doForceWidth = 1",
                            "23:oneChannel = 1 0.5 0 11 -13", # Z->e-mu+
                            "23:addChannel = 1 0.5 0 -11 13"] # Z->e+mu-

include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut = 15000.
filtSeq.MultiLeptonFilter.Etacut = 2.8
filtSeq.MultiLeptonFilter.NLeptons = 2
