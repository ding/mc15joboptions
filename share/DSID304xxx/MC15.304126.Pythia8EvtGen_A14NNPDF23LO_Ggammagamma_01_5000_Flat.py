######################################################################
# Graviton to gam gam decay with Pythia 8 -- FLAT sample
######################################################################

evgenConfig.description = "RS Graviton, m = 5000 GeV, kappaMG = 0.542, Flat."
evgenConfig.keywords = ["exotic", "BSM", "graviton","diphoton","RandallSundrum"]
evgenConfig.contact = ["jan.stark@cern.ch"]
evgenConfig.process = "RS Graviton -> gam gam"

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
                           "ExtraDimensionsG*:gg2G* = on",
                           "ExtraDimensionsG*:ffbar2G* = on",
                           "5100039:m0 = 5000.",
                           "5100039:onMode = off",
                           "5100039:onIfAny = 22",
                           "ExtraDimensionsG*:kappaMG = 0.542",
                           "5100039:mMin = 50.",
                           "PhaseSpace:mHatMin = 50.",
                           ]

genSeq.Pythia8.UserHook = "GravFlat"
genSeq.Pythia8.UserModes += ["GravFlat:EnergyMode = 13"]
