mchi = 35
mphi = 800
gx = 2.65451098537
filter_string = "T"
evt_multiplier = 25
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
