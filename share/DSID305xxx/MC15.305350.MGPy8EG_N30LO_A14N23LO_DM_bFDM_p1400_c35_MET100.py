mchi = 35
mphi = 1400
gx = 4.64240488076
filter_string = "T"
evt_multiplier = 25
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
