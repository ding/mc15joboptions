evgenConfig.description = "UEH subgroup; prompt leptonjet gun samples"
evgenConfig.keywords = ["BSM", "exotic", "darkPhoton"]
evgenConfig.generators += ["ParticleDecayer"]
evgenConfig.process = 'z_d decay'
evgenConfig.contact = ["Bernhard.Meirose@cern.ch"]

include("GeneratorUtils/StdEvgenSetup.py")
theApp.EvtMax = 15000

# ParticleGun
import ParticleGun as PG
pg = PG.ParticleGun()
genSeq += pg
pg.randomSeed = 123456
pg.sampler.pid = 999
pg.samplers = []

## For convenience
import math
PI = math.pi
TWOPI = 2*math.pi
TENDEG = TWOPI/36

for i in range(36): #< choose how many samplers
     phirange = [i*TENDEG -PI, (i+1)*TENDEG -PI]
     etarange = [0.0, 2.5] if (i % 2 == 0) else [-2.5, 0.0]
     momsampler = PG.PtEtaMPhiSampler(pt=[10000, 500000],
                                      phi=phirange, eta=etarange)
     pg.samplers.append( PG.ParticleSampler(mom=momsampler))

topSeq += pg
	                    
include("GeneratorUtils/postJO.CopyWeights.py")
include("GeneratorUtils/postJO.PoolOutput.py")
include("GeneratorUtils/postJO.DumpMC.py")

# Particle Decayer
from ParticleDecayer.ParticleDecayerConf import ParticleDecayer
genSeq += ParticleDecayer()
genSeq.ParticleDecayer.OutputLevel = INFO
genSeq.ParticleDecayer.McEventCollection = "GEN_EVENT"

genSeq.ParticleDecayer.LJType = 1
genSeq.ParticleDecayer.ParticleID = 999 
genSeq.ParticleDecayer.ParticleMass = 400 
genSeq.ParticleDecayer.ParticleLifeTime = 0
genSeq.ParticleDecayer.ParticlePolarization = 0 
genSeq.ParticleDecayer.ParticlePDGID = 700022 
genSeq.ParticleDecayer.DecayBRElectrons = 0.33 
genSeq.ParticleDecayer.DecayBRMuons     = 0.33 
genSeq.ParticleDecayer.DecayBRPions     = 0.34 
genSeq.ParticleDecayer.DoUniformDecay               = False
genSeq.ParticleDecayer.DoExponentialDecay           = True
genSeq.ParticleDecayer.ExpDecayDoVariableLifetime   = False
genSeq.ParticleDecayer.ExpDecayDoTruncateLongDecays = False
genSeq.ParticleDecayer.BarrelRadius         = 8.e3 
genSeq.ParticleDecayer.EndCapDistance       = 11.e3 
genSeq.ParticleDecayer.ThetaEndCapBarrel    = 0.628796286
