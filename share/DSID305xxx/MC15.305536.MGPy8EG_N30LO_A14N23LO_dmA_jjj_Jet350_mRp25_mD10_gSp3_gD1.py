model   = 'dmA'
mR      = 250
mDM     = 10000
gSM     = 0.30
gDM     = 1.00
widthR  = 8.948453
jetminpt= 350
filteff = 0.005280

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetjet.py")

evgenConfig.description = "DM axial Z'->jj, m_{R}=250 g_{SM}=0.30 m_{DM}=10000 g_{DM}=1.00"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.process = "p p > xi j, xi > j j"
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>"]
evgenConfig.minevents = 1000
