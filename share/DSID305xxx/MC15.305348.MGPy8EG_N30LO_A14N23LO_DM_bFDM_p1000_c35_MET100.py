mchi = 35
mphi = 1000
gx = 3.31699751695
filter_string = "T"
evt_multiplier = 25
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
