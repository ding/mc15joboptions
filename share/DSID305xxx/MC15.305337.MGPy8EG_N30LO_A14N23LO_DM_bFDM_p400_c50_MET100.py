mchi = 50
mphi = 400
gx = 1.11803398875
filter_string = "T"
evt_multiplier = 10
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
