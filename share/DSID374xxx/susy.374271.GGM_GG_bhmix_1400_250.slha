#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375322E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416392E+03   # H
        36     2.00000000E+03   # A
        37     2.00208439E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.99643661E+03   # ~d_L
   2000001     4.99632684E+03   # ~d_R
   1000002     4.99619130E+03   # ~u_L
   2000002     4.99624954E+03   # ~u_R
   1000003     4.99643661E+03   # ~s_L
   2000003     4.99632684E+03   # ~s_R
   1000004     4.99619130E+03   # ~c_L
   2000004     4.99624954E+03   # ~c_R
   1000005     4.99626612E+03   # ~b_1
   2000005     4.99649880E+03   # ~b_2
   1000006     5.12684281E+03   # ~t_1
   2000006     5.42499591E+03   # ~t_2
   1000011     5.00008259E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984142E+03   # ~nu_eL
   1000013     5.00008259E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984142E+03   # ~nu_muL
   1000015     5.00001287E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984142E+03   # ~nu_tauL
   1000021     1.52201101E+03   # ~g
   1000022     2.38927030E+02   # ~chi_10
   1000023    -2.70409324E+02   # ~chi_20
   1000025     3.33141032E+02   # ~chi_30
   1000035     3.12901945E+03   # ~chi_40
   1000024     2.68166082E+02   # ~chi_1+
   1000037     3.12901902E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.56341339E-01   # N_11
  1  2    -2.22771737E-02   # N_12
  1  3     5.94533630E-01   # N_13
  1  4    -5.80101548E-01   # N_14
  2  1     1.53222952E-02   # N_21
  2  2    -4.61395454E-03   # N_22
  2  3    -7.05672863E-01   # N_23
  2  4    -7.08357077E-01   # N_24
  3  1     8.30812466E-01   # N_31
  3  2     1.55237283E-02   # N_32
  3  3    -3.85097965E-01   # N_33
  3  4     4.01508676E-01   # N_34
  4  1     4.33062251E-04   # N_41
  4  2    -9.99620654E-01   # N_42
  4  3    -1.59728018E-02   # N_43
  4  4     2.24327716E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.25813090E-02   # U_11
  1  2     9.99745010E-01   # U_12
  2  1     9.99745010E-01   # U_21
  2  2     2.25813090E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.17205384E-02   # V_11
  1  2     9.99496777E-01   # V_12
  2  1     9.99496777E-01   # V_21
  2  2     3.17205384E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99849622E-01   # cos(theta_t)
  1  2    -1.73416662E-02   # sin(theta_t)
  2  1     1.73416662E-02   # -sin(theta_t)
  2  2     9.99849622E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13924745E-01   # cos(theta_b)
  1  2     8.57835274E-01   # sin(theta_b)
  2  1    -8.57835274E-01   # -sin(theta_b)
  2  2     5.13924745E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89410499E-01   # cos(theta_tau)
  1  2     7.24370875E-01   # sin(theta_tau)
  2  1    -7.24370875E-01   # -sin(theta_tau)
  2  2     6.89410499E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90205879E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51792809E+02   # vev(Q)              
         4     3.84457963E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53061973E-01   # gprime(Q) DRbar
     2     6.28967028E-01   # g(Q) DRbar
     3     1.08619188E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02660967E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72300217E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79956776E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99435292E+06   # M^2_Hd              
        22    -5.53873039E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37847942E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.06221677E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.04778009E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.51743490E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.44944103E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.01790075E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.54586644E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.26308728E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     9.16281298E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.85656678E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.88313740E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.01790075E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.54586644E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.26308728E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     9.16281298E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.85656678E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.88313740E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.15728365E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.21480820E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.33829356E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.33773903E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.90373328E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.07664247E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     7.40621940E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.40621940E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.40621940E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.40621940E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35046751E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35046751E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.22883972E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.53582238E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.13977123E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.98523427E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.39388966E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.43193554E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.77252836E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.81844277E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.56087496E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.25997849E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     4.02494538E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.54064492E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.72527470E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.95318972E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.19152883E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.94076280E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -2.37805542E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.83508258E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.15057327E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.16180647E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     6.60513425E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.83900685E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.00184106E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.83306299E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.08131189E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.30328299E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -2.72624247E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -7.68421767E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -5.48161149E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.07080555E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.24703021E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.83000363E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.40850541E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.04763611E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.33559331E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.67806300E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.68645426E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.60248503E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.36176729E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.35645964E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.43882453E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.32923456E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.97830896E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.60828247E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.95610936E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.07629864E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.24919590E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.45669765E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.10356221E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.23499386E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.28868154E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.53072046E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.36180458E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.27787323E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.47237184E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.56017622E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.97974849E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.15068874E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.96001563E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.07678330E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.17008043E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.77457195E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.85953308E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.38246606E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.52191500E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87840102E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.36176729E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.35645964E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.43882453E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.32923456E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.97830896E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.60828247E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.95610936E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.07629864E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.24919590E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.45669765E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.10356221E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.23499386E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.28868154E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.53072046E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.36180458E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.27787323E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.47237184E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.56017622E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.97974849E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.15068874E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.96001563E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.07678330E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.17008043E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.77457195E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.85953308E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.38246606E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.52191500E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87840102E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80275452E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.87774127E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10942358E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.61576200E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59513847E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11826601E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19409619E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46125651E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.10440830E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35171814E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.89323929E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.99413904E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80275452E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.87774127E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10942358E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.61576200E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59513847E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11826601E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19409619E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46125651E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.10440830E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35171814E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.89323929E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.99413904E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63122753E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.88543631E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46981338E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15123988E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.31199059E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95802623E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.62591577E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36736234E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64274959E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.64592684E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.92664858E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.00387550E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44599477E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.67420332E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89410882E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80261518E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.82242395E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.21880145E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.41426722E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59734534E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40461561E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19088009E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80261518E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.82242395E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.21880145E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.41426722E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59734534E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40461561E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19088009E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80581699E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.81349751E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.21741063E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.41265335E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59438142E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.54379454E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18496013E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.52054412E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.64739483E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33996891E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33996891E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11332396E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11332396E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09304953E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41061071E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.06992284E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.33253363E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.02061077E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.85339550E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07666958E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.52688308E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.55025113E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.25263610E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.51946464E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.29899175E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.00956455E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.08126943E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00683531E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92415664E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.90080475E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.77437347E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.19042913E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.15639506E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.14172949E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.69653620E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.20449658E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.55907146E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.20449658E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.55907146E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.25915118E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.55804731E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.55804731E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48793759E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.10386902E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.10386902E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.10386902E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18477064E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18477064E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18477064E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18477064E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.94923609E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.94923609E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.94923609E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.94923609E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.18831471E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.18831471E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.43805973E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99749476E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.76015893E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.32483101E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.25967294E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.41064795E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39842115E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.01540756E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.20425559E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.01741888E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.01741888E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33824300E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.26896851E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.02324620E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.68880556E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.20902912E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.02338332E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.83693554E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.74529940E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.22088403E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.54408797E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.54408797E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.42032766E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.67450277E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.77969327E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.06212762E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.44122372E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21695368E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01624055E-01    2           5        -5   # BR(h -> b       bb     )
     6.22602261E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20374057E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66268802E-04    2           3        -3   # BR(h -> s       sb     )
     2.01154372E-02    2           4        -4   # BR(h -> c       cb     )
     6.64297193E-02    2          21        21   # BR(h -> g       g      )
     2.31880492E-03    2          22        22   # BR(h -> gam     gam    )
     1.62697495E-03    2          22        23   # BR(h -> Z       gam    )
     2.16847833E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80903071E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01149492E+01   # H decays
#          BR         NDA      ID1       ID2
     1.39019468E-03    2           5        -5   # BR(H -> b       bb     )
     2.32285598E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21216069E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05144529E-06    2           3        -3   # BR(H -> s       sb     )
     9.48915798E-06    2           4        -4   # BR(H -> c       cb     )
     9.38912289E-01    2           6        -6   # BR(H -> t       tb     )
     7.51952317E-04    2          21        21   # BR(H -> g       g      )
     2.63842103E-06    2          22        22   # BR(H -> gam     gam    )
     1.09242755E-06    2          23        22   # BR(H -> Z       gam    )
     3.16581800E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57859087E-04    2          23        23   # BR(H -> Z       Z      )
     8.53030762E-04    2          25        25   # BR(H -> h       h      )
     8.42891164E-24    2          36        36   # BR(H -> A       A      )
     3.36665745E-11    2          23        36   # BR(H -> Z       A      )
     2.54915349E-12    2          24       -37   # BR(H -> W+      H-     )
     2.54915349E-12    2         -24        37   # BR(H -> W-      H+     )
     7.23238384E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.15129136E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.93525172E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.01944541E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.96735516E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.32626177E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.54796237E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05621841E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39305083E-03    2           5        -5   # BR(A -> b       bb     )
     2.29923920E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12864117E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07164398E-06    2           3        -3   # BR(A -> s       sb     )
     9.39101857E-06    2           4        -4   # BR(A -> c       cb     )
     9.39874414E-01    2           6        -6   # BR(A -> t       tb     )
     8.89608988E-04    2          21        21   # BR(A -> g       g      )
     2.66246382E-06    2          22        22   # BR(A -> gam     gam    )
     1.27397352E-06    2          23        22   # BR(A -> Z       gam    )
     3.08491910E-04    2          23        25   # BR(A -> Z       h      )
     6.12180702E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.74258571E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.47859922E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.11535451E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.99371259E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.13650338E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.45936260E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97574446E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23524253E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34822340E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30181796E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42114466E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14112782E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02438909E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41574000E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16171758E-04    2          24        25   # BR(H+ -> W+      h      )
     1.30113781E-12    2          24        36   # BR(H+ -> W+      A      )
     1.79061582E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.30726621E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98247463E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
