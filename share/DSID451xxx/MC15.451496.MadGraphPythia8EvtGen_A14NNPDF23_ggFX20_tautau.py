from MadGraphControl.MadGraphUtils import *
from os import path

safefactor=1.1
mode=0


### DSID lists (extensions can include systematics samples)
test=[451496]

fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in test:
    fcard.write("""
    import model 2HDMCPF_NLO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ 
    define l- = e- mu- 
    define vl = ve vm 
    define vl~ = ve~ vm~
    generate p p > h3 > ta+ ta- j [QCD]
    output -f""")
    fcard.close()

else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'lhapdf'",
           'lhaid'  :"263000",
           'use_syst':"True",
           'sys_pdf' : 'NNPDF30_lo_as_0130',
           'ptj1min' : '120'
}

XMass  = { '25':'1.2500e+02',
           '35':'1.2500e+10',
           '36':'20',
           '37':'1.2500e+10',
}

Params={}

decays={'WH3':'1.801e-02',
}


Params['DECAY']=decays

runName='run_01'     

process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=runArgs.maxEvents/0.53*safefactor,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

build_param_card(param_card_old=path.join(process_dir,'Cards/param_card.dat'),param_card_new='param_card_new.dat',masses=XMass,params=Params)    
print_cards()

generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)  

#### Shower 
evgenConfig.description = 'MG5 ggF X to ta+ ta-, mX = 20 GeV'
evgenConfig.keywords+=['jets','tau']
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
