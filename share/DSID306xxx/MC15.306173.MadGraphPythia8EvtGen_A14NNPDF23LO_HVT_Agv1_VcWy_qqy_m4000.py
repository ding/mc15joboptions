include("MC15JobOptions/MadGraphControl_HVT.py")
evgenConfig.description = "heavyVectorTriplet Spin-1 Wgamma 4 TeV resonance in fully hadronic decay model"
evgenConfig.keywords = ["exotic", "Wgamma", "heavyVectorTriplet", "allHadronic", "spin1"]
