from MadGraphControl.MadGraphUtils import *

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_ewkhjj_EW6_NLO'
evgenConfig.keywords = ['SM','diboson','VBS','2jet','NLO']
evgenConfig.contact = ["schae@cern.ch","mdshapiro@lbl.gov"]

include("MC15JobOptions/MadGraph_NNPDF30_HInv_NLO.py")
evgenConfig.inputconfcheck="MGaMcAtNloPy8EG_HInv_NLO"
