include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.contact = ['Stefan von Buddenbrock <stef.von.b@cern.com>']
evgenConfig.description = "Event generation of gg > A > ZH (H is a heavy scalar), H -> Z(ll)y, Z -> vv"
evgenConfig.keywords = ["BSMHiggs", "Zgamma"]

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'HiggsBSM:gg2A3 = on',
                            '36:m0 = 350.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 103 25 93',
                            '36:onMode = off',
                            '36:onIfMatch = 25 93',
                            '25:m0 = 250.0',
                            '25:doForceWidth = yes',
                            '25:mWidth = 0.01',
                            '25:onMode = off',
                            '25:onIfMatch = 22 23',
                            '23:onMode = off',
                            '23:onIfAny = 11 13',
                            '93:onMode = off'
                            ]

genSeq.Pythia8.Commands.append("93:onIfAny = 12 14 16")

