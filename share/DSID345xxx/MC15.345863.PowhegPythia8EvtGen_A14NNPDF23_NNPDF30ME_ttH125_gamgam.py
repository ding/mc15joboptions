#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]

# Decay to diphoton
genSeq.Pythia8.Commands  += [ '25:onMode = off', '25:onIfMatch = 22 22' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description     = 'POWHEG+Pythia8 ttH production with A14 NNPDF2.3 tune'
evgenConfig.inputfilecheck  = 'TXT'
evgenConfig.keywords        = [ 'SM', 'top', 'Higgs' ]
evgenConfig.generators     += [ 'Powheg', 'Pythia8' ]
evgenConfig.contact         = [ 'james.robinson@cern.ch', 'kathrin.becker@cern.ch', 'chris.meyer@cern.ch' ]
