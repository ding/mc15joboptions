#--------------------------------------------------------------
# Powheg+Herwig7+Evtgen, ggH+mumu, H+jet production with NNLOPS and Herwig shower, Hmumu mh=125 GeV"
#--------------------------------------------------------------
evgenConfig.process     = "ggH H->mumu"
evgenConfig.description = "POWHEG+HERWIG7+EVTGEN, H+jet production with NNLOPS and Herwig sho, Hmumu mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2muon", "mH125" ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]  # important to get past checks
evgenConfig.contact     = [ 'paul.daniel.thompson@cern.ch' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.minevents   = 2000

#--------------------------------------------------------------
# remove additional weights so as not to cause issues for Herwig
#--------------------------------------------------------------
include("MC15JobOptions/Herwig7_701_StripWeights.py")

#--------------------------------------------------------------
# Herwig7 showering with MMHT2014lo68cl PDF and NNPDF30_nnlo_as_0118 ME (261000)
#--------------------------------------------------------------
#
include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3nnloME_LHEF_EvtGen_Common.py')
from Herwig7_i import config as hw

genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()

#--------------------------------------------------------------
# Higgs to mumu decay
#--------------------------------------------------------------
genSeq.Herwig7.Commands += [
  'do /Herwig/Particles/h0:SelectDecayModes h0->mu-,mu+;',
  'do /Herwig/Particles/h0:PrintDecayModes' # print out decays modes and branching ratios to the terminal/log.generate
]
