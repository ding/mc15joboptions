#--------------------------------------------------------------
# Powheg HJ setup starting from ATLAS defaults
# Include fragments that remove the weight lines for Herwig showering
#--------------------------------------------------------------

include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3nnloME_LHEF_EvtGen_Common.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")

from Herwig7_i import config as hw

genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()

## only consider H->yy devays
genSeq.Herwig7.Commands += [
  '## force H->yy decays',
  'do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma;',
  'do /Herwig/Particles/h0:PrintDecayModes' # print out decays modes and branching ratios to the terminal/log.generate
]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 H+jet production with NNLOPS and the A14 tune'
evgenConfig.keywords    = [ 'Higgs', '1jet' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'kathrin.becker@cern.ch']
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.minevents   = 2000
evgenConfig.inputfilecheck = "TXT"

