######################################################################
# Small script to correct the FxFx file after using MadSpin,
# which loses FxFx-specific info when producing decayed LHE file.
######################################################################

def fix_madspin_file(infile,infile_madspin,outfile): 
    event_text_array = []
    event_counter = 0
    for line in open(infile,'r') :
        if "<event" in line:
            event_text_array.append(line)
    file_out = open(outfile,'w')
    num_events = 0
    for line in open(infile_madspin, 'r'):
        if "<event>" in line:
            file_out.write(event_text_array[num_events])
            num_events = num_events + 1
        else : 
            file_out.write(line)

######################################################################

from MadGraphControl.MadGraphUtils import *
    
from os import environ,path,unlink
environ['ATHENA_PROC_NUMBER'] = '1'

#Need extra events to avoid Pythia8 problems.
safefactor=50.0
nevents=runArgs.maxEvents*safefactor

######################################################################
# Configure event generation.
######################################################################

runName       = 'run_01'
gridpack_dir  = 'madevent/'
gridpack_mode = True

#Set input file for Pythia8.
runArgs.inputGeneratorFile = runName+'._00001.events.tar.gz'

#Make process card.
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HC_NLO_X0_UFO-heft
define p = g d d~ u u~ s s~ c c~ b b~                                   
define j = g d d~ u u~ s s~ c c~ b b~                                 
generate    p p > x0     / t a [QCD] @0
add process p p > x0 j   / t a [QCD] @1
add process p p > x0 j j / t a [QCD] @2
output ppx0_4l_NLOQCD_FxFx_MadSpin""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Define process directory.
process_dir = new_process(grid_pack=gridpack_dir)

#Fetch default run_card.dat and set parameters
ptj = 25
qCut = 2*ptj
maxjetflavor = 5

extras = {'lhe_version'   :'3.0',
          'parton_shower' :'PYTHIA8',
          'reweight_scale':'True',
          'ickkw'         :'3',
          'jetradius'     :'1.0',
          'ptj'           :str(ptj),
          'maxjetflavor'  :str(maxjetflavor),
          'pdlabel'       :"'lhapdf'",
          'lhaid'         :260000,
          'pdfwgt'        :'T',
          'use_syst'      :'T',
          'sys_scalefact' :'None',
          'sys_alpsfact'  :'None',
          'sys_matchscale':'None',
          'sys_pdf'       :'NNPDF30_nlo_as_0118'}

#Set couplings here.
parameters={'frblock': {
        'kAza':   0.0, 
        'kAgg':   0.0, 
        'kAaa':   0.0, 
        'cosa':   1.0, 
        'kHdwR':  0.0, 
        'kHaa':   0.0, 
        'kAll':   0.0, 
        'kHll':   0.0, 
        'kAzz':   0.0, 
        'kSM':    1.0, 
        'kHdwI':  0.0, 
        'kHdz':   0.0, 
        'kAww':   0.0, 
        'kHgg':   1.0, 
        'kHda':   0.0, 
        'kHza':   0.0, 
        'kHww':   0.0, 
        'kHzz':   0.0, 
        'Lambda': 1000.0
        }}

#Create parame card
build_param_card(param_card_old=path.join(process_dir,'Cards/param_card.dat'),param_card_new='param_card_new.dat',
                 masses={'25': '1.250000e+02'},params=parameters)

#Create run card.
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#Create MadSpin decay card.
madspin_card_loc='madspin_card.dat'

if not hasattr(runArgs, 'inputGenConfFile'):
    fMadSpinCard = open(madspin_card_loc,'w')
    fMadSpinCard.write('import '+path.join('Events',runName,'events.lhe.gz')+'\n')                  
    fMadSpinCard.write('set ms_dir MadSpin\n')                                        
else:                                                                               
    unlink(path.join(gridpack_dir,'Cards/madspin_card.dat'))                                  
    fMadSpinCard = open(path.join(gridpack_dir,'Cards/madspin_card.dat'),'w')                    
    fMadSpinCard.write('import '+path.join(gridpack_dir,'Events',runName,'events.lhe.gz')+'\n')  
    fMadSpinCard.write('set ms_dir '+path.join(gridpack_dir,'MadSpin')+'\n')                        

#NOTE: Can't set random seed in due to crashes when using "set spinmode none".
#    fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')        

fMadSpinCard.write('''set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000 # number of PS to estimate the maximum for each event (default: 400)
set spinmode none
define l- = e- mu- ta-
define l+ = e+ mu+ ta+
decay x0 > w+ w- > l+ vl l- vl~
launch''')
fMadSpinCard.close()  

print_cards()

#Run the event generation!
generate(run_card_loc='run_card.dat',
         param_card_loc='param_card_new.dat',
         mode=0,
         proc_dir=process_dir,
         run_name=runName,
         madspin_card_loc=madspin_card_loc,
         grid_pack=gridpack_mode,
         gridpack_dir=gridpack_dir,
         nevents=nevents,
         random_seed=runArgs.randomSeed)

######################################################################
# NOTE: Need to add python hack here if we're using FxFx merging!
######################################################################

print "Have to hack the input files first!"

import subprocess
eventspath = path.join(process_dir,'Events',runName)
decayed_events         = path.join(eventspath,'unweighted_events.lhe')
decayed_events_renamed = path.join(eventspath,'tmp_events.lhe')
notdecayed_events      = path.join(eventspath,'events.lhe')

unzip = subprocess.Popen(['gunzip',decayed_events+".gz"])
unzip.wait()
shutil.move(decayed_events, decayed_events_renamed)
fix_madspin_file(notdecayed_events, decayed_events_renamed, decayed_events)
rezip = subprocess.Popen(['gzip',decayed_events])
rezip.wait()

#arrange output
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runArgs.inputGeneratorFile,lhe_version=3,saveProcDir=True)  

######################################################################
# End of event generation, start configuring parton shower here.
######################################################################

print "Now performing parton showering ..."

#### Shower 
environ['ATHENA_PROC_NUMBER'] = '1'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

testSeq.TestHepMC.MaxVtxDisp = 100000000

# Matching settings                                                                                                                              
genSeq.Pythia8.Commands += [
    "JetMatching:setMad           = off",
    "JetMatching:merge            = on",
    "JetMatching:scheme           = 1",
    "JetMatching:nQmatch          = "+str(maxjetflavor), # Should match maxjetflavour in 'mg5'.
    "JetMatching:jetAlgorithm     = 2",
    "JetMatching:slowJetPower     = 1",  
    "JetMatching:clFact           = 1.0",
    "JetMatching:eTjetMin         = "+str(qCut),         # Should match qcut.
    "JetMatching:coneRadius       = 1.0",                # Default.
    "JetMatching:etaJetMax        = 4.5",
    "JetMatching:exclusive        = 1",                  # Exclusive - all PS jets must match HS jets.
    "JetMatching:nJetMax          = 2",                  # *** Must match highest Born-level jet multiplicity. ***
    "JetMatching:nJet             = 0", 
    "JetMatching:jetAllow         = 1",
    "JetMatching:doShowerKt       = off",
    "JetMatching:qCut             = "+str(qCut),         # Merging scale for FxFx.
    "JetMatching:doFxFx           = on",
    "JetMatching:qCutME           = "+str(ptj)           # Should match ptj.
    ]

# Pythia8_i settings                                                                                                     
genSeq.Pythia8.UserHook = 'JetMatchingMadgraph'
genSeq.Pythia8.FxFxXS = True

#--------------------------------------------------------------
# DF Dilepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter,MultiElectronFilter,MultiMuonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")
filtSeq += MultiElectronFilter("Multi1ElectronFilter")
filtSeq += MultiMuonFilter("Multi1MuonFilter")

Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 10000.
Multi1TLeptonFilter.Etacut = 2.7
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 2.7
Multi2LLeptonFilter.NLeptons = 2

Multi1ElectronFilter = filtSeq.Multi1ElectronFilter
Multi1ElectronFilter.Ptcut  = 5000.
Multi1ElectronFilter.Etacut = 2.7
Multi1ElectronFilter.NElectrons = 1

Multi1MuonFilter = filtSeq.Multi1MuonFilter
Multi1MuonFilter.Ptcut  = 5000.
Multi1MuonFilter.Etacut = 2.7
Multi1MuonFilter.NMuons = 1

filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter) and (Multi1ElectronFilter) and (Multi1MuonFilter)"

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "ggF+012jets (FxFx merging) 125 GeV Higgs production in the Higgs Characterization model decaying to wwlnulnu."
evgenConfig.keywords = ['BSM','Higgs','mH125','BSMHiggs','2lepton','dijet','resonance','WW']

evgenConfig.contact = ['Adam Kaluza <Adam.Kaluza@cern.ch>']
evgenConfig.inputconfcheck = "ggfhwwlnulnuNp2"
