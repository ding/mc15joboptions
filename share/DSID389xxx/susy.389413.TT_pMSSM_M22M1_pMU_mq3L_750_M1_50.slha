#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14980398E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04004820E+01   # W+
        25     1.25575629E+02   # h
        35     3.00015000E+03   # H
        36     2.99999973E+03   # A
        37     3.00095867E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03544171E+03   # ~d_L
   2000001     3.03019028E+03   # ~d_R
   1000002     3.03452248E+03   # ~u_L
   2000002     3.03152140E+03   # ~u_R
   1000003     3.03544171E+03   # ~s_L
   2000003     3.03019028E+03   # ~s_R
   1000004     3.03452248E+03   # ~c_L
   2000004     3.03152140E+03   # ~c_R
   1000005     8.43999404E+02   # ~b_1
   2000005     3.02939394E+03   # ~b_2
   1000006     8.39346950E+02   # ~t_1
   2000006     3.01414368E+03   # ~t_2
   1000011     3.00651466E+03   # ~e_L
   2000011     3.00182783E+03   # ~e_R
   1000012     3.00511237E+03   # ~nu_eL
   1000013     3.00651466E+03   # ~mu_L
   2000013     3.00182783E+03   # ~mu_R
   1000014     3.00511237E+03   # ~nu_muL
   1000015     2.98590600E+03   # ~tau_1
   2000015     3.02195826E+03   # ~tau_2
   1000016     3.00497909E+03   # ~nu_tauL
   1000021     2.34353438E+03   # ~g
   1000022     4.99517637E+01   # ~chi_10
   1000023     1.08492505E+02   # ~chi_20
   1000025    -2.99754824E+03   # ~chi_30
   1000035     2.99794848E+03   # ~chi_40
   1000024     1.08644188E+02   # ~chi_1+
   1000037     2.99865582E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886273E-01   # N_11
  1  2    -2.69679048E-03   # N_12
  1  3     1.48053127E-02   # N_13
  1  4    -9.85333002E-04   # N_14
  2  1     3.08481979E-03   # N_21
  2  2     9.99652117E-01   # N_22
  2  3    -2.61041397E-02   # N_23
  2  4     2.16850779E-03   # N_24
  3  1    -9.72329482E-03   # N_31
  3  2     1.69533634E-02   # N_32
  3  3     7.06818075E-01   # N_33
  3  4     7.07125342E-01   # N_34
  4  1    -1.11077571E-02   # N_41
  4  2     2.00238718E-02   # N_42
  4  3     7.06758505E-01   # N_43
  4  4    -7.07084208E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99316419E-01   # U_11
  1  2    -3.69688467E-02   # U_12
  2  1     3.69688467E-02   # U_21
  2  2     9.99316419E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995280E-01   # V_11
  1  2    -3.07229899E-03   # V_12
  2  1     3.07229899E-03   # V_21
  2  2     9.99995280E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98555974E-01   # cos(theta_t)
  1  2    -5.37211950E-02   # sin(theta_t)
  2  1     5.37211950E-02   # -sin(theta_t)
  2  2     9.98555974E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99916718E-01   # cos(theta_b)
  1  2     1.29056989E-02   # sin(theta_b)
  2  1    -1.29056989E-02   # -sin(theta_b)
  2  2     9.99916718E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06878655E-01   # cos(theta_tau)
  1  2     7.07334834E-01   # sin(theta_tau)
  2  1    -7.07334834E-01   # -sin(theta_tau)
  2  2     7.06878655E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01772416E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.49803976E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44202083E+02   # higgs               
         4     7.35298842E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.49803976E+03  # The gauge couplings
     1     3.62126178E-01   # gprime(Q) DRbar
     2     6.41743845E-01   # g(Q) DRbar
     3     1.02641607E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.49803976E+03  # The trilinear couplings
  1  1     2.12987399E-06   # A_u(Q) DRbar
  2  2     2.12990511E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.49803976E+03  # The trilinear couplings
  1  1     5.21162546E-07   # A_d(Q) DRbar
  2  2     5.21268651E-07   # A_s(Q) DRbar
  3  3     1.22815962E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.49803976E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.15132713E-07   # A_mu(Q) DRbar
  3  3     1.16527003E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.49803976E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53496827E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.49803976E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.10454416E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.49803976E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04760130E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.49803976E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.28354411E+04   # M^2_Hd              
        22    -9.07601203E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42953730E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.19593900E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47765565E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47765565E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52234435E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52234435E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.01097834E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.14168047E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.12375478E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.76207718E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.15923624E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.68961523E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.00590400E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.02335996E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.14703129E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.89748911E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.70804015E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62176322E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.19430202E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.93609842E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.30109903E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.46395518E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.40593491E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.20596760E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.96648323E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.37813177E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.46339368E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.44720225E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.58491577E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.31480355E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.82390771E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.11804237E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.94055724E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.11857918E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.70479029E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65833676E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.58025487E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.55223584E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.31508192E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.00724017E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.96953278E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18156784E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59777678E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.51849900E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.72636675E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.11289961E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40220784E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12356692E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.03923784E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65385012E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.16951615E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.66154741E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.30930019E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.04976730E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.97645328E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66969290E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.54955681E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.32381242E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.30103787E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.93179463E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54503994E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.11857918E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.70479029E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65833676E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.58025487E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.55223584E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.31508192E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.00724017E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.96953278E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18156784E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59777678E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.51849900E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.72636675E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.11289961E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40220784E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12356692E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.03923784E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65385012E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.16951615E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.66154741E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.30930019E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.04976730E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.97645328E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66969290E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.54955681E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.32381242E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.30103787E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.93179463E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54503994E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07351391E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.52826878E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02409098E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.41015455E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.85253919E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02308188E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.25285774E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56505841E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990500E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.49859712E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.68131371E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.23821054E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07351391E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.52826878E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02409098E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.41015455E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.85253919E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02308188E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.25285774E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56505841E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990500E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.49859712E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.68131371E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.23821054E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.84843764E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42707063E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19703236E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37589701E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78804153E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50716294E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17061693E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.15907587E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.31319155E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32184030E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.32611047E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07387982E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.70678400E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00142286E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.84668954E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.73715322E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02789865E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.05108789E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07387982E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.70678400E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00142286E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.84668954E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.73715322E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02789865E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.05108789E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07404000E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.70597140E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00117105E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.71250957E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.56275401E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02822125E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.04779006E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.79078081E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35841010E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.32163836E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35841010E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.32163836E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09616014E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.44057958E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09616014E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.44057958E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09085933E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     8.40376855E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.58665772E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.17775768E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.18503151E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.13182052E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.19105181E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.98719359E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.32565769E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.30393811E-10   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.07931322E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     4.39930894E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     5.57840436E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     4.39930894E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     5.57840436E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     7.72833641E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     4.75460255E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.75460255E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     5.00211706E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.69341978E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.69341978E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.68954853E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     8.44612787E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.28901194E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.11909894E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.92875342E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.92875342E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.01280535E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.01145053E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.61734426E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.61734426E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.18158601E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.18158601E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.46376871E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.46376871E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.33916629E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.00031332E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.99160929E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.01907376E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.01907376E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.34017462E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.30917327E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.59492569E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.59492569E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.21098387E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.21098387E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.02448644E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.02448644E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.92322332E-03   # h decays
#          BR         NDA      ID1       ID2
     6.65097319E-01    2           5        -5   # BR(h -> b       bb     )
     5.32564493E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.88527324E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.99759930E-04    2           3        -3   # BR(h -> s       sb     )
     1.72592743E-02    2           4        -4   # BR(h -> c       cb     )
     5.66649425E-02    2          21        21   # BR(h -> g       g      )
     1.95229328E-03    2          22        22   # BR(h -> gam     gam    )
     1.33586136E-03    2          22        23   # BR(h -> Z       gam    )
     1.80967449E-01    2          24       -24   # BR(h -> W+      W-     )
     2.28366295E-02    2          23        23   # BR(h -> Z       Z      )
     4.14949010E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.41050105E+01   # H decays
#          BR         NDA      ID1       ID2
     7.35080157E-01    2           5        -5   # BR(H -> b       bb     )
     1.76304748E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.23371459E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.65499680E-04    2           3        -3   # BR(H -> s       sb     )
     2.15967406E-07    2           4        -4   # BR(H -> c       cb     )
     2.15441426E-02    2           6        -6   # BR(H -> t       tb     )
     3.31077293E-05    2          21        21   # BR(H -> g       g      )
     2.11245369E-08    2          22        22   # BR(H -> gam     gam    )
     8.30790683E-09    2          23        22   # BR(H -> Z       gam    )
     2.99605485E-05    2          24       -24   # BR(H -> W+      W-     )
     1.49618036E-05    2          23        23   # BR(H -> Z       Z      )
     8.02742492E-05    2          25        25   # BR(H -> h       h      )
     6.53049773E-24    2          36        36   # BR(H -> A       A      )
     5.99265650E-18    2          23        36   # BR(H -> Z       A      )
     2.42076866E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13152535E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.20219607E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.38435117E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.10432449E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.73373326E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32451183E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82871348E-01    2           5        -5   # BR(A -> b       bb     )
     1.87745683E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.63822943E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.15312425E-04    2           3        -3   # BR(A -> s       sb     )
     2.30069610E-07    2           4        -4   # BR(A -> c       cb     )
     2.29382887E-02    2           6        -6   # BR(A -> t       tb     )
     6.75510710E-05    2          21        21   # BR(A -> g       g      )
     4.28751160E-08    2          22        22   # BR(A -> gam     gam    )
     6.65366495E-08    2          23        22   # BR(A -> Z       gam    )
     3.17817637E-05    2          23        25   # BR(A -> Z       h      )
     2.63494803E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22248877E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30854800E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.00127500E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01885145E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.08940845E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.44148317E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.63248898E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.97220316E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.07312204E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.04370434E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.09168539E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.13819549E-05    2          24        25   # BR(H+ -> W+      h      )
     1.04721083E-13    2          24        36   # BR(H+ -> W+      A      )
     2.04856888E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.95048024E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.15391085E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
