#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871846E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.49900000E+02   # M_1(MX)             
         2     8.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04042122E+01   # W+
        25     1.24726729E+02   # h
        35     3.00002223E+03   # H
        36     2.99999994E+03   # A
        37     3.00108288E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04193550E+03   # ~d_L
   2000001     3.03674373E+03   # ~d_R
   1000002     3.04103236E+03   # ~u_L
   2000002     3.03806950E+03   # ~u_R
   1000003     3.04193550E+03   # ~s_L
   2000003     3.03674373E+03   # ~s_R
   1000004     3.04103236E+03   # ~c_L
   2000004     3.03806950E+03   # ~c_R
   1000005     1.05535123E+03   # ~b_1
   2000005     3.03433001E+03   # ~b_2
   1000006     1.05308417E+03   # ~t_1
   2000006     3.02467451E+03   # ~t_2
   1000011     3.00646160E+03   # ~e_L
   2000011     3.00182359E+03   # ~e_R
   1000012     3.00507830E+03   # ~nu_eL
   1000013     3.00646160E+03   # ~mu_L
   2000013     3.00182359E+03   # ~mu_R
   1000014     3.00507830E+03   # ~nu_muL
   1000015     2.98619910E+03   # ~tau_1
   2000015     3.02154850E+03   # ~tau_2
   1000016     3.00492441E+03   # ~nu_tauL
   1000021     2.35218400E+03   # ~g
   1000022     4.52540402E+02   # ~chi_10
   1000023     9.38659991E+02   # ~chi_20
   1000025    -2.99492979E+03   # ~chi_30
   1000035     2.99549329E+03   # ~chi_40
   1000024     9.38822715E+02   # ~chi_1+
   1000037     2.99619894E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887180E-01   # N_11
  1  2    -1.30532827E-04   # N_12
  1  3    -1.49447397E-02   # N_13
  1  4    -1.50509396E-03   # N_14
  2  1     5.58820091E-04   # N_21
  2  2     9.99584218E-01   # N_22
  2  3     2.79437245E-02   # N_23
  2  4     7.08713286E-03   # N_24
  3  1    -9.49891249E-03   # N_31
  3  2     1.47526753E-02   # N_32
  3  3    -7.06874205E-01   # N_33
  3  4     7.07121621E-01   # N_34
  4  1     1.16226492E-02   # N_41
  4  2    -2.47736353E-02   # N_42
  4  3     7.06629083E-01   # N_43
  4  4     7.07054821E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99218934E-01   # U_11
  1  2     3.95160973E-02   # U_12
  2  1    -3.95160973E-02   # U_21
  2  2     9.99218934E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99949791E-01   # V_11
  1  2    -1.00207826E-02   # V_12
  2  1    -1.00207826E-02   # V_21
  2  2    -9.99949791E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98450644E-01   # cos(theta_t)
  1  2    -5.56445100E-02   # sin(theta_t)
  2  1     5.56445100E-02   # -sin(theta_t)
  2  2     9.98450644E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99718861E-01   # cos(theta_b)
  1  2    -2.37107351E-02   # sin(theta_b)
  2  1     2.37107351E-02   # -sin(theta_b)
  2  2     9.99718861E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06970563E-01   # cos(theta_tau)
  1  2     7.07242973E-01   # sin(theta_tau)
  2  1    -7.07242973E-01   # -sin(theta_tau)
  2  2    -7.06970563E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00138391E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68718461E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43872449E+02   # higgs               
         4     1.02734494E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68718461E+03  # The gauge couplings
     1     3.62543179E-01   # gprime(Q) DRbar
     2     6.36482910E-01   # g(Q) DRbar
     3     1.02366103E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68718461E+03  # The trilinear couplings
  1  1     2.72392718E-06   # A_u(Q) DRbar
  2  2     2.72396567E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68718461E+03  # The trilinear couplings
  1  1     9.84624769E-07   # A_d(Q) DRbar
  2  2     9.84733119E-07   # A_s(Q) DRbar
  3  3     1.94557141E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68718461E+03  # The trilinear couplings
  1  1     3.92727134E-07   # A_e(Q) DRbar
  2  2     3.92745957E-07   # A_mu(Q) DRbar
  3  3     3.97973749E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68718461E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49851218E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68718461E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.71793861E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68718461E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.00633066E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68718461E+03  # The soft SUSY breaking masses at the scale Q
         1     4.49900000E+02   # M_1(Q)              
         2     8.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.11454168E+04   # M^2_Hd              
        22    -9.02335002E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40603166E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.11326346E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47864743E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47864743E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52135257E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52135257E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.70943078E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.99085859E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.00914141E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10859407E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.74222784E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.77888753E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.60571462E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.48308206E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.03918076E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66863103E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59354565E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11155210E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.02668870E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.50769460E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.49230540E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.21069431E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.08126661E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     7.48834688E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.70976858E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.48012894E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -3.53994839E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.64743558E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.76600497E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.66576088E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.40014179E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.43485549E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.25441563E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.49122743E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.15093874E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     9.34414156E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.98378847E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.57496007E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.46243834E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.13532048E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55761987E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.17637498E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.33901305E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.95417847E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44237938E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.44026662E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.26844739E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.48997970E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.34240440E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.38596363E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.97819985E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.77209893E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.46912829E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64150819E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.41995526E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.18493323E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.57947713E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.21966662E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55800427E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.43485549E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.25441563E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.49122743E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.15093874E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     9.34414156E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.98378847E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.57496007E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.46243834E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.13532048E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55761987E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.17637498E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.33901305E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.95417847E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44237938E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.44026662E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.26844739E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.48997970E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.34240440E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.38596363E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.97819985E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.77209893E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.46912829E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64150819E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.41995526E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.18493323E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.57947713E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.21966662E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55800427E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.33229571E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.12597468E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.96085390E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.86014599E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.35536563E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.91317081E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.27410368E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.49898952E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999729E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.66217085E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.98894618E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.51129863E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.33229571E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.12597468E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.96085390E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.86014599E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.35536563E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.91317081E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.27410368E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.49898952E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999729E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.66217085E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.98894618E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.51129863E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43643716E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.84142999E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.05501074E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.10355928E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.39212155E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.92564586E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.02683425E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.76249514E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.52653533E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04701247E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.78521973E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.33212878E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.12649720E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.95528446E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.71762361E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.45293632E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.91821810E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.54137336E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.33212878E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.12649720E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.95528446E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.71762361E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.45293632E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.91821810E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.54137336E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.33222307E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.12640228E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95498405E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.51592125E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.40676041E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.91859001E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.34546881E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.34065210E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.34039841E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.28810519E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.58780605E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.44012056E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.29211232E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.11028543E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.11954421E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.71892125E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.92886773E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.99672987E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     7.00327013E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.35316059E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.30101331E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.63152900E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.10455004E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.10455004E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.04055992E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.67668126E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.33291788E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.33291788E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.30960452E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.30960452E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.68533060E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.68533060E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.26797937E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.78654714E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.66486108E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.17152941E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.17152941E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.37579094E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.08289811E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.29515803E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.29515803E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.37576798E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.37576798E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.99266638E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.99266638E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.56890382E-03   # h decays
#          BR         NDA      ID1       ID2
     5.66311360E-01    2           5        -5   # BR(h -> b       bb     )
     7.24941777E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.56632899E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.44938807E-04    2           3        -3   # BR(h -> s       sb     )
     2.36795497E-02    2           4        -4   # BR(h -> c       cb     )
     7.65580354E-02    2          21        21   # BR(h -> g       g      )
     2.61252431E-03    2          22        22   # BR(h -> gam     gam    )
     1.70868308E-03    2          22        23   # BR(h -> Z       gam    )
     2.27495930E-01    2          24       -24   # BR(h -> W+      W-     )
     2.83381681E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.42286969E+01   # H decays
#          BR         NDA      ID1       ID2
     8.94464318E-01    2           5        -5   # BR(H -> b       bb     )
     7.26500094E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.56873073E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.15441786E-04    2           3        -3   # BR(H -> s       sb     )
     8.84146826E-08    2           4        -4   # BR(H -> c       cb     )
     8.81992690E-03    2           6        -6   # BR(H -> t       tb     )
     1.37869680E-05    2          21        21   # BR(H -> g       g      )
     8.25233163E-08    2          22        22   # BR(H -> gam     gam    )
     3.61540179E-09    2          23        22   # BR(H -> Z       gam    )
     7.92318171E-07    2          24       -24   # BR(H -> W+      W-     )
     3.95670739E-07    2          23        23   # BR(H -> Z       Z      )
     6.18502092E-06    2          25        25   # BR(H -> h       h      )
    -2.49162227E-27    2          36        36   # BR(H -> A       A      )
    -1.43060009E-20    2          23        36   # BR(H -> Z       A      )
     5.63584821E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.15253980E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.81551071E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.31417932E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23490681E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.94926329E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.34849017E+01   # A decays
#          BR         NDA      ID1       ID2
     9.14371227E-01    2           5        -5   # BR(A -> b       bb     )
     7.42637379E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.62578464E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.22500878E-04    2           3        -3   # BR(A -> s       sb     )
     9.10051766E-08    2           4        -4   # BR(A -> c       cb     )
     9.07335400E-03    2           6        -6   # BR(A -> t       tb     )
     2.67201593E-05    2          21        21   # BR(A -> g       g      )
     6.54396027E-08    2          22        22   # BR(A -> gam     gam    )
     2.63358212E-08    2          23        22   # BR(A -> Z       gam    )
     8.06934771E-07    2          23        25   # BR(A -> Z       h      )
     9.00269031E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.57663250E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.49628866E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.83227448E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.69095474E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46026587E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.73975107E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.38301159E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.34568999E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.40043547E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.88114609E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.19418731E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.33315297E-07    2          24        25   # BR(H+ -> W+      h      )
     5.30952076E-14    2          24        36   # BR(H+ -> W+      A      )
     4.71493766E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.24225833E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07014991E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
