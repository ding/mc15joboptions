#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14468638E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03955229E+01   # W+
        25     1.25566814E+02   # h
        35     3.00010143E+03   # H
        36     2.99999992E+03   # A
        37     3.00109992E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03332934E+03   # ~d_L
   2000001     3.02798721E+03   # ~d_R
   1000002     3.03241171E+03   # ~u_L
   2000002     3.03011896E+03   # ~u_R
   1000003     3.03332934E+03   # ~s_L
   2000003     3.02798721E+03   # ~s_R
   1000004     3.03241171E+03   # ~c_L
   2000004     3.03011896E+03   # ~c_R
   1000005     7.90487681E+02   # ~b_1
   2000005     3.02704206E+03   # ~b_2
   1000006     7.87944209E+02   # ~t_1
   2000006     3.02228465E+03   # ~t_2
   1000011     3.00686905E+03   # ~e_L
   2000011     3.00101598E+03   # ~e_R
   1000012     3.00547136E+03   # ~nu_eL
   1000013     3.00686905E+03   # ~mu_L
   2000013     3.00101598E+03   # ~mu_R
   1000014     3.00547136E+03   # ~nu_muL
   1000015     2.98612374E+03   # ~tau_1
   2000015     3.02208315E+03   # ~tau_2
   1000016     3.00560167E+03   # ~nu_tauL
   1000021     2.34108071E+03   # ~g
   1000022     1.01005614E+02   # ~chi_10
   1000023     2.17223896E+02   # ~chi_20
   1000025    -3.00040058E+03   # ~chi_30
   1000035     3.00048463E+03   # ~chi_40
   1000024     2.17383575E+02   # ~chi_1+
   1000037     3.00137964E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891873E-01   # N_11
  1  2     7.64336668E-04   # N_12
  1  3    -1.46832393E-02   # N_13
  1  4     2.45407234E-04   # N_14
  2  1    -3.82111542E-04   # N_21
  2  2     9.99661155E-01   # N_22
  2  3     2.60238338E-02   # N_23
  2  4     4.34898835E-04   # N_24
  3  1     1.02196759E-02   # N_31
  3  2    -1.87030562E-02   # N_32
  3  3     7.06779070E-01   # N_33
  3  4     7.07113216E-01   # N_34
  4  1    -1.05666534E-02   # N_41
  4  2     1.80882988E-02   # N_42
  4  3    -7.06803020E-01   # N_43
  4  4     7.07100170E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99323285E-01   # U_11
  1  2     3.67827797E-02   # U_12
  2  1    -3.67827797E-02   # U_21
  2  2     9.99323285E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999812E-01   # V_11
  1  2    -6.13450134E-04   # V_12
  2  1    -6.13450134E-04   # V_21
  2  2    -9.99999812E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98579499E-01   # cos(theta_t)
  1  2    -5.32821187E-02   # sin(theta_t)
  2  1     5.32821187E-02   # -sin(theta_t)
  2  2     9.98579499E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99711548E-01   # cos(theta_b)
  1  2    -2.40170938E-02   # sin(theta_b)
  2  1     2.40170938E-02   # -sin(theta_b)
  2  2     9.99711548E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06906879E-01   # cos(theta_tau)
  1  2     7.07306627E-01   # sin(theta_tau)
  2  1    -7.07306627E-01   # -sin(theta_tau)
  2  2    -7.06906879E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00155143E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.44686382E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44147151E+02   # higgs               
         4     1.07058763E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.44686382E+03  # The gauge couplings
     1     3.62025685E-01   # gprime(Q) DRbar
     2     6.39708711E-01   # g(Q) DRbar
     3     1.02715820E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.44686382E+03  # The trilinear couplings
  1  1     1.98344182E-06   # A_u(Q) DRbar
  2  2     1.98347310E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.44686382E+03  # The trilinear couplings
  1  1     6.94410048E-07   # A_d(Q) DRbar
  2  2     6.94495650E-07   # A_s(Q) DRbar
  3  3     1.45537096E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.44686382E+03  # The trilinear couplings
  1  1     3.27920943E-07   # A_e(Q) DRbar
  2  2     3.27937421E-07   # A_mu(Q) DRbar
  3  3     3.32642755E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.44686382E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53922152E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.44686382E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.95347726E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.44686382E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03322193E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.44686382E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.32994986E+04   # M^2_Hd              
        22    -9.08217136E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42070426E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.43022193E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48016613E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48016613E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51983387E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51983387E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.31154980E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.28822001E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.06352847E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.80764953E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18017865E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.59341512E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.55219460E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.13563421E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.56787935E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.91553613E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69635182E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62455925E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.19576667E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.09398770E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.42677639E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.51961220E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.33771016E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.56243993E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.98789421E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.43312373E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.78710050E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.71603888E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.79841287E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.29160404E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.55425550E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.07951076E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.57411985E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.07251902E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.94308202E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64387544E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.40099774E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.19896282E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.29040754E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.04527135E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.00628573E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18882416E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59133563E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.30746801E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.41919654E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.88797146E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40866399E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.07754773E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.84519843E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64370241E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.83283073E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.61345160E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28466358E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.29783813E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.01317899E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67162001E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.53545140E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.57642409E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.83206525E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.94698821E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54645476E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.07251902E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.94308202E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64387544E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.40099774E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.19896282E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.29040754E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.04527135E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.00628573E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18882416E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59133563E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.30746801E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.41919654E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.88797146E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40866399E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.07754773E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.84519843E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64370241E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.83283073E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.61345160E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28466358E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.29783813E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.01317899E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67162001E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.53545140E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.57642409E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.83206525E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.94698821E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54645476E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02118896E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.75072288E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00882831E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.38460707E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.03061368E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01609927E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.09605634E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56109329E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.44842447E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.76076704E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.40333427E-11    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02118896E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.75072288E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00882831E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.38460707E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.03061368E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01609927E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.09605634E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56109329E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999855E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.44842447E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.76076704E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.40333427E-11    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.81946192E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.46242460E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17979386E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35778154E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76094863E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.54323060E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15270090E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.06887114E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.99067493E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30376742E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.04296181E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02149361E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.69291906E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00977569E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.07347241E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.93741662E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02093237E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.69524930E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02149361E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.69291906E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00977569E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.07347241E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.93741662E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02093237E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.69524930E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02200723E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.69210336E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00952455E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.18094320E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.03955691E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02126069E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.38064131E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.13722698E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.42038562E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.41901461E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.53166372E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.26248707E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.16960848E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.95036741E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.01329144E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.35989672E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.83462065E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     9.42340643E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.02758147E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.98127169E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.01535124E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.01535124E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.65763653E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.48666302E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.32963590E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.32963590E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.00758904E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.00758904E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     6.08209837E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.08209837E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.45753173E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.58130783E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.17904341E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.99330253E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.99330253E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.20592028E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.04756730E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.23277333E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.23277333E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.98358547E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.98358547E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.78251438E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.78251438E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.70673766E-03   # h decays
#          BR         NDA      ID1       ID2
     5.55985957E-01    2           5        -5   # BR(h -> b       bb     )
     7.02744718E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48771003E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.27554236E-04    2           3        -3   # BR(h -> s       sb     )
     2.29225389E-02    2           4        -4   # BR(h -> c       cb     )
     7.50826539E-02    2          21        21   # BR(h -> g       g      )
     2.58625935E-03    2          22        22   # BR(h -> gam     gam    )
     1.77255767E-03    2          22        23   # BR(h -> Z       gam    )
     2.40301039E-01    2          24       -24   # BR(h -> W+      W-     )
     3.02981971E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.80900150E+01   # H decays
#          BR         NDA      ID1       ID2
     9.02540322E-01    2           5        -5   # BR(H -> b       bb     )
     6.52869365E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.30839006E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.83470792E-04    2           3        -3   # BR(H -> s       sb     )
     7.94589161E-08    2           4        -4   # BR(H -> c       cb     )
     7.92653685E-03    2           6        -6   # BR(H -> t       tb     )
     7.47449319E-06    2          21        21   # BR(H -> g       g      )
     4.91947126E-08    2          22        22   # BR(H -> gam     gam    )
     3.24231574E-09    2          23        22   # BR(H -> Z       gam    )
     7.55734322E-07    2          24       -24   # BR(H -> W+      W-     )
     3.77401036E-07    2          23        23   # BR(H -> Z       Z      )
     5.30360610E-06    2          25        25   # BR(H -> h       h      )
    -6.89349284E-26    2          36        36   # BR(H -> A       A      )
     2.95215848E-19    2          23        36   # BR(H -> Z       A      )
     8.75058664E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.06969536E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.37921833E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.67604903E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.20911402E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.42932754E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.72522016E+01   # A decays
#          BR         NDA      ID1       ID2
     9.22854930E-01    2           5        -5   # BR(A -> b       bb     )
     6.67534760E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.36024009E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.89886494E-04    2           3        -3   # BR(A -> s       sb     )
     8.18018598E-08    2           4        -4   # BR(A -> c       cb     )
     8.15576937E-03    2           6        -6   # BR(A -> t       tb     )
     2.40179605E-05    2          21        21   # BR(A -> g       g      )
     5.68105216E-08    2          22        22   # BR(A -> gam     gam    )
     2.36370696E-08    2          23        22   # BR(A -> Z       gam    )
     7.69768855E-07    2          23        25   # BR(A -> Z       h      )
     9.10863239E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.19406270E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.55821698E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.76338979E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.05769078E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47116588E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.13064396E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16764617E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.41545009E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.27386974E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.62075969E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.25490821E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.07918116E-07    2          24        25   # BR(H+ -> W+      h      )
     5.22217624E-14    2          24        36   # BR(H+ -> W+      A      )
     5.05925800E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.41834264E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07239451E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
