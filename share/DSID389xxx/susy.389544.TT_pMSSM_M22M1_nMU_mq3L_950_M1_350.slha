#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871824E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04021774E+01   # W+
        25     1.24780142E+02   # h
        35     3.00001808E+03   # H
        36     2.99999994E+03   # A
        37     3.00108435E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04190281E+03   # ~d_L
   2000001     3.03674801E+03   # ~d_R
   1000002     3.04099783E+03   # ~u_L
   2000002     3.03806464E+03   # ~u_R
   1000003     3.04190281E+03   # ~s_L
   2000003     3.03674801E+03   # ~s_R
   1000004     3.04099783E+03   # ~c_L
   2000004     3.03806464E+03   # ~c_R
   1000005     1.05195771E+03   # ~b_1
   2000005     3.03427311E+03   # ~b_2
   1000006     1.04979764E+03   # ~t_1
   2000006     3.02457768E+03   # ~t_2
   1000011     3.00642197E+03   # ~e_L
   2000011     3.00183656E+03   # ~e_R
   1000012     3.00503624E+03   # ~nu_eL
   1000013     3.00642197E+03   # ~mu_L
   2000013     3.00183656E+03   # ~mu_R
   1000014     3.00503624E+03   # ~nu_muL
   1000015     2.98612314E+03   # ~tau_1
   2000015     3.02158482E+03   # ~tau_2
   1000016     3.00487822E+03   # ~nu_tauL
   1000021     2.35218684E+03   # ~g
   1000022     3.52032193E+02   # ~chi_10
   1000023     7.35889025E+02   # ~chi_20
   1000025    -2.99497753E+03   # ~chi_30
   1000035     2.99534919E+03   # ~chi_40
   1000024     7.36052597E+02   # ~chi_1+
   1000037     2.99612090E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99889505E-01   # N_11
  1  2    -5.59511700E-05   # N_12
  1  3    -1.48319596E-02   # N_13
  1  4    -9.94044554E-04   # N_14
  2  1     4.61559346E-04   # N_21
  2  2     9.99622537E-01   # N_22
  2  3     2.70093424E-02   # N_23
  2  4     5.00663671E-03   # N_24
  3  1    -9.78138734E-03   # N_31
  3  2     1.55618690E-02   # N_32
  3  3    -7.06857581E-01   # N_33
  3  4     7.07117044E-01   # N_34
  4  1     1.11843509E-02   # N_41
  4  2    -2.26408729E-02   # N_42
  4  3     7.06684420E-01   # N_43
  4  4     7.07078095E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99270372E-01   # U_11
  1  2     3.81932408E-02   # U_12
  2  1    -3.81932408E-02   # U_21
  2  2     9.99270372E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99974947E-01   # V_11
  1  2    -7.07845853E-03   # V_12
  2  1    -7.07845853E-03   # V_21
  2  2    -9.99974947E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98450622E-01   # cos(theta_t)
  1  2    -5.56449048E-02   # sin(theta_t)
  2  1     5.56449048E-02   # -sin(theta_t)
  2  2     9.98450622E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99714320E-01   # cos(theta_b)
  1  2    -2.39014306E-02   # sin(theta_b)
  2  1     2.39014306E-02   # -sin(theta_b)
  2  2     9.99714320E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06962165E-01   # cos(theta_tau)
  1  2     7.07251368E-01   # sin(theta_tau)
  2  1    -7.07251368E-01   # -sin(theta_tau)
  2  2    -7.06962165E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00149099E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68718237E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43852667E+02   # higgs               
         4     1.02155929E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68718237E+03  # The gauge couplings
     1     3.62544838E-01   # gprime(Q) DRbar
     2     6.37039329E-01   # g(Q) DRbar
     3     1.02367070E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68718237E+03  # The trilinear couplings
  1  1     2.71591200E-06   # A_u(Q) DRbar
  2  2     2.71595145E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68718237E+03  # The trilinear couplings
  1  1     9.66987179E-07   # A_d(Q) DRbar
  2  2     9.67098136E-07   # A_s(Q) DRbar
  3  3     1.94515190E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68718237E+03  # The trilinear couplings
  1  1     4.01401530E-07   # A_e(Q) DRbar
  2  2     4.01421053E-07   # A_mu(Q) DRbar
  3  3     4.06875323E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68718237E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49926750E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68718237E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.74819348E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68718237E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01306646E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68718237E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.94971516E+04   # M^2_Hd              
        22    -9.02479795E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40858034E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.13307319E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47878717E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47878717E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52121283E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52121283E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.26167382E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     3.50737568E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.74682306E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.90243938E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11184000E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.82481271E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.59007972E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.22180943E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.47760955E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.02981408E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66561172E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59450235E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11530108E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.08670582E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.05232481E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.69866135E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.89610617E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.24398492E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.11466282E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.24771610E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.73693854E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.55863779E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.52713377E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.60479941E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.83895006E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.79404423E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.41930055E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.67557580E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.13773960E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55725899E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.39710633E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.49472368E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.11599928E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.72574696E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.26536307E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14663536E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58024665E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.07323771E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.41262943E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.81612814E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41975272E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.68080538E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.14214934E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55608346E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.69087718E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.13267226E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.11035063E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.21714405E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.27213750E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64453879E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.49277643E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.73675134E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.78476428E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.86322395E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55072218E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.67557580E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.13773960E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55725899E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.39710633E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.49472368E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.11599928E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.72574696E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.26536307E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14663536E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58024665E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.07323771E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.41262943E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.81612814E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41975272E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.68080538E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.14214934E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55608346E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.69087718E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.13267226E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.11035063E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.21714405E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.27213750E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64453879E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.49277643E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.73675134E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.78476428E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.86322395E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55072218E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59636706E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.06276351E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98163501E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.94668357E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.83251777E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95560095E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.60741205E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52667404E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999802E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.93528874E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.04995173E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.39789438E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59636706E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.06276351E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98163501E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.94668357E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.83251777E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95560095E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.60741205E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52667404E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999802E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.93528874E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.04995173E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.39789438E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58505067E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68835197E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10586334E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20578469E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53550186E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.77126737E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07811599E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.67722313E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45998402E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15013318E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.69730899E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59636576E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06266466E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97678080E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.72586105E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.17580784E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96055434E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.18651393E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59636576E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06266466E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97678080E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.72586105E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.17580784E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96055434E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.18651393E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59646682E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06257581E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97650068E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.51669919E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.13784758E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96090122E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.21180543E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.38407100E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.41203568E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.29795453E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.54528564E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.46013526E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.41818517E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.11171167E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.24402862E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.79364826E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.24146083E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.38653754E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.61346246E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.42869894E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.22861208E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.34558792E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.22762398E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.22762398E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.77067456E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.09545115E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.30833628E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.30833628E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.36562654E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.36562654E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.74223650E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.74223650E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.33848410E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.50536487E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.08014189E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.29791325E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.29791325E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.29741924E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.73500003E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.27353622E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.27353622E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.43515596E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.43515596E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.97255279E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.97255279E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.58307355E-03   # h decays
#          BR         NDA      ID1       ID2
     5.66288550E-01    2           5        -5   # BR(h -> b       bb     )
     7.22415782E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.55738418E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.42993852E-04    2           3        -3   # BR(h -> s       sb     )
     2.35940309E-02    2           4        -4   # BR(h -> c       cb     )
     7.63480200E-02    2          21        21   # BR(h -> g       g      )
     2.60681191E-03    2          22        22   # BR(h -> gam     gam    )
     1.71000531E-03    2          22        23   # BR(h -> Z       gam    )
     2.27995961E-01    2          24       -24   # BR(h -> W+      W-     )
     2.84163107E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.43439398E+01   # H decays
#          BR         NDA      ID1       ID2
     8.94474226E-01    2           5        -5   # BR(H -> b       bb     )
     7.24061208E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.56010740E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.14382893E-04    2           3        -3   # BR(H -> s       sb     )
     8.81216670E-08    2           4        -4   # BR(H -> c       cb     )
     8.79069646E-03    2           6        -6   # BR(H -> t       tb     )
     1.36749044E-05    2          21        21   # BR(H -> g       g      )
     6.87436929E-08    2          22        22   # BR(H -> gam     gam    )
     3.60049125E-09    2          23        22   # BR(H -> Z       gam    )
     8.20452837E-07    2          24       -24   # BR(H -> W+      W-     )
     4.09720641E-07    2          23        23   # BR(H -> Z       Z      )
     6.16449843E-06    2          25        25   # BR(H -> h       h      )
     7.12194952E-25    2          36        36   # BR(H -> A       A      )
     1.06883657E-19    2          23        36   # BR(H -> Z       A      )
     7.26749755E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.30560108E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.63093128E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.57511291E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23419115E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.01149495E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.35909644E+01   # A decays
#          BR         NDA      ID1       ID2
     9.14564212E-01    2           5        -5   # BR(A -> b       bb     )
     7.40292518E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.61749378E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.21482589E-04    2           3        -3   # BR(A -> s       sb     )
     9.07178298E-08    2           4        -4   # BR(A -> c       cb     )
     9.04470509E-03    2           6        -6   # BR(A -> t       tb     )
     2.66357910E-05    2          21        21   # BR(A -> g       g      )
     6.99083087E-08    2          22        22   # BR(A -> gam     gam    )
     2.62434647E-08    2          23        22   # BR(A -> Z       gam    )
     8.35754093E-07    2          23        25   # BR(A -> Z       h      )
     9.43085287E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.59663954E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.71095099E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.90794145E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.69535544E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46032326E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.73172819E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.38017489E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.34605734E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.39876833E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.87771624E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.19444932E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.61004921E-07    2          24        25   # BR(H+ -> W+      h      )
     5.33930748E-14    2          24        36   # BR(H+ -> W+      A      )
     5.03661254E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.67995667E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07239173E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
