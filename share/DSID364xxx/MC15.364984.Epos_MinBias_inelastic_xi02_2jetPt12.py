evgenConfig.description = "Low-pT inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minBias"]
## Base config for Epos
from Epos_i.Epos_iConf import Epos
genSeq += Epos("Epos")
evgenConfig.generators += ["Epos"]

genSeq.Epos.BeamMomentum     = -6500.0
genSeq.Epos.TargetMomentum   = 6500.0
genSeq.Epos.PrimaryParticle  = 1
genSeq.Epos.TargetParticle   = 1
genSeq.Epos.Model            = 0
genSeq.Epos.ParamFile        = "epos_crmc.param"

## Get files from the InstallArea
import os
os.system("get_files %s" % genSeq.Epos.ParamFile)
inputFiles = "qgsjet.dat \
              qgsjet.ncs \
              sectnu-II-03 \
              epos.initl \
              epos.iniev \
              epos.inirj \
              epos.inics \
              epos.inirj.lhc \
              epos.inics.lhc"
if not os.path.exists("tabs"):
    os.mkdir("tabs")
os.system("get_files %s" % inputFiles)
os.system("mv %s tabs/" % inputFiles)

from GeneratorFilters.GeneratorFiltersConf import ForwardProtonFilter
if  "ForwardProtonFilter" not in filtSeq:
    filtSeq += ForwardProtonFilter()

filtSeq.ForwardProtonFilter.xi_min = 0.00
filtSeq.ForwardProtonFilter.xi_max = 0.20
filtSeq.ForwardProtonFilter.beam_energy = 6500.*GeV
filtSeq.ForwardProtonFilter.pt_min = 0.0*GeV
filtSeq.ForwardProtonFilter.pt_max = 1.5*GeV

evgenConfig.findJets = True

from JetRec.JetRecFlags import jetFlags
jetFlags.truthFlavorTags = []

jetFlags.useTracks = False

from JetRec.JetRecStandard import jtm
jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth",
                 modifiersin="none",
                 ptmin=7000.)
        
from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in filtSeq:
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.DoShape = False
filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

