#Pythia8 minimum bias CD with A3

evgenConfig.description = "Central-diffractive events, with the A3 MSTW2008LO tune. Default PomFlux4"
evgenConfig.keywords = ["QCD", "minBias","diffraction","CD"]
evgenConfig.generators = ["Pythia8"]


include("Pythia8_i/Pythia8_A2_MSTW2008LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:centralDiffractive = on"]
genSeq.Pythia8.Commands += ["SigmaTotal:zeroAXB = off"]
genSeq.Pythia8.Commands += ["SigmaDiffractive:mMinCD = 0.5"] 


from AthenaCommon.SystemOfUnits import GeV

from GeneratorFilters.GeneratorFiltersConf import ForwardProtonFilter
if  "ForwardProtonFilter" not in filtSeq:
    filtSeq += ForwardProtonFilter()

filtSeq.ForwardProtonFilter.xi_min = 0.000
filtSeq.ForwardProtonFilter.xi_max = 0.050
filtSeq.ForwardProtonFilter.beam_energy = 6500.*GeV
filtSeq.ForwardProtonFilter.pt_min = 0.1*GeV
filtSeq.ForwardProtonFilter.pt_max = 1.2*GeV
#filtSeq.ForwardProtonFilter.double_tag = True



from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
if "ChargedTracksFilter" not in filtSeq:
    filtSeq += ChargedTracksFilter()


filtSeq.ChargedTracksFilter.Ptcut = 0.15*GeV
filtSeq.ChargedTracksFilter.Etacut = 2.55
filtSeq.ChargedTracksFilter.NTracks = 1
filtSeq.ChargedTracksFilter.NTracksMax = 7
