#--------------------------------------------------------------
# Read MadGraph output LHEF, Showering with Pythia8, AU2 tune
#--------------------------------------------------------------
include("MC15JobOptions/nonStandard/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
if (runArgs.runNumber == 343363):
    evgenConfig.description = "SM p p > l nu j j gam gam, qcd=2 at 13TeV"
    evgenConfig.keywords = ["SM","diphoton"]

evgenConfig.contact = ['Xiaohu Sun <Xiaohu.Sun@cern.ch>']
evgenConfig.inputfilecheck = 'group.phys-gener.MadGraph5.343363.sm_lnugamgamjj_qcd2_13TeV.TXT.mc15_v1'

