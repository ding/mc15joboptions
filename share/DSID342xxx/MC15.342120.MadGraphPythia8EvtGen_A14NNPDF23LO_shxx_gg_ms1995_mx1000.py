include("MC15JobOptions/MadGraphControl_monoHiggs_scalar.py")

evgenConfig.description = "Simplified Model of scalar mediator for\
MonoHiggs(h->gamgam) with mDM="+str(mDM)+"GeV and mS="+str(mS)+"GeV"
evgenConfig.keywords = [ "BSMHiggs", "Higgs", "diphoton", "scalar","simplifiedModel"]
evgenConfig.contact = ['Lashkar Kashif <lashkar.kashif@cern.ch>']

genSeq.Pythia8.Commands += [
    			    "25:onMode=off",
			    "25:onIfMatch = 22 22"
]

if not hasattr( filtSeq, "DiPhotonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
    filtSeq += DiPhotonFilter()
    
DiPhotonFilter = filtSeq.DiPhotonFilter
DiPhotonFilter.PtCut1st = 30000.
DiPhotonFilter.PtCut2nd = 25000.
