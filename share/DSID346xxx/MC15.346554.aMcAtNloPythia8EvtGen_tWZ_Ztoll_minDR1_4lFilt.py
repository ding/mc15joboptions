include('MC15JobOptions/MadGraphControl_aMcAtNloPythia8_tWZ_Ztoll_minDR1.py')

evgenConfig.minevents = 500

include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut    = 8000.
filtSeq.MultiLeptonFilter.Etacut   = 3.
filtSeq.MultiLeptonFilter.NLeptons = 3

include("MC15JobOptions/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt           = 4000.
filtSeq.FourLeptonMassFilter.MaxEta          = 4.
filtSeq.FourLeptonMassFilter.MinMass1        = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1        = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2        = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2        = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu     = False
filtSeq.FourLeptonMassFilter.AllowSameCharge = False

evgenConfig.description = "MG tWZ sample with 4l filter"
evgenConfig.contact     = [ 'syed.haider.abidi@cern.ch' ]

