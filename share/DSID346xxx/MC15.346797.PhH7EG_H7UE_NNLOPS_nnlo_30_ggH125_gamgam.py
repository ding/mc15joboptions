#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nnlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# only consider H->gammagamma devays
Herwig7Config.add_commands("""
  # force H->yy decays
  do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma;
  # print out decays modes and branching ratios to the terminal/log.generate 
  do /Herwig/Particles/h0:PrintDecayModes 
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'Powheg+Herwig7 H+jet production with NNLOPS and with H7UE tune'
evgenConfig.keywords       = [ 'Higgs', '1jet' ]
evgenConfig.contact        = [ 'ana.cueto@cern.ch' ]
evgenConfig.generators    += [ 'Powheg', 'Herwig7' ]
# 2200 events per file
evgenConfig.minevents      = 2000
evgenConfig.tune    = "H7UE"
