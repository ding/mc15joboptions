#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.45402667E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05415429E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414538E+03   # H
        36     2.00000000E+03   # A
        37     2.00175703E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013260E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989271E+03   # ~u_L
   2000002     4.99994938E+03   # ~u_R
   1000003     5.00013260E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989271E+03   # ~c_L
   2000004     4.99994938E+03   # ~c_R
   1000005     4.99949089E+03   # ~b_1
   2000005     5.00066840E+03   # ~b_2
   1000006     4.98750867E+03   # ~t_1
   2000006     5.01692266E+03   # ~t_2
   1000011     5.00008198E+03   # ~e_L
   2000011     5.00007594E+03   # ~e_R
   1000012     4.99984208E+03   # ~nu_eL
   1000013     5.00008198E+03   # ~mu_L
   2000013     5.00007594E+03   # ~mu_R
   1000014     4.99984208E+03   # ~nu_muL
   1000015     4.99969305E+03   # ~tau_1
   2000015     5.00046546E+03   # ~tau_2
   1000016     4.99984208E+03   # ~nu_tauL
   1000021     1.80000000E+03   # ~g
   1000022     1.44323185E+03   # ~chi_10
   1000023    -1.45200041E+03   # ~chi_20
   1000025     1.46129290E+03   # ~chi_30
   1000035     3.00150232E+03   # ~chi_40
   1000024     1.45119254E+03   # ~chi_1+
   1000037     3.00150204E+03   # ~chi_2+
   1000039     1.16148254E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.05060121E-01   # N_11
  1  2    -7.45714276E-03   # N_12
  1  3    -5.69505146E-01   # N_13
  1  4    -5.56336706E-01   # N_14
  2  1     1.49896346E-02   # N_21
  2  2    -1.73907418E-02   # N_22
  2  3     7.06822924E-01   # N_23
  2  4    -7.07017841E-01   # N_24
  3  1     7.96038481E-01   # N_31
  3  2     6.68144312E-03   # N_32
  3  3     4.19568533E-01   # N_33
  3  4     4.36165497E-01   # N_34
  4  1     5.46095233E-04   # N_41
  4  2    -9.99798635E-01   # N_42
  4  3    -5.24302629E-03   # N_43
  4  4     1.93623608E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.40441150E-03   # U_11
  1  2     9.99972587E-01   # U_12
  2  1     9.99972587E-01   # U_21
  2  2     7.40441150E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.73833876E-02   # V_11
  1  2    -9.99625005E-01   # V_12
  2  1     9.99625005E-01   # V_21
  2  2     2.73833876E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07787260E-01   # cos(theta_t)
  1  2    -7.06425647E-01   # sin(theta_t)
  2  1     7.06425647E-01   # -sin(theta_t)
  2  2     7.07787260E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.74124100E-01   # cos(theta_b)
  1  2     7.38618100E-01   # sin(theta_b)
  2  1    -7.38618100E-01   # -sin(theta_b)
  2  2    -6.74124100E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04336186E-01   # cos(theta_tau)
  1  2     7.09866563E-01   # sin(theta_tau)
  2  1    -7.09866563E-01   # -sin(theta_tau)
  2  2    -7.04336186E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90198283E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51924685E+02   # vev(Q)              
         4     4.53770785E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52751738E-01   # gprime(Q) DRbar
     2     6.26977223E-01   # g(Q) DRbar
     3     1.08213180E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02691243E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72809145E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79696618E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.45402667E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.96133365E+05   # M^2_Hd              
        22    -7.40475174E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36960055E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.83229378E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.77939551E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     9.83974806E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.64801741E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.33418115E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     8.66167797E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.18432489E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.02877693E-05    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.01383352E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.64801741E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.33418115E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     8.66167797E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.18432489E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.02877693E-05    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.01383352E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.01447521E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.48559774E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     8.65099219E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.15258552E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.89811243E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.66482797E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.66482797E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.66482797E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.66482797E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.44027294E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.44027294E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.99582101E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.87338569E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.72426316E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.92618447E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.38553148E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.69610604E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.76687334E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.46276558E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.12487439E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.45028831E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.01400172E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.71974680E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.15966476E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.02066653E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.31482971E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.50087879E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.38965053E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.03897154E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.98970855E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.17567291E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.50062256E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.18347021E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.00640656E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.30328146E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.45025806E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.43269105E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.64516413E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.02188352E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.69970056E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.32701933E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.40523231E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.10629648E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.22337078E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.02063563E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.51353273E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.72808997E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.48404261E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.06805941E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.96709394E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.92836540E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.07417252E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57596162E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.65095851E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.71538457E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.27161435E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.57076881E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.22341644E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.04365101E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.90607604E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.44473121E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.48594011E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.80947517E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.97287930E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.92886553E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00744863E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.07094384E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.49298665E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.01424425E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.62009744E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88912317E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.22337078E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.02063563E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.51353273E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.72808997E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.48404261E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.06805941E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.96709394E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.92836540E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.07417252E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57596162E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.65095851E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.71538457E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.27161435E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.57076881E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.22341644E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.04365101E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.90607604E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.44473121E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.48594011E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.80947517E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.97287930E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.92886553E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00744863E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.07094384E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.49298665E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.01424425E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.62009744E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88912317E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.91933522E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.23923547E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.50529061E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.15747773E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73741943E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.15850657E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.48011292E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07422343E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.67161893E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.24843289E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.32613118E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.45626870E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.91933522E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.23923547E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.50529061E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.15747773E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73741943E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.15850657E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.48011292E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07422343E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.67161893E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.24843289E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.32613118E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.45626870E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.49637617E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.95508461E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.73388398E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.27200740E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.58837116E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.00660210E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.17979624E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.06799131E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50526452E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.82579966E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.14034930E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.32984389E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60712563E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.47397427E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21735336E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.91914574E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.81069271E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.74481664E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.09042193E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.74053647E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.42303000E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.47580449E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.91914574E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.81069271E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.74481664E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.09042193E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.74053647E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.42303000E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.47580449E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92183984E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.80441286E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.74136370E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.08941650E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.73800954E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.76374207E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.47075390E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.11239897E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.28512209E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.54200916E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.03993827E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18067620E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17963760E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.29226563E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.98919875E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53372821E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48666988E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46343543E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.91812248E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52435421E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.46117912E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.43148391E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.09380497E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.50917312E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.39702191E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.92672337E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.40982077E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.01462384E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.91598360E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40209343E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81553185E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17691443E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80856347E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.14653006E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.14345677E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.18774507E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.28103728E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.28103728E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.28103728E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.95228446E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.95228446E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.65076159E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.65076159E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.51856414E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.51856414E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.77127201E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.01887526E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.14864803E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.97322743E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.11719605E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     9.90011325E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.32109099E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.14431984E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.31939650E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.30276571E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.34001221E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.33830771E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.90273620E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.86158405E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.86158405E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.86158405E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.17222093E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.10765030E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.71803790E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.09360524E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     9.38163379E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.37543106E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.45654216E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.87354768E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.87354768E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.87354768E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.62311059E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.62311059E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.47518136E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.47518136E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.41031449E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.41031449E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.40735679E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.40735679E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.65749484E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.65749484E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.98938338E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.20742993E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.23186120E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.06636984E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46673562E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46673562E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.16281359E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.90294824E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.10046100E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     8.32631834E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.62801608E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.98902545E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.74759750E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.74668653E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07726900E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16992101E-01    2           5        -5   # BR(h -> b       bb     )
     6.38794907E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26109870E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79147322E-04    2           3        -3   # BR(h -> s       sb     )
     2.06717791E-02    2           4        -4   # BR(h -> c       cb     )
     6.71214059E-02    2          21        21   # BR(h -> g       g      )
     2.30331783E-03    2          22        22   # BR(h -> gam     gam    )
     1.54070114E-03    2          22        23   # BR(h -> Z       gam    )
     2.01115909E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56700386E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78124239E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48234803E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430476E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71223478E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547401E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666761E-05    2           4        -4   # BR(H -> c       cb     )
     9.96054769E-01    2           6        -6   # BR(H -> t       tb     )
     7.97608481E-04    2          21        21   # BR(H -> g       g      )
     2.73784675E-06    2          22        22   # BR(H -> gam     gam    )
     1.16011688E-06    2          23        22   # BR(H -> Z       gam    )
     3.33538003E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66314212E-04    2          23        23   # BR(H -> Z       Z      )
     9.03040849E-04    2          25        25   # BR(H -> h       h      )
     8.72448330E-24    2          36        36   # BR(H -> A       A      )
     3.49317003E-11    2          23        36   # BR(H -> Z       A      )
     5.40361172E-12    2          24       -37   # BR(H -> W+      H-     )
     5.40361172E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82386133E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48513270E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895256E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62257838E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676247E-06    2           3        -3   # BR(A -> s       sb     )
     9.96166420E-06    2           4        -4   # BR(A -> c       cb     )
     9.96985920E-01    2           6        -6   # BR(A -> t       tb     )
     9.43666114E-04    2          21        21   # BR(A -> g       g      )
     3.02532561E-06    2          22        22   # BR(A -> gam     gam    )
     1.35273772E-06    2          23        22   # BR(A -> Z       gam    )
     3.25046711E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74519109E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38912894E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237206E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143557E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51905404E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45684325E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729666E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403327E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33222583E-04    2          24        25   # BR(H+ -> W+      h      )
     5.87932345E-13    2          24        36   # BR(H+ -> W+      A      )
