#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.15500058E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -2.15000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05425155E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414336E+03   # H
        36     2.00000000E+03   # A
        37     2.00171360E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013256E+03   # ~d_L
   2000001     5.00002533E+03   # ~d_R
   1000002     4.99989276E+03   # ~u_L
   2000002     4.99994934E+03   # ~u_R
   1000003     5.00013256E+03   # ~s_L
   2000003     5.00002533E+03   # ~s_R
   1000004     4.99989276E+03   # ~c_L
   2000004     4.99994934E+03   # ~c_R
   1000005     4.99920699E+03   # ~b_1
   2000005     5.00095221E+03   # ~b_2
   1000006     4.98037013E+03   # ~t_1
   2000006     5.02401602E+03   # ~t_2
   1000011     5.00008191E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008191E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99950672E+03   # ~tau_1
   2000015     5.00065174E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     2.14342040E+03   # ~chi_10
   1000023    -2.15160487E+03   # ~chi_20
   1000025     2.16173885E+03   # ~chi_30
   1000035     3.00144620E+03   # ~chi_40
   1000024     2.15088181E+03   # ~chi_1+
   1000037     3.00144587E+03   # ~chi_2+
   1000039     3.60481836E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.86321305E-01   # N_11
  1  2    -1.40813309E-02   # N_12
  1  3    -5.77097048E-01   # N_13
  1  4    -5.68320368E-01   # N_14
  2  1     1.01190199E-02   # N_21
  2  2    -1.50276586E-02   # N_22
  2  3     7.06937971E-01   # N_23
  2  4    -7.07043478E-01   # N_24
  3  1     8.10014820E-01   # N_31
  3  2     1.15670440E-02   # N_32
  3  3     4.08892059E-01   # N_33
  3  4     4.20177913E-01   # N_34
  4  1     9.61495880E-04   # N_41
  4  2    -9.99721006E-01   # N_42
  4  3     2.23296741E-03   # N_43
  4  4     2.34946865E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     3.18025574E-03   # U_11
  1  2     9.99994943E-01   # U_12
  2  1     9.99994943E-01   # U_21
  2  2    -3.18025574E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.32342524E-02   # V_11
  1  2    -9.99447590E-01   # V_12
  2  1     9.99447590E-01   # V_21
  2  2     3.32342524E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07564759E-01   # cos(theta_t)
  1  2    -7.06648507E-01   # sin(theta_t)
  2  1     7.06648507E-01   # -sin(theta_t)
  2  2     7.07564759E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.85038068E-01   # cos(theta_b)
  1  2     7.28507272E-01   # sin(theta_b)
  2  1    -7.28507272E-01   # -sin(theta_b)
  2  2    -6.85038068E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05275736E-01   # cos(theta_tau)
  1  2     7.08933097E-01   # sin(theta_tau)
  2  1    -7.08933097E-01   # -sin(theta_tau)
  2  2    -7.05275736E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90196836E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -2.15000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52058957E+02   # vev(Q)              
         4     4.91275286E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52681651E-01   # gprime(Q) DRbar
     2     6.26531910E-01   # g(Q) DRbar
     3     1.07892169E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02711595E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73168032E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79559383E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.15500058E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -2.12328750E+06   # M^2_Hd              
        22    -9.68838659E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36761437E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.38980515E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.34964661E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     7.57903317E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.97005540E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.87873683E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.00294027E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.26217509E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.20123277E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.54586065E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.97005540E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.87873683E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.00294027E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.26217509E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.20123277E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.54586065E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.30503749E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     9.24191080E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.98043990E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.51303205E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.51303205E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.51303205E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.51303205E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.52287478E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.80772423E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.00348835E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.23329761E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.66223983E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     8.97975300E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.31434274E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.59991542E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.67924117E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.97227726E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.46582198E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.34977711E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.33607424E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.53265452E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.66224691E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.66811480E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.07472680E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.67612853E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.41854867E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.93777453E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.76189846E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.09508270E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.53569241E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.31857733E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.11531342E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.27186373E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.35093654E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.77776107E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.93052970E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.19544676E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.87405649E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.17224743E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96389618E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.88528290E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.09847793E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.69749003E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.93727129E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.40820102E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.87307919E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.79458672E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.79759417E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.35295891E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.01606664E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.56244495E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.24227521E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60841923E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96396235E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.84842965E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.82592655E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.25132245E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.94059329E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.28956158E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.88266291E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.79511724E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.74485339E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.48473605E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.03439460E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.59993939E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.77541233E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89914284E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96389618E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.88528290E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.09847793E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.69749003E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.93727129E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.40820102E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.87307919E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.79458672E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.79759417E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.35295891E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.01606664E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.56244495E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.24227521E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60841923E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96396235E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.84842965E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.82592655E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.25132245E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.94059329E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.28956158E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.88266291E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.79511724E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.74485339E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.48473605E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.03439460E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.59993939E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.77541233E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89914284E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80737497E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.62565855E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.02073012E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.00497762E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.84089498E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.34294723E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.69106605E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.64032302E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.45521800E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.02561289E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.54375068E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.70652170E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80737497E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.62565855E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.02073012E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.00497762E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.84089498E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.34294723E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.69106605E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.64032302E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.45521800E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.02561289E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.54375068E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.70652170E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.22326434E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.62126204E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.95627918E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.01492287E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.78337011E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.93561093E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.57255308E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.24469893E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.23082480E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.51335884E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     8.85535008E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.07478165E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.79779242E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.74976893E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.60146197E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80717692E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.48688374E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.98301218E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.07963726E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.84681035E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.02028740E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.68435167E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80717692E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.48688374E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.98301218E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.07963726E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.84681035E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.02028740E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.68435167E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80930744E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.48272260E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.98150831E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.07275145E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.84465139E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.77830789E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.68003662E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.56685212E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.29494754E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     4.18162341E-06    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     3.57522103E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.00513749E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.19174635E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.19055323E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.07805325E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.10207151E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.54067656E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49124677E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.47240644E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.98786849E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.49688338E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.02363866E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.69203097E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.81767957E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51572798E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.66521175E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
     5.00103986E-06    2     1000039        35   # BR(~chi_10 -> ~G        H)
     1.33069461E-04    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.98380201E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.56268997E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.04144763E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.04933067E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
     3.22129542E-06    2     1000039        35   # BR(~chi_20 -> ~G        H)
     1.43154498E-07    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.41121069E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82749410E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15154280E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81945016E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.17459109E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.17104683E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.06287812E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.33752419E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.33752419E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.33752419E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.89206074E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.89206074E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.29735370E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.29735370E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.16935298E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.16935298E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.31896240E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.67717622E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.49339986E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.70205975E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     8.07939567E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.07541743E-09    2     1000039        35   # BR(~chi_30 -> ~G        H)
     3.48374435E-08    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     6.33027505E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.56197598E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.88178622E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.55163255E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.55414170E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.26405919E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.26293633E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.98446433E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.75037142E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.75037142E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.75037142E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.75143460E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.15306149E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.17872088E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.13535749E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.27068556E-05    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.40556480E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.40478099E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.16424167E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.80713673E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.80713673E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.80713673E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.26277247E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.26277247E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     2.08283092E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     2.08283092E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     7.54249664E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     7.54249664E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     7.53892763E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     7.53892763E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     6.62327796E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     6.62327796E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     2.10212843E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.92163678E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.48638318E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.61887784E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.47878552E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.47878552E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.31340608E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.01091775E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     6.59735937E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.46049452E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77302601E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.19755718E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     6.09347884E-15    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.08754496E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07413025E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16726489E-01    2           5        -5   # BR(h -> b       bb     )
     6.39284279E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26283090E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79514429E-04    2           3        -3   # BR(h -> s       sb     )
     2.06877449E-02    2           4        -4   # BR(h -> c       cb     )
     6.71715712E-02    2          21        21   # BR(h -> g       g      )
     2.30589179E-03    2          22        22   # BR(h -> gam     gam    )
     1.54194169E-03    2          22        23   # BR(h -> Z       gam    )
     2.01242321E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56898152E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78122710E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48709803E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431701E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71227811E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547975E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666646E-05    2           4        -4   # BR(H -> c       cb     )
     9.96053572E-01    2           6        -6   # BR(H -> t       tb     )
     7.97569240E-04    2          21        21   # BR(H -> g       g      )
     2.73276245E-06    2          22        22   # BR(H -> gam     gam    )
     1.16040428E-06    2          23        22   # BR(H -> Z       gam    )
     3.33098773E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66095234E-04    2          23        23   # BR(H -> Z       Z      )
     9.00188856E-04    2          25        25   # BR(H -> h       h      )
     8.59446621E-24    2          36        36   # BR(H -> A       A      )
     3.48475614E-11    2          23        36   # BR(H -> Z       A      )
     5.88853193E-12    2          24       -37   # BR(H -> W+      H-     )
     5.88853193E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82387776E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48982925E-03    2           5        -5   # BR(A -> b       bb     )
     2.43894208E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62254131E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675759E-06    2           3        -3   # BR(A -> s       sb     )
     9.96162138E-06    2           4        -4   # BR(A -> c       cb     )
     9.96981635E-01    2           6        -6   # BR(A -> t       tb     )
     9.43662058E-04    2          21        21   # BR(A -> g       g      )
     3.04866082E-06    2          22        22   # BR(A -> gam     gam    )
     1.35305968E-06    2          23        22   # BR(A -> Z       gam    )
     3.24617019E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74514193E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39948702E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49235070E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81136005E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52568325E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45681251E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729054E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403776E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32765941E-04    2          24        25   # BR(H+ -> W+      h      )
     5.18791345E-13    2          24        36   # BR(H+ -> W+      A      )
