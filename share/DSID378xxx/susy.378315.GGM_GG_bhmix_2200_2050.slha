#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.05484272E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -2.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05423967E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414396E+03   # H
        36     2.00000000E+03   # A
        37     2.00171854E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002533E+03   # ~d_R
   1000002     4.99989276E+03   # ~u_L
   2000002     4.99994935E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002533E+03   # ~s_R
   1000004     4.99989276E+03   # ~c_L
   2000004     4.99994935E+03   # ~c_R
   1000005     4.99924761E+03   # ~b_1
   2000005     5.00091160E+03   # ~b_2
   1000006     4.98139039E+03   # ~t_1
   2000006     5.02300391E+03   # ~t_2
   1000011     5.00008191E+03   # ~e_L
   2000011     5.00007598E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008191E+03   # ~mu_L
   2000013     5.00007598E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99953334E+03   # ~tau_1
   2000015     5.00062513E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     2.04339195E+03   # ~chi_10
   1000023    -2.05164937E+03   # ~chi_20
   1000025     2.06166056E+03   # ~chi_30
   1000035     3.00143958E+03   # ~chi_40
   1000024     2.05093454E+03   # ~chi_1+
   1000037     3.00143926E+03   # ~chi_2+
   1000039     3.14166509E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.89816540E-01   # N_11
  1  2    -1.25430991E-02   # N_12
  1  3    -5.75535901E-01   # N_13
  1  4    -5.66319298E-01   # N_14
  2  1     1.06115062E-02   # N_21
  2  2    -1.53250566E-02   # N_22
  2  3     7.06927063E-01   # N_23
  2  4    -7.07040781E-01   # N_24
  3  1     8.07467097E-01   # N_31
  3  2     1.04231389E-02   # N_32
  3  3     4.11110818E-01   # N_33
  3  4     4.22937514E-01   # N_34
  4  1     8.55806804E-04   # N_41
  4  2    -9.99749554E-01   # N_42
  4  3     6.70539720E-04   # N_43
  4  4     2.23527737E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.67393215E-04   # U_11
  1  2     9.99999532E-01   # U_12
  2  1     9.99999532E-01   # U_21
  2  2    -9.67393215E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.16174753E-02   # V_11
  1  2    -9.99500043E-01   # V_12
  2  1     9.99500043E-01   # V_21
  2  2     3.16174753E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07587196E-01   # cos(theta_t)
  1  2    -7.06626040E-01   # sin(theta_t)
  2  1     7.06626040E-01   # -sin(theta_t)
  2  2     7.07587196E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.83941670E-01   # cos(theta_b)
  1  2     7.29536697E-01   # sin(theta_b)
  2  1    -7.29536697E-01   # -sin(theta_b)
  2  2    -6.83941670E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05181717E-01   # cos(theta_tau)
  1  2     7.09026619E-01   # sin(theta_tau)
  2  1    -7.09026619E-01   # -sin(theta_tau)
  2  2    -7.05181717E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90197500E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -2.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52040210E+02   # vev(Q)              
         4     4.86069806E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52690147E-01   # gprime(Q) DRbar
     2     6.26585862E-01   # g(Q) DRbar
     3     1.07892198E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02713551E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73127684E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79577890E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.05484272E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.65412835E+06   # M^2_Hd              
        22    -9.29944840E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36785496E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.46619532E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.68796666E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.36020569E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.41374960E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.04799452E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.29863409E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.73383695E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.32941788E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     8.09670440E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.41374960E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.04799452E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.29863409E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.73383695E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.32941788E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     8.09670440E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.59438463E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.08987966E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.23865612E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.03574163E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.03574163E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.03574163E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.03574163E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.55499537E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.90522540E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.28227580E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.45865309E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.63510075E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.26476758E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.26163055E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.51923468E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.71018323E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.17695176E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.72427953E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.44478686E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.32599145E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.78492202E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.64347626E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.58995921E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.08421241E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.78704490E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.48686228E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.03480751E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.75123767E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.13410845E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.51267217E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.28083336E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.12679355E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.32591366E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.41702283E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.90283761E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.92340529E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.24012172E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.85797945E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.12803527E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96412215E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.41095788E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.18009410E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.73341729E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.93789693E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.32738668E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.87439428E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.79358035E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.80052884E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.42334225E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.59225047E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.64855132E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.77361829E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59276454E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96418673E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.01086631E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.99027044E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.31589606E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.94090976E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.24271815E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.88323098E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.79411803E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.74558786E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.67045275E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.18422961E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.82997010E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.57382871E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89498388E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96412215E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.41095788E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.18009410E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.73341729E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.93789693E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.32738668E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.87439428E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.79358035E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.80052884E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.42334225E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.59225047E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.64855132E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.77361829E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59276454E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96418673E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.01086631E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.99027044E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.31589606E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.94090976E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.24271815E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.88323098E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.79411803E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.74558786E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.67045275E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.18422961E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.82997010E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.57382871E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89498388E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.82498120E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.89448371E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.18124942E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.02928544E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.82418765E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.95049158E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.65665147E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.70906712E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.49522125E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.12767344E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.50364674E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.33931750E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.82498120E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.89448371E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.18124942E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.02928544E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.82418765E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.95049158E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.65665147E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.70906712E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.49522125E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.12767344E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.50364674E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.33931750E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.26650787E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.67886675E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.94124857E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.05956810E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.74974936E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.25730440E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.50461722E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.60752134E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.27419917E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.56796388E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.18666654E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.12012277E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.76433985E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.52969560E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.53385713E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.82478453E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.69329152E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.16849919E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.39036496E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.82939153E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     9.56065710E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.65051367E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.82478453E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.69329152E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.16849919E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.39036496E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.82939153E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     9.56065710E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.65051367E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.82700423E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.68882128E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.16679653E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.38299186E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.82716995E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.74086138E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.64607332E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.64607568E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.27850592E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     6.49801222E-08    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     3.56992977E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.01172748E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18998265E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.18881677E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.11692091E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.49033231E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53995534E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49039913E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.47078783E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.97580421E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.50127726E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.13759437E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.67096424E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.86754939E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51289224E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.61954325E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
     4.17413496E-08    2     1000039        35   # BR(~chi_10 -> ~G        H)
     1.47003540E-06    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.19802558E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     6.51661400E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.03879892E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.03651928E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
     4.59681609E-08    2     1000039        35   # BR(~chi_20 -> ~G        H)
     2.55454858E-09    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.41002247E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82593652E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15507332E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81803993E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.17094339E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.16746367E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.08018360E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.33018461E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.33018461E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.33018461E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.53626068E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.53626068E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.17875366E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.17875366E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.05993900E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.05993900E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.39815486E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.99039642E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.56600124E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.97875765E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     8.47131269E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.63871365E-11    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.02371218E-09    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     6.61574994E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.93070004E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.14048090E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.91978828E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.68360713E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.34637403E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.34520388E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.05346028E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.91946248E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.91946248E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.91946248E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.51687937E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.84925519E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.95910130E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.83201205E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.46451650E-05    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.33613727E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.33537415E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.10091316E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.66845987E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.66845987E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.66845987E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.15109839E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.15109839E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.97606121E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.97606121E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     7.17025462E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     7.17025462E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     7.16677842E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     7.16677842E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     6.27663323E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     6.27663323E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     2.49040660E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.91115730E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.32736357E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.22386639E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.47622214E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.47622214E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.32446467E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.17588103E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.98362269E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.73527404E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.63786691E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.03313909E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.59500074E-15    2     1000039        35   # BR(~chi_40 -> ~G        H)
     9.15095448E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07449968E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16757521E-01    2           5        -5   # BR(h -> b       bb     )
     6.39227583E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26263022E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79471885E-04    2           3        -3   # BR(h -> s       sb     )
     2.06858508E-02    2           4        -4   # BR(h -> c       cb     )
     6.71656935E-02    2          21        21   # BR(h -> g       g      )
     2.30560207E-03    2          22        22   # BR(h -> gam     gam    )
     1.54179540E-03    2          22        23   # BR(h -> Z       gam    )
     2.01227558E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56874858E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78123657E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48657028E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430938E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71225113E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547623E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666619E-05    2           4        -4   # BR(H -> c       cb     )
     9.96053324E-01    2           6        -6   # BR(H -> t       tb     )
     7.97575336E-04    2          21        21   # BR(H -> g       g      )
     2.73324644E-06    2          22        22   # BR(H -> gam     gam    )
     1.16036673E-06    2          23        22   # BR(H -> Z       gam    )
     3.33299884E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66195511E-04    2          23        23   # BR(H -> Z       Z      )
     9.00657219E-04    2          25        25   # BR(H -> h       h      )
     8.61375349E-24    2          36        36   # BR(H -> A       A      )
     3.48726191E-11    2          23        36   # BR(H -> Z       A      )
     5.83612441E-12    2          24       -37   # BR(H -> W+      H-     )
     5.83612441E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82387652E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48931107E-03    2           5        -5   # BR(A -> b       bb     )
     2.43894287E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62254411E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675796E-06    2           3        -3   # BR(A -> s       sb     )
     9.96162461E-06    2           4        -4   # BR(A -> c       cb     )
     9.96981958E-01    2           6        -6   # BR(A -> t       tb     )
     9.43662364E-04    2          21        21   # BR(A -> g       g      )
     3.04657606E-06    2          22        22   # BR(A -> gam     gam    )
     1.35302008E-06    2          23        22   # BR(A -> Z       gam    )
     3.24813625E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74514813E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39832719E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49235272E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81136720E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52494095E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45681512E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729106E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403573E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32969320E-04    2          24        25   # BR(H+ -> W+      h      )
     5.26310297E-13    2          24        36   # BR(H+ -> W+      A      )
