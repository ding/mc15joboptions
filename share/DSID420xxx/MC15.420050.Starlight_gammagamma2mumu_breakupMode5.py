evgenConfig.description = "Starlight gamma + gamma UPC collisions at 5020 GeV to continuum -> 2 mu, breakup mode 5 (no selection)"
evgenConfig.keywords = ["2photon","2lepton"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["peter.steinberg@bnl.gov"]

# TODO: Sort out proper param setting based on runArgs.ecmEnergy
if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
    sys.exit(1)

#include("MC12JobOptions/Starlight_Common.py")
#-- Starlight_Common.py code
from Starlight_i.Starlight_iConf import Starlight_i
genSeq += Starlight_i("Starlight")
genSeq.Starlight.McEventKey = "GEN_EVENT"

evgenConfig.generators += ["Starlight"]

# Extra stream persistency
# 2101 == EventInfo, 133273 == MCTruth (HepMC)
from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream
_evgenstream = AthenaPoolOutputStream("StreamEVGEN")
_evgenstream.ItemList = ["2101#*","133273#GEN_EVENT"]
del _evgenstream
#-- end Starlight_Common.py code

genSeq.Starlight.Initialize = \
    ["beam1Z 82", "beam1A 208", #Z,A of projectile
     "beam2Z 82", "beam2A 208", #Z,A of target
     # TODO: Calculate this from runArgs.ecmEnergy
     "beamLorentzGamma 2705",   #Gamma of the colliding ions, for sqrt(nn)=2.76 TeV
     "maxW 100", #Max value of w
     "minW 5", #Min value of w
     "nmbWBins 400", #Bins n w
     "maxRapidity 9.", #max y
     "nmbRapidityBins 1000", #Bins n y
     "accCutPt 1", #Cut in pT? 0 = (no, 1 = yes)
     "minPt 2.5", #Minimum pT in GeV
     "maxPt 100.0", #Maximum pT in GeV
     "accCutEta 1", #Cut in pseudorapidity? (0 = no, 1 = yes)
     "minEta -2.7", #Minimum pseudorapidity
     "maxEta 2.7", #Maximum pseudorapidity
     "productionMode 1", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
     "nmbEventsTot 1", #Number of events
     "prodParticleId 13", #Channel of interest
     "outputFormat 2", #Form of the output
     "beamBreakupMode 5", #Controls the nuclear breakup
     "interferenceEnabled 0", #Interference (0 = off, 1 = on)
     "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
     "coherentProduction 1", #Coherent=1,Incoherent=0
     "incoherentFactor 1.", #percentage of incoherence
     "bford 9.5",
     "maxPtInterference 0.24", #Maximum pt considered, when interference is turned on
     "nmbPtBinsInterference 120" #Number of pt bins when interference is turned on
    ]
