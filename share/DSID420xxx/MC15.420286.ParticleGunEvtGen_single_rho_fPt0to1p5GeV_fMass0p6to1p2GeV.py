evgenConfig.description = "Single Rho with flat in pT [0-1.5] GeV, rapidity [-2.5,2.5] and mass [0.6,1.2]"
evgenConfig.keywords = ["singleParticle", "rho"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-113, 113)

# flat pT   
genSeq.ParticleGun.sampler.mom = PG.PtRapMPhiSampler(pt=[0, 1500], rap=[-3.0, 3.0], mass=[600,1200])

# Use EvtGen to decay the rho 
evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"

include("MC15JobOptions/ChargedTrackFilter.py")
filtSeq.ChargedTracksFilter.NTracks = 1 #Ntracks > 1, i.e. 2 or more tracks
