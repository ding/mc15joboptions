evgenConfig.description = "Single Rho with flat E in [0-1] GeV"
evgenConfig.keywords = ["singleParticle", "rho"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-113, 113)

# flat pT   
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[0, 1000], eta=[-3.0, 3.0], mass=775.49)

# Use EvtGen to decay the rho 
evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"

