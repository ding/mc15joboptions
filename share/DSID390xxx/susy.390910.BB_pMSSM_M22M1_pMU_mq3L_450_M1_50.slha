#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11565304E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04021227E+01   # W+
        25     1.26572210E+02   # h
        35     3.00026036E+03   # H
        36     2.99999967E+03   # A
        37     3.00105983E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02075052E+03   # ~d_L
   2000001     3.01535407E+03   # ~d_R
   1000002     3.01982913E+03   # ~u_L
   2000002     3.01748988E+03   # ~u_R
   1000003     3.02075052E+03   # ~s_L
   2000003     3.01535407E+03   # ~s_R
   1000004     3.01982913E+03   # ~c_L
   2000004     3.01748988E+03   # ~c_R
   1000005     5.09370991E+02   # ~b_1
   2000005     3.01490670E+03   # ~b_2
   1000006     5.00919376E+02   # ~t_1
   2000006     2.99375020E+03   # ~t_2
   1000011     3.00689817E+03   # ~e_L
   2000011     3.00100971E+03   # ~e_R
   1000012     3.00549958E+03   # ~nu_eL
   1000013     3.00689817E+03   # ~mu_L
   2000013     3.00100971E+03   # ~mu_R
   1000014     3.00549958E+03   # ~nu_muL
   1000015     2.98609628E+03   # ~tau_1
   2000015     3.02222071E+03   # ~tau_2
   1000016     3.00565919E+03   # ~nu_tauL
   1000021     2.32502964E+03   # ~g
   1000022     5.01884188E+01   # ~chi_10
   1000023     1.08263778E+02   # ~chi_20
   1000025    -3.00330200E+03   # ~chi_30
   1000035     3.00368225E+03   # ~chi_40
   1000024     1.08414779E+02   # ~chi_1+
   1000037     3.00438231E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886292E-01   # N_11
  1  2    -2.70085988E-03   # N_12
  1  3     1.48033220E-02   # N_13
  1  4    -9.85200622E-04   # N_14
  2  1     3.08947112E-03   # N_21
  2  2     9.99650975E-01   # N_22
  2  3    -2.61470153E-02   # N_23
  2  4     2.17206178E-03   # N_24
  3  1    -9.72182650E-03   # N_31
  3  2     1.69812070E-02   # N_32
  3  3     7.06817381E-01   # N_33
  3  4     7.07125388E-01   # N_34
  4  1    -1.11060733E-02   # N_41
  4  2     2.00567459E-02   # N_42
  4  3     7.06757657E-01   # N_43
  4  4    -7.07084151E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99314173E-01   # U_11
  1  2    -3.70295083E-02   # U_12
  2  1     3.70295083E-02   # U_21
  2  2     9.99314173E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995265E-01   # V_11
  1  2    -3.07733340E-03   # V_12
  2  1     3.07733340E-03   # V_21
  2  2     9.99995265E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98646317E-01   # cos(theta_t)
  1  2    -5.20147435E-02   # sin(theta_t)
  2  1     5.20147435E-02   # -sin(theta_t)
  2  2     9.98646317E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99924275E-01   # cos(theta_b)
  1  2     1.23062694E-02   # sin(theta_b)
  2  1    -1.23062694E-02   # -sin(theta_b)
  2  2     9.99924275E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06860754E-01   # cos(theta_tau)
  1  2     7.07352723E-01   # sin(theta_tau)
  2  1    -7.07352723E-01   # -sin(theta_tau)
  2  2     7.06860754E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01760732E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.15653040E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44748930E+02   # higgs               
         4     6.23160228E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.15653040E+03  # The gauge couplings
     1     3.61262804E-01   # gprime(Q) DRbar
     2     6.41362121E-01   # g(Q) DRbar
     3     1.03219324E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.15653040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.21976850E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.15653040E+03  # The trilinear couplings
  1  1     2.93269886E-07   # A_d(Q) DRbar
  2  2     2.93331618E-07   # A_s(Q) DRbar
  3  3     7.00203012E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.15653040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.43167454E-08   # A_mu(Q) DRbar
  3  3     6.50778081E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.15653040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.59473898E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.15653040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.08770846E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.15653040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04579103E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.15653040E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.92654848E+04   # M^2_Hd              
        22    -9.17712174E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42776419E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.40612095E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47839760E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47839760E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52160240E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52160240E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.49520045E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.04931551E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.74640357E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.14866488E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20690870E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.39958726E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.74895866E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.50693432E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.77152493E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.78671979E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.64129412E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.25224654E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.19608184E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.51287981E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.85849596E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.99021606E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.24874805E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.88744709E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.43293181E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.80484718E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.74707489E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -5.10935227E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30627003E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.85645154E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16742663E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.02557998E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12535420E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.64142345E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64595877E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.42584663E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.29074179E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.29034630E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.92156512E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.00728057E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.21056493E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57113882E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.49769028E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.58378685E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.96026749E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42884617E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.13037461E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.97327982E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64150430E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.27704994E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.23190476E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.28458823E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.35069143E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.01417383E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69710133E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47009419E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.26111324E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.28258982E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.02420506E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55298631E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12535420E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.64142345E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64595877E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.42584663E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.29074179E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.29034630E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.92156512E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.00728057E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.21056493E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57113882E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.49769028E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.58378685E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.96026749E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42884617E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.13037461E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.97327982E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64150430E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.27704994E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.23190476E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.28458823E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.35069143E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.01417383E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69710133E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47009419E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.26111324E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.28258982E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.02420506E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55298631E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.06781481E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.49703616E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02513739E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.28787579E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.63301048E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02515896E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.32071729E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55717147E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990473E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.52740678E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.06781481E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.49703616E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02513739E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.28787579E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.63301048E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02515896E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.32071729E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55717147E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990473E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.52740678E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.84174836E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.41908297E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19970425E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.38121278E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78143161E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.49888804E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17343742E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.80807871E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.99247987E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32744861E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     7.79214298E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.06819751E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.67554896E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00245854E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.25786772E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.05898686E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02998656E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.16360289E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.06819751E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.67554896E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00245854E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.25786772E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.05898686E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02998656E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.16360289E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.06875267E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.67474313E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00220913E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.74738109E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.61902528E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.03031655E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.71630845E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35855241E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.45083639E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35855241E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.45083639E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09610253E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.48362870E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09610253E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.48362870E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09068992E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.04491306E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.54524665E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.96979854E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.33793276E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.95077057E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.67266874E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.04420868E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.54274606E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.81075298E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.86938137E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.92300283E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     9.11537237E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.15506951E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     9.11537237E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.15506951E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.50308822E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     9.73871116E-04    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     9.73871116E-04    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.02461384E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.54529833E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.54529833E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.54624747E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     9.08923232E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.20191584E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.85458634E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.48915899E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.48915899E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.38361419E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.80117297E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.69489867E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.69489867E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.16376156E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.16376156E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.88805106E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.88805106E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     1.88805106E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.88805106E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.44539306E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.44539306E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.98112163E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.31977359E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.79725337E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.56849152E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.56849152E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.24091986E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.00278248E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.67536824E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.67536824E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.19126401E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.19126401E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.38871515E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.38871515E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.38871515E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.38871515E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.54833143E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.54833143E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     5.16303426E-03   # h decays
#          BR         NDA      ID1       ID2
     6.56394143E-01    2           5        -5   # BR(h -> b       bb     )
     5.11844120E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.81188927E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.83609485E-04    2           3        -3   # BR(h -> s       sb     )
     1.65626980E-02    2           4        -4   # BR(h -> c       cb     )
     5.52884072E-02    2          21        21   # BR(h -> g       g      )
     1.92518534E-03    2          22        22   # BR(h -> gam     gam    )
     1.38952610E-03    2          22        23   # BR(h -> Z       gam    )
     1.92049188E-01    2          24       -24   # BR(h -> W+      W-     )
     2.46011213E-02    2          23        23   # BR(h -> Z       Z      )
     4.05201093E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.40116774E+01   # H decays
#          BR         NDA      ID1       ID2
     7.29396659E-01    2           5        -5   # BR(H -> b       bb     )
     1.77485680E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.27546951E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.70623430E-04    2           3        -3   # BR(H -> s       sb     )
     2.17402811E-07    2           4        -4   # BR(H -> c       cb     )
     2.16873510E-02    2           6        -6   # BR(H -> t       tb     )
     3.94990914E-05    2          21        21   # BR(H -> g       g      )
     4.16339255E-08    2          22        22   # BR(H -> gam     gam    )
     8.36114916E-09    2          23        22   # BR(H -> Z       gam    )
     2.98422307E-05    2          24       -24   # BR(H -> W+      W-     )
     1.49027216E-05    2          23        23   # BR(H -> Z       Z      )
     8.00867603E-05    2          25        25   # BR(H -> h       h      )
    -7.81287771E-23    2          36        36   # BR(H -> A       A      )
     9.29512128E-17    2          23        36   # BR(H -> Z       A      )
     2.44517103E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13863658E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.21430997E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.44437683E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.53435962E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.16255150E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.30967615E+01   # A decays
#          BR         NDA      ID1       ID2
     7.80397710E-01    2           5        -5   # BR(A -> b       bb     )
     1.89872416E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.71342550E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.24548069E-04    2           3        -3   # BR(A -> s       sb     )
     2.32675777E-07    2           4        -4   # BR(A -> c       cb     )
     2.31981275E-02    2           6        -6   # BR(A -> t       tb     )
     6.83162732E-05    2          21        21   # BR(A -> g       g      )
     4.33387627E-08    2          22        22   # BR(A -> gam     gam    )
     6.73093863E-08    2          23        22   # BR(A -> Z       gam    )
     3.17968136E-05    2          23        25   # BR(A -> Z       h      )
     2.67368711E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.23584154E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32778128E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.10346819E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.80965843E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.06531487E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.53586048E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.96618413E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.81800428E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.26920426E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.08404475E-03    2           4        -3   # BR(H+ -> c       sb     )
     6.95263833E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.25231903E-05    2          24        25   # BR(H+ -> W+      h      )
     1.79629741E-13    2          24        36   # BR(H+ -> W+      A      )
     2.13073713E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.03487217E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.58713502E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
