#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.93805167E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     2.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04030603E+01   # W+
        25     1.27395299E+02   # h
        35     3.00026127E+03   # H
        36     3.00000092E+03   # A
        37     3.00115235E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00857812E+03   # ~d_L
   2000001     3.00307853E+03   # ~d_R
   1000002     3.00765584E+03   # ~u_L
   2000002     3.00578356E+03   # ~u_R
   1000003     3.00857812E+03   # ~s_L
   2000003     3.00307853E+03   # ~s_R
   1000004     3.00765584E+03   # ~c_L
   2000004     3.00578356E+03   # ~c_R
   1000005     2.83603532E+02   # ~b_1
   2000005     3.00275766E+03   # ~b_2
   1000006     2.66175002E+02   # ~t_1
   2000006     2.99107367E+03   # ~t_2
   1000011     3.00716347E+03   # ~e_L
   2000011     3.00043562E+03   # ~e_R
   1000012     3.00576879E+03   # ~nu_eL
   1000013     3.00716347E+03   # ~mu_L
   2000013     3.00043562E+03   # ~mu_R
   1000014     3.00576879E+03   # ~nu_muL
   1000015     2.98620796E+03   # ~tau_1
   2000015     3.02239280E+03   # ~tau_2
   1000016     3.00612383E+03   # ~nu_tauL
   1000021     2.30902233E+03   # ~g
   1000022     5.03821524E+01   # ~chi_10
   1000023     1.08098233E+02   # ~chi_20
   1000025    -3.00752750E+03   # ~chi_30
   1000035     3.00790080E+03   # ~chi_40
   1000024     1.08247407E+02   # ~chi_1+
   1000037     3.00858925E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886283E-01   # N_11
  1  2    -2.70451534E-03   # N_12
  1  3     1.48032177E-02   # N_13
  1  4    -9.85193688E-04   # N_14
  2  1     3.09364960E-03   # N_21
  2  2     9.99650025E-01   # N_22
  2  3    -2.61825626E-02   # N_23
  2  4     2.17500829E-03   # N_24
  3  1    -9.72162439E-03   # N_31
  3  2     1.70042976E-02   # N_32
  3  3     7.06816789E-01   # N_33
  3  4     7.07125428E-01   # N_34
  4  1    -1.11058369E-02   # N_41
  4  2     2.00840080E-02   # N_42
  4  3     7.06756935E-01   # N_43
  4  4    -7.07084103E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99312307E-01   # U_11
  1  2    -3.70798150E-02   # U_12
  2  1     3.70798150E-02   # U_21
  2  2     9.99312307E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995252E-01   # V_11
  1  2    -3.08150842E-03   # V_12
  2  1     3.08150842E-03   # V_21
  2  2     9.99995252E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98661645E-01   # cos(theta_t)
  1  2    -5.17196172E-02   # sin(theta_t)
  2  1     5.17196172E-02   # -sin(theta_t)
  2  2     9.98661645E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99926259E-01   # cos(theta_b)
  1  2     1.21439928E-02   # sin(theta_b)
  2  1    -1.21439928E-02   # -sin(theta_b)
  2  2     9.99926259E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06846932E-01   # cos(theta_tau)
  1  2     7.07366535E-01   # sin(theta_tau)
  2  1    -7.07366535E-01   # -sin(theta_tau)
  2  2     7.06846932E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01644307E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.38051670E+02  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.45215470E+02   # higgs               
         4     5.19948281E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.38051670E+02  # The gauge couplings
     1     3.60568229E-01   # gprime(Q) DRbar
     2     6.41012767E-01   # g(Q) DRbar
     3     1.03683220E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.38051670E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.41294750E-07   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.38051670E+02  # The trilinear couplings
  1  1     1.76117815E-07   # A_d(Q) DRbar
  2  2     1.76155891E-07   # A_s(Q) DRbar
  3  3     4.24580309E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.38051670E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     3.86118760E-08   # A_mu(Q) DRbar
  3  3     3.90599064E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.38051670E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.63835092E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.38051670E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.08252932E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.38051670E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04412859E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.38051670E+02  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.71989393E+04   # M^2_Hd              
        22    -9.26378665E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     2.99899995E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42616754E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.80878264E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47827332E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47827332E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52172668E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52172668E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.65646190E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     5.72719282E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.94272807E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.25148596E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.18150954E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.65638437E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.31951990E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.75485194E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.79139998E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.65324993E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.27437129E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.13466556E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.77988779E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.09685529E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.52515593E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.28158228E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.82593460E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.44342293E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -2.76199460E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30944258E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.84013799E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.18401257E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.05544735E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.13402703E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.58769234E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63491227E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.54838568E-13    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.26827022E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.04094058E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.23733254E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54859245E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.48019291E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.45139274E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.13906476E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.91732073E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63048808E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.84683261E-11    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.25742614E-11    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26253925E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.04779946E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.72326585E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.40192328E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.20748249E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.55980346E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.13402703E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.58769234E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63491227E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.54838568E-13    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.26827022E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.04094058E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.23733254E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54859245E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.48019291E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.45139274E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.13906476E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.91732073E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63048808E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.84683261E-11    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.25742614E-11    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26253925E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.04779946E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.72326585E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.40192328E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.20748249E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.55980346E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.06268730E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.47303875E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02594284E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.02675329E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55088572E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990447E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.55330558E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.06268730E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.47303875E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02594284E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.02675329E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55088572E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990447E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.55330558E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.83608991E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.41293755E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.20176097E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.38530148E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.77585431E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.49251435E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17560098E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.08026385E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     4.99737047E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.33174715E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     4.67499649E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.06308467E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.65158392E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00325139E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.03159022E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.06308467E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.65158392E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00325139E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.03159022E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.06390540E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.65077581E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00300137E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.03192105E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.66085924E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35866735E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.22900603E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35866735E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.22900603E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09605479E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.40967057E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09605479E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.40967057E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09055553E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.28124236E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.88643244E-12    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.62687931E-14    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.88643244E-12    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.62687931E-14    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.51226842E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.34845960E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.38870994E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.41859158E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.41859158E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.46911487E-08    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.63568173E-10    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.63568173E-10    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.15523296E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.53652678E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.99816277E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.41337135E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.63896862E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.74298905E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.52580165E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     9.17364197E-04    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.16184091E-03    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     9.17364197E-04    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.16184091E-03    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.94547049E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     9.70939103E-05    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     9.70939103E-05    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.02153614E-04    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.51992547E-05    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.51992547E-05    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.52203171E-05    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     9.32695454E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.17414860E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.77593902E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.36083725E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.36083725E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.12150295E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.73122599E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.16038468E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     2.16038468E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     3.27962703E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     3.27962703E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     2.16038468E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     2.16038468E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     3.27962703E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     3.27962703E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.71958907E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.71958907E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.14642648E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.14642648E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.13853394E-13    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     5.13853394E-13    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     1.74900610E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.74900610E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     5.13853394E-13    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     5.13853394E-13    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     1.74900610E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.74900610E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.13580857E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.13580857E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     4.54944687E-11    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     4.54944687E-11    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     4.54944687E-11    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     4.54944687E-11    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.89835414E-11    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.89835414E-11    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     9.21765118E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.10277466E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.73966746E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.43741002E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.43741002E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.20618198E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.90029730E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.99679311E-12    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.99679311E-12    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.14613809E-11    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.14613809E-11    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.05599286E-11    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.05599286E-11    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.99679311E-12    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.99679311E-12    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.14613809E-11    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.14613809E-11    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.05599286E-11    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.05599286E-11    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.70108666E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.70108666E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.17335433E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.17335433E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.80967309E-07    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.80967309E-07    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     3.06818078E-12    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     3.06818078E-12    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     2.55850279E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     2.55850279E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.06818078E-12    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     3.06818078E-12    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     2.55850279E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     2.55850279E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.38723084E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.38723084E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     9.28507792E-11    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     9.28507792E-11    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     9.28507792E-11    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     9.28507792E-11    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     6.45087384E-11    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     6.45087384E-11    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     5.35347753E-03   # h decays
#          BR         NDA      ID1       ID2
     6.47812777E-01    2           5        -5   # BR(h -> b       bb     )
     4.96623201E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.75798167E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.71730336E-04    2           3        -3   # BR(h -> s       sb     )
     1.60571268E-02    2           4        -4   # BR(h -> c       cb     )
     5.41884710E-02    2          21        21   # BR(h -> g       g      )
     1.91064915E-03    2          22        22   # BR(h -> gam     gam    )
     1.43769567E-03    2          22        23   # BR(h -> Z       gam    )
     2.02144948E-01    2          24       -24   # BR(h -> W+      W-     )
     2.61986364E-02    2          23        23   # BR(h -> Z       Z      )
     3.98471854E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.39509448E+01   # H decays
#          BR         NDA      ID1       ID2
     7.26141964E-01    2           5        -5   # BR(H -> b       bb     )
     1.78258590E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.30279777E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.73979303E-04    2           3        -3   # BR(H -> s       sb     )
     2.18248056E-07    2           4        -4   # BR(H -> c       cb     )
     2.17716699E-02    2           6        -6   # BR(H -> t       tb     )
     3.98032178E-05    2          21        21   # BR(H -> g       g      )
     5.79636261E-08    2          22        22   # BR(H -> gam     gam    )
     8.42239787E-09    2          23        22   # BR(H -> Z       gam    )
     2.68517790E-05    2          24       -24   # BR(H -> W+      W-     )
     1.34093403E-05    2          23        23   # BR(H -> Z       Z      )
     7.65197271E-05    2          25        25   # BR(H -> h       h      )
     3.53423579E-23    2          36        36   # BR(H -> A       A      )
     9.26376964E-17    2          23        36   # BR(H -> Z       A      )
     2.46261856E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.14348885E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.22296909E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.48649215E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.77117136E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.34909284E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.30071829E+01   # A decays
#          BR         NDA      ID1       ID2
     7.78876458E-01    2           5        -5   # BR(A -> b       bb     )
     1.91180120E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.75966272E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.30226914E-04    2           3        -3   # BR(A -> s       sb     )
     2.34278267E-07    2           4        -4   # BR(A -> c       cb     )
     2.33578982E-02    2           6        -6   # BR(A -> t       tb     )
     6.87867474E-05    2          21        21   # BR(A -> g       g      )
     4.36154019E-08    2          22        22   # BR(A -> gam     gam    )
     6.77838864E-08    2          23        22   # BR(A -> Z       gam    )
     2.86806729E-05    2          23        25   # BR(A -> Z       h      )
     2.69950593E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.24424636E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.34059733E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.16990123E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.57325817E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.04972352E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.59856066E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     9.18787667E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.71821966E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.39946590E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.11084376E-03    2           4        -3   # BR(H+ -> c       sb     )
     6.86285364E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.90377574E-05    2          24        25   # BR(H+ -> W+      h      )
     2.78154127E-13    2          24        36   # BR(H+ -> W+      A      )
     2.18621251E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.09353322E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.84932310E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
