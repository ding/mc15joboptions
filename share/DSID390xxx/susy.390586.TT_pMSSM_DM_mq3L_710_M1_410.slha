#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12000854E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.36900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04184090E+01   # W+
        25     1.25652751E+02   # h
        35     4.00001046E+03   # H
        36     3.99999708E+03   # A
        37     4.00100928E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02300657E+03   # ~d_L
   2000001     4.02022498E+03   # ~d_R
   1000002     4.02235301E+03   # ~u_L
   2000002     4.02123448E+03   # ~u_R
   1000003     4.02300657E+03   # ~s_L
   2000003     4.02022498E+03   # ~s_R
   1000004     4.02235301E+03   # ~c_L
   2000004     4.02123448E+03   # ~c_R
   1000005     7.74575089E+02   # ~b_1
   2000005     4.02385326E+03   # ~b_2
   1000006     7.58827075E+02   # ~t_1
   2000006     2.07110275E+03   # ~t_2
   1000011     4.00405223E+03   # ~e_L
   2000011     4.00312016E+03   # ~e_R
   1000012     4.00293835E+03   # ~nu_eL
   1000013     4.00405223E+03   # ~mu_L
   2000013     4.00312016E+03   # ~mu_R
   1000014     4.00293835E+03   # ~nu_muL
   1000015     4.00414339E+03   # ~tau_1
   2000015     4.00806726E+03   # ~tau_2
   1000016     4.00462046E+03   # ~nu_tauL
   1000021     1.97469545E+03   # ~g
   1000022     3.98343930E+02   # ~chi_10
   1000023    -4.48078264E+02   # ~chi_20
   1000025     4.62903318E+02   # ~chi_30
   1000035     2.05339024E+03   # ~chi_40
   1000024     4.45896033E+02   # ~chi_1+
   1000037     2.05355465E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.28838602E-01   # N_11
  1  2    -1.68405948E-02   # N_12
  1  3    -4.18674974E-01   # N_13
  1  4    -3.70748207E-01   # N_14
  2  1    -3.85464242E-02   # N_21
  2  2     2.36022252E-02   # N_22
  2  3    -7.04470865E-01   # N_23
  2  4     7.08292248E-01   # N_24
  3  1     5.58157267E-01   # N_31
  3  2     2.86071249E-02   # N_32
  3  3     5.73049316E-01   # N_33
  3  4     5.99380162E-01   # N_34
  4  1    -1.10027133E-03   # N_41
  4  2     9.99170137E-01   # N_42
  4  3    -6.82261075E-03   # N_43
  4  4    -4.01407479E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.65463904E-03   # U_11
  1  2     9.99953393E-01   # U_12
  2  1    -9.99953393E-01   # U_21
  2  2     9.65463904E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.67774982E-02   # V_11
  1  2    -9.98386857E-01   # V_12
  2  1    -9.98386857E-01   # V_21
  2  2    -5.67774982E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94800290E-01   # cos(theta_t)
  1  2    -1.01844897E-01   # sin(theta_t)
  2  1     1.01844897E-01   # -sin(theta_t)
  2  2     9.94800290E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999031E-01   # cos(theta_b)
  1  2    -1.39212035E-03   # sin(theta_b)
  2  1     1.39212035E-03   # -sin(theta_b)
  2  2     9.99999031E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06078500E-01   # cos(theta_tau)
  1  2     7.08133569E-01   # sin(theta_tau)
  2  1    -7.08133569E-01   # -sin(theta_tau)
  2  2    -7.06078500E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00252288E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20008537E+03  # DRbar Higgs Parameters
         1    -4.36900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43889008E+02   # higgs               
         4     1.62128418E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20008537E+03  # The gauge couplings
     1     3.61611672E-01   # gprime(Q) DRbar
     2     6.35838945E-01   # g(Q) DRbar
     3     1.03202446E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20008537E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14457786E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20008537E+03  # The trilinear couplings
  1  1     4.22147242E-07   # A_d(Q) DRbar
  2  2     4.22186672E-07   # A_s(Q) DRbar
  3  3     7.54521078E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20008537E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.35270165E-08   # A_mu(Q) DRbar
  3  3     9.44763589E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20008537E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68952446E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20008537E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86662827E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20008537E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02998181E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20008537E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56958575E+07   # M^2_Hd              
        22    -1.84359202E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40256763E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.03789476E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43870501E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43870501E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56129499E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56129499E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.25298450E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.72762228E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.04084213E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.99154273E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.23999286E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18804453E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.78630053E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.14794465E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.88215232E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.27322842E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.81592908E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.62511932E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.13425102E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.35261112E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.70025739E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.64196191E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.80035430E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.70953357E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.68481502E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66912887E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54354173E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80834038E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60280137E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.59469259E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62905776E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.16492033E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13175128E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.10826980E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.87439812E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.82526227E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.90645518E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78617304E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.68484421E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.26168802E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.53649267E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78702414E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.49306263E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56196395E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52716859E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60985728E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.75111500E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.07184210E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.68967542E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.70238044E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45511340E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78637580E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.59115554E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.13842300E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.06896184E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79235328E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.58840067E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.59465791E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52934568E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54319436E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.78029470E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.10456867E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.40548135E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.64995088E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85793169E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78617304E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.68484421E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.26168802E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.53649267E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78702414E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.49306263E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56196395E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52716859E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60985728E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.75111500E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.07184210E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.68967542E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.70238044E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45511340E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78637580E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.59115554E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.13842300E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.06896184E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79235328E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.58840067E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.59465791E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52934568E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54319436E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.78029470E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.10456867E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.40548135E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.64995088E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85793169E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13518516E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04021313E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.41396866E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.98682059E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78169429E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.33886338E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57846249E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03727084E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.88479889E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.48119392E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.10038245E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.72037972E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13518516E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04021313E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.41396866E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.98682059E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78169429E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.33886338E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57846249E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03727084E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.88479889E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.48119392E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.10038245E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.72037972E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07304580E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.47595494E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47835818E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.29563952E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40618419E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.54384347E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82000119E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06561701E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.52666599E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.97943306E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.06107357E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43884461E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.90035713E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88543681E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13624717E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.19933230E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.03727822E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.16812741E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78584449E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.22775419E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55536014E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13624717E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.19933230E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.03727822E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.16812741E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78584449E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.22775419E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55536014E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45934888E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.08779036E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.40812103E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.78050009E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52822507E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.54869315E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04165712E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.74331799E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33583565E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33583565E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11196047E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11196047E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10440776E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.80954915E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.72704894E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.59104605E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.81681987E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.64497248E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.64853086E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.99002072E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.71870621E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.59299884E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.91408167E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18103998E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53192482E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18103998E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53192482E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41012459E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51246298E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51246298E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47942762E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02195205E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02195205E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02195188E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.01508143E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.01508143E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.01508143E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.01508143E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.71694349E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.71694349E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.71694349E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.71694349E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.66234715E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.66234715E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.00024326E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.09316495E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.78205156E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.77513027E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.86459946E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.77513027E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.86459946E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.64759624E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.09008736E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.09008736E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.09144657E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.24195198E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.24195198E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.24194340E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.11189880E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.44228067E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.11189880E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.44228067E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.82028205E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.30713428E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.30713428E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.04574308E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.61069190E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.61069190E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.61069197E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     7.51395368E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     7.51395368E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     7.51395368E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     7.51395368E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.50462467E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.50462467E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.50462467E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.50462467E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.37393263E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.37393263E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.80820785E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.16424923E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.53972644E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.66126577E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.64598970E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.64598970E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.05404352E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.24636611E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.40091446E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82563474E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.82563474E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.83382923E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.83382923E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02433846E-03   # h decays
#          BR         NDA      ID1       ID2
     5.89443786E-01    2           5        -5   # BR(h -> b       bb     )
     6.47979596E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29383823E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86373754E-04    2           3        -3   # BR(h -> s       sb     )
     2.11251026E-02    2           4        -4   # BR(h -> c       cb     )
     6.90068632E-02    2          21        21   # BR(h -> g       g      )
     2.39436946E-03    2          22        22   # BR(h -> gam     gam    )
     1.64642598E-03    2          22        23   # BR(h -> Z       gam    )
     2.22665068E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82046676E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.54891900E+01   # H decays
#          BR         NDA      ID1       ID2
     3.63931706E-01    2           5        -5   # BR(H -> b       bb     )
     5.97522433E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11269460E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49895317E-04    2           3        -3   # BR(H -> s       sb     )
     7.00865168E-08    2           4        -4   # BR(H -> c       cb     )
     7.02412826E-03    2           6        -6   # BR(H -> t       tb     )
     7.19535649E-07    2          21        21   # BR(H -> g       g      )
     2.56567950E-09    2          22        22   # BR(H -> gam     gam    )
     1.80769440E-09    2          23        22   # BR(H -> Z       gam    )
     1.68652825E-06    2          24       -24   # BR(H -> W+      W-     )
     8.42679964E-07    2          23        23   # BR(H -> Z       Z      )
     6.78979081E-06    2          25        25   # BR(H -> h       h      )
    -2.09683869E-25    2          36        36   # BR(H -> A       A      )
     9.97743280E-21    2          23        36   # BR(H -> Z       A      )
     1.85385872E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.55362170E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.55362170E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.32280503E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.94230142E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.48737819E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.47956781E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.22068931E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.57063965E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.27497935E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.19651262E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.81953137E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     7.19971299E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     8.14663734E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     8.14663734E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.40701872E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.54869441E+01   # A decays
#          BR         NDA      ID1       ID2
     3.63971954E-01    2           5        -5   # BR(A -> b       bb     )
     5.97549087E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11278718E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49947056E-04    2           3        -3   # BR(A -> s       sb     )
     7.05465582E-08    2           4        -4   # BR(A -> c       cb     )
     7.03828515E-03    2           6        -6   # BR(A -> t       tb     )
     1.44618938E-05    2          21        21   # BR(A -> g       g      )
     6.47290713E-08    2          22        22   # BR(A -> gam     gam    )
     1.59008632E-08    2          23        22   # BR(A -> Z       gam    )
     1.68304017E-06    2          23        25   # BR(A -> Z       h      )
     1.88573475E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.55350203E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.55350203E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.02579403E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.36470514E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.27456187E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.96496364E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.99267929E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.87989978E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.40879199E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.27248900E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.42888334E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     8.49410198E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     8.49410198E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.57037058E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.86168862E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.95374447E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10509818E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.75147755E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19257819E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45351685E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.72988232E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.67835091E-06    2          24        25   # BR(H+ -> W+      h      )
     2.50997309E-14    2          24        36   # BR(H+ -> W+      A      )
     5.42326581E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.75942331E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.93718890E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.54964819E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.44008074E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.54103543E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.02313867E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.70583891E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.66091752E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
