#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12088694E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.48900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.23485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04168488E+01   # W+
        25     1.25344000E+02   # h
        35     4.00000927E+03   # H
        36     3.99999696E+03   # A
        37     4.00101502E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02324648E+03   # ~d_L
   2000001     4.02038248E+03   # ~d_R
   1000002     4.02259007E+03   # ~u_L
   2000002     4.02156625E+03   # ~u_R
   1000003     4.02324648E+03   # ~s_L
   2000003     4.02038248E+03   # ~s_R
   1000004     4.02259007E+03   # ~c_L
   2000004     4.02156625E+03   # ~c_R
   1000005     7.31836114E+02   # ~b_1
   2000005     4.02391394E+03   # ~b_2
   1000006     7.20623656E+02   # ~t_1
   2000006     2.25057824E+03   # ~t_2
   1000011     4.00420311E+03   # ~e_L
   2000011     4.00300121E+03   # ~e_R
   1000012     4.00308588E+03   # ~nu_eL
   1000013     4.00420311E+03   # ~mu_L
   2000013     4.00300121E+03   # ~mu_R
   1000014     4.00308588E+03   # ~nu_muL
   1000015     4.00454345E+03   # ~tau_1
   2000015     4.00765901E+03   # ~tau_2
   1000016     4.00475369E+03   # ~nu_tauL
   1000021     1.97681086E+03   # ~g
   1000022     3.00477016E+02   # ~chi_10
   1000023    -3.59716669E+02   # ~chi_20
   1000025     3.71022100E+02   # ~chi_30
   1000035     2.05354422E+03   # ~chi_40
   1000024     3.57220230E+02   # ~chi_1+
   1000037     2.05370871E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.70044196E-01   # N_11
  1  2    -1.34472291E-02   # N_12
  1  3    -3.79071007E-01   # N_13
  1  4    -3.14876866E-01   # N_14
  2  1    -4.95079975E-02   # N_21
  2  2     2.45072398E-02   # N_22
  2  3    -7.03256644E-01   # N_23
  2  4     7.08786602E-01   # N_24
  3  1     4.90480387E-01   # N_31
  3  2     2.84095424E-02   # N_32
  3  3     6.01423924E-01   # N_33
  3  4     6.30008851E-01   # N_34
  4  1    -1.02214753E-03   # N_41
  4  2     9.99205417E-01   # N_42
  4  3    -4.95268922E-03   # N_43
  4  4    -3.95343010E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.00982279E-03   # U_11
  1  2     9.99975431E-01   # U_12
  2  1    -9.99975431E-01   # U_21
  2  2     7.00982279E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.59212461E-02   # V_11
  1  2    -9.98435183E-01   # V_12
  2  1    -9.98435183E-01   # V_21
  2  2    -5.59212461E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96527885E-01   # cos(theta_t)
  1  2    -8.32596806E-02   # sin(theta_t)
  2  1     8.32596806E-02   # -sin(theta_t)
  2  2     9.96527885E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999398E-01   # cos(theta_b)
  1  2    -1.09726917E-03   # sin(theta_b)
  2  1     1.09726917E-03   # -sin(theta_b)
  2  2     9.99999398E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05793861E-01   # cos(theta_tau)
  1  2     7.08417268E-01   # sin(theta_tau)
  2  1    -7.08417268E-01   # -sin(theta_tau)
  2  2    -7.05793861E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00264430E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20886944E+03  # DRbar Higgs Parameters
         1    -3.48900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43891488E+02   # higgs               
         4     1.62037208E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20886944E+03  # The gauge couplings
     1     3.61672371E-01   # gprime(Q) DRbar
     2     6.36138350E-01   # g(Q) DRbar
     3     1.03189258E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20886944E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.16253998E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20886944E+03  # The trilinear couplings
  1  1     4.27899407E-07   # A_d(Q) DRbar
  2  2     4.27939669E-07   # A_s(Q) DRbar
  3  3     7.65165569E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20886944E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.36897800E-08   # A_mu(Q) DRbar
  3  3     9.46403029E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20886944E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67819601E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20886944E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84138731E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20886944E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03263767E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20886944E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57644503E+07   # M^2_Hd              
        22    -1.37709248E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.23485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40395706E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.26355246E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45353311E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45353311E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54646689E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54646689E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.59500328E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.19869743E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.21509190E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.42819007E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.15802060E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.31238762E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.25680963E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18732276E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.90480505E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.95075992E-09    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.35370978E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.16021117E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.30076504E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.38888323E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.22741655E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.00110568E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.95578406E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.06324290E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.52639348E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.95529592E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.74550677E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66945832E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52471194E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78857612E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62341329E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.62822085E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59946975E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.70748302E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14027378E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.32628917E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.77105378E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.98119870E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.86902820E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78832709E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.97914771E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96497826E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.26392465E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78641714E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.37350706E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56071665E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52728589E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61261662E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.16190770E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.34133285E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.31522058E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.19117881E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45094552E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78853238E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.74468459E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.86601174E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.57504860E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79146226E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.44389263E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.59290454E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52947039E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54518336E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.08555187E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.49859366E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.43048311E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.32020865E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85679004E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78832709E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.97914771E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96497826E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.26392465E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78641714E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.37350706E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56071665E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52728589E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61261662E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.16190770E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.34133285E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.31522058E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.19117881E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45094552E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78853238E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.74468459E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.86601174E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.57504860E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79146226E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.44389263E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.59290454E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52947039E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54518336E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.08555187E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.49859366E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.43048311E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.32020865E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85679004E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14251369E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.17437844E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.68740869E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.75981708E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77822265E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.96073287E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57085426E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05703884E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.58069127E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.44254354E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.39487755E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.74507355E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14251369E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.17437844E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.68740869E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.75981708E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77822265E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.96073287E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57085426E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05703884E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.58069127E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.44254354E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.39487755E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.74507355E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09049357E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.72819758E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.40568835E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.07105241E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40121670E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.49254322E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80971015E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08623774E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.82259991E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.13677095E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.76877013E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42903246E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00469062E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86545134E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14357474E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.30850132E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.39831964E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.16025810E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78186798E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.15510451E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54807065E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14357474E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.30850132E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.39831964E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.16025810E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78186798E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.15510451E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54807065E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47040132E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.18577764E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26717717E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.86387339E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52243392E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.62057481E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03067184E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.94985061E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33504171E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33504171E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11169478E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11169478E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10652703E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.96251438E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.73855799E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.61977492E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.71462426E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.14179267E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.55469726E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.38774907E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.61780770E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.81127874E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.47346730E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17733249E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52703163E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17733249E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52703163E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43857929E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50074474E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50074474E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47470013E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99862947E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99862947E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99862925E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.08734956E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.08734956E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.08734956E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.08734956E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.95783865E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.95783865E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.95783865E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.95783865E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.07583910E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.07583910E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.21953757E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.10359320E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.42100596E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.88035343E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.27738079E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.88035343E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.27738079E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.22874960E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.89547723E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.89547723E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.88971446E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.86960583E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.86960583E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.86959511E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.71016135E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.21820386E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.71016135E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.21820386E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.68689402E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.08573689E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.08573689E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.38929884E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.01657381E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.01657381E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.01657382E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.56973281E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.56973281E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.56973281E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.56973281E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.23239482E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.23239482E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.23239482E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.23239482E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.82547781E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.82547781E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.96121485E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.88131883E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.19662108E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.02991054E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.54747421E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.54747421E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.72238887E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.48556804E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.44231963E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.83304896E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.83304896E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.84646412E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.84646412E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.99644737E-03   # h decays
#          BR         NDA      ID1       ID2
     5.95408809E-01    2           5        -5   # BR(h -> b       bb     )
     6.50926229E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30428287E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.88822062E-04    2           3        -3   # BR(h -> s       sb     )
     2.12304468E-02    2           4        -4   # BR(h -> c       cb     )
     6.93148368E-02    2          21        21   # BR(h -> g       g      )
     2.38300426E-03    2          22        22   # BR(h -> gam     gam    )
     1.61319678E-03    2          22        23   # BR(h -> Z       gam    )
     2.16900918E-01    2          24       -24   # BR(h -> W+      W-     )
     2.73369142E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50024519E+01   # H decays
#          BR         NDA      ID1       ID2
     3.56019124E-01    2           5        -5   # BR(H -> b       bb     )
     6.02809889E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13138976E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52106642E-04    2           3        -3   # BR(H -> s       sb     )
     7.07101508E-08    2           4        -4   # BR(H -> c       cb     )
     7.08662936E-03    2           6        -6   # BR(H -> t       tb     )
     8.33614652E-07    2          21        21   # BR(H -> g       g      )
     7.53719381E-10    2          22        22   # BR(H -> gam     gam    )
     1.82213975E-09    2          23        22   # BR(H -> Z       gam    )
     1.76383598E-06    2          24       -24   # BR(H -> W+      W-     )
     8.81306969E-07    2          23        23   # BR(H -> Z       Z      )
     6.94926423E-06    2          25        25   # BR(H -> h       h      )
     4.67494643E-24    2          36        36   # BR(H -> A       A      )
     2.68898270E-20    2          23        36   # BR(H -> Z       A      )
     1.85725846E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.59924955E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.59924955E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.12465964E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.79148500E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.27959850E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.72212937E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.30815873E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.24082800E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.07434151E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.14514406E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.65691585E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.19969174E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.95666446E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.95666446E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.40022871E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.49967016E+01   # A decays
#          BR         NDA      ID1       ID2
     3.56081898E-01    2           5        -5   # BR(A -> b       bb     )
     6.02875643E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13162058E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52175086E-04    2           3        -3   # BR(A -> s       sb     )
     7.11754109E-08    2           4        -4   # BR(A -> c       cb     )
     7.10102449E-03    2           6        -6   # BR(A -> t       tb     )
     1.45908078E-05    2          21        21   # BR(A -> g       g      )
     5.97288667E-08    2          22        22   # BR(A -> gam     gam    )
     1.60382468E-08    2          23        22   # BR(A -> Z       gam    )
     1.76032769E-06    2          23        25   # BR(A -> Z       h      )
     1.87120383E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.59936539E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.59936539E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.84082665E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.58957995E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.07448491E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.31510363E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.07015385E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.36673624E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.19124840E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.72022673E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.95176705E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.06948159E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.06948159E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.51200327E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.71940302E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.01679806E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12739239E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.66041482E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20520807E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47950057E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.64174845E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.75834542E-06    2          24        25   # BR(H+ -> W+      h      )
     2.61086517E-14    2          24        36   # BR(H+ -> W+      A      )
     6.20932293E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.83489600E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.40875967E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.59790941E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.60490955E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58363177E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.16046303E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.66753593E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     8.00014384E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
