#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871761E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03998303E+01   # W+
        25     1.24898759E+02   # h
        35     3.00007805E+03   # H
        36     2.99999995E+03   # A
        37     3.00089529E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04198867E+03   # ~d_L
   2000001     3.03688006E+03   # ~d_R
   1000002     3.04108207E+03   # ~u_L
   2000002     3.03781350E+03   # ~u_R
   1000003     3.04198867E+03   # ~s_L
   2000003     3.03688006E+03   # ~s_R
   1000004     3.04108207E+03   # ~c_L
   2000004     3.03781350E+03   # ~c_R
   1000005     1.05087305E+03   # ~b_1
   2000005     3.03580244E+03   # ~b_2
   1000006     1.04712538E+03   # ~t_1
   2000006     3.01874218E+03   # ~t_2
   1000011     3.00624819E+03   # ~e_L
   2000011     3.00223103E+03   # ~e_R
   1000012     3.00485922E+03   # ~nu_eL
   1000013     3.00624819E+03   # ~mu_L
   2000013     3.00223103E+03   # ~mu_R
   1000014     3.00485922E+03   # ~nu_muL
   1000015     2.98562688E+03   # ~tau_1
   2000015     3.02188936E+03   # ~tau_2
   1000016     3.00456666E+03   # ~nu_tauL
   1000021     2.35219767E+03   # ~g
   1000022     2.00618195E+02   # ~chi_10
   1000023     4.25209499E+02   # ~chi_20
   1000025    -2.99472240E+03   # ~chi_30
   1000035     2.99539613E+03   # ~chi_40
   1000024     4.25372366E+02   # ~chi_1+
   1000037     2.99598517E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887634E-01   # N_11
  1  2    -9.59866835E-04   # N_12
  1  3     1.48598739E-02   # N_13
  1  4    -1.72686605E-03   # N_14
  2  1     1.36233112E-03   # N_21
  2  2     9.99635267E-01   # N_22
  2  3    -2.65361932E-02   # N_23
  2  4     4.82771083E-03   # N_24
  3  1    -9.26891290E-03   # N_31
  3  2     1.53615731E-02   # N_32
  3  3     7.06852627E-01   # N_33
  3  4     7.07133278E-01   # N_34
  4  1    -1.17025882E-02   # N_41
  4  2     2.21908318E-02   # N_42
  4  3     7.06706713E-01   # N_43
  4  4    -7.07061693E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99294964E-01   # U_11
  1  2    -3.75442944E-02   # U_12
  2  1     3.75442944E-02   # U_21
  2  2     9.99294964E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99976664E-01   # V_11
  1  2    -6.83164477E-03   # V_12
  2  1     6.83164477E-03   # V_21
  2  2     9.99976664E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98447787E-01   # cos(theta_t)
  1  2    -5.56957506E-02   # sin(theta_t)
  2  1     5.56957506E-02   # -sin(theta_t)
  2  2     9.98447787E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99906574E-01   # cos(theta_b)
  1  2     1.36690626E-02   # sin(theta_b)
  2  1    -1.36690626E-02   # -sin(theta_b)
  2  2     9.99906574E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06944381E-01   # cos(theta_tau)
  1  2     7.07269144E-01   # sin(theta_tau)
  2  1    -7.07269144E-01   # -sin(theta_tau)
  2  2     7.06944381E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01751718E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68717615E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43926289E+02   # higgs               
         4     7.74948221E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68717615E+03  # The gauge couplings
     1     3.62569335E-01   # gprime(Q) DRbar
     2     6.38438541E-01   # g(Q) DRbar
     3     1.02371831E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68717615E+03  # The trilinear couplings
  1  1     2.70642651E-06   # A_u(Q) DRbar
  2  2     2.70646481E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68717615E+03  # The trilinear couplings
  1  1     7.01829917E-07   # A_d(Q) DRbar
  2  2     7.01960127E-07   # A_s(Q) DRbar
  3  3     1.58342829E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68717615E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.49877327E-07   # A_mu(Q) DRbar
  3  3     1.51681440E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68717615E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50455314E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68717615E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14167467E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68717615E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06308634E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68717615E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.29778403E+04   # M^2_Hd              
        22    -9.03153792E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41487827E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.14379699E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47654145E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47654145E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52345855E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52345855E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.97242484E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.53171611E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.12520257E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.72162582E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10775776E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.93745389E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.83024286E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.69515256E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.43802910E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.00074682E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67653043E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59538282E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.12112534E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.85046780E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.65843130E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.43835322E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.39580365E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.17805204E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.98402256E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.14869046E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.53400241E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.48048710E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.70156733E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.31577270E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.82377479E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.11754967E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.91184298E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.94939135E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.94153394E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62692851E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.27657607E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.86373673E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.25430474E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.56570581E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.05935023E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15631984E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60392659E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.89181016E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.26603604E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.95735135E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39607020E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.95441548E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.06079460E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62461135E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.37761100E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.06335941E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.24859125E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.90999889E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.06618294E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64879097E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.56609617E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.23232050E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.45630295E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.33993512E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54338947E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.94939135E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.94153394E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62692851E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.27657607E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.86373673E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.25430474E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.56570581E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.05935023E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15631984E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60392659E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.89181016E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.26603604E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.95735135E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39607020E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.95441548E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.06079460E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62461135E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.37761100E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.06335941E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.24859125E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.90999889E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.06618294E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64879097E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.56609617E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.23232050E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.45630295E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.33993512E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54338947E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89760475E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.96013933E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00585135E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.86767030E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.92471830E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99813425E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.09600088E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55597407E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998196E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.79865927E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.16395368E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.85887193E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.89760475E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.96013933E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00585135E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.86767030E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.92471830E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99813425E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.09600088E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55597407E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998196E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.79865927E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.16395368E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.85887193E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.75483420E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52605740E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16133073E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31261187E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69639210E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.60976745E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13344164E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.50905970E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.67773104E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25630024E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.71991585E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89787032E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00223149E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99485859E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.03237466E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.02786504E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00290974E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.01481666E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89787032E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00223149E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99485859E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.03237466E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.02786504E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00290974E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.01481666E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89780334E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00214939E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99459462E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.69040205E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     9.65497143E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00323490E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.09376674E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.29654435E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.85748351E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.70345904E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.01424241E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.00121939E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.54620498E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.35012663E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.35652741E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.90122785E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.24530300E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.80916229E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.51908377E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.88680893E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.48867974E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.07327208E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.31699206E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.31699206E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.67725164E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.48328710E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.54503247E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.54503247E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.22617939E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.22617939E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.18459571E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.18459571E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.79243773E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.45333227E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.46379273E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.40879726E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.40879726E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.56581659E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.44407703E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.51248992E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.51248992E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.25675717E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.25675717E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.65750423E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.65750423E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.77081911E-03   # h decays
#          BR         NDA      ID1       ID2
     6.70995975E-01    2           5        -5   # BR(h -> b       bb     )
     5.46562813E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93485235E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.10705232E-04    2           3        -3   # BR(h -> s       sb     )
     1.77333183E-02    2           4        -4   # BR(h -> c       cb     )
     5.75504677E-02    2          21        21   # BR(h -> g       g      )
     1.96683771E-03    2          22        22   # BR(h -> gam     gam    )
     1.29794514E-03    2          22        23   # BR(h -> Z       gam    )
     1.73533048E-01    2          24       -24   # BR(h -> W+      W-     )
     2.16619365E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.41117139E+01   # H decays
#          BR         NDA      ID1       ID2
     7.39793328E-01    2           5        -5   # BR(H -> b       bb     )
     1.76216809E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.23060528E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.65120289E-04    2           3        -3   # BR(H -> s       sb     )
     2.15842529E-07    2           4        -4   # BR(H -> c       cb     )
     2.15316739E-02    2           6        -6   # BR(H -> t       tb     )
     2.61500892E-05    2          21        21   # BR(H -> g       g      )
     1.20343222E-08    2          22        22   # BR(H -> gam     gam    )
     8.31126988E-09    2          23        22   # BR(H -> Z       gam    )
     2.93804249E-05    2          24       -24   # BR(H -> W+      W-     )
     1.46720976E-05    2          23        23   # BR(H -> Z       Z      )
     7.86748719E-05    2          25        25   # BR(H -> h       h      )
     3.03181460E-23    2          36        36   # BR(H -> A       A      )
     4.41428800E-19    2          23        36   # BR(H -> Z       A      )
     2.19586778E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.09811384E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.09472463E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.98546315E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.68165964E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.34831488E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33168416E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84034349E-01    2           5        -5   # BR(A -> b       bb     )
     1.86734517E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.60247704E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.10921282E-04    2           3        -3   # BR(A -> s       sb     )
     2.28830492E-07    2           4        -4   # BR(A -> c       cb     )
     2.28147467E-02    2           6        -6   # BR(A -> t       tb     )
     6.71872456E-05    2          21        21   # BR(A -> g       g      )
     3.32218299E-08    2          22        22   # BR(A -> gam     gam    )
     6.61708580E-08    2          23        22   # BR(A -> Z       gam    )
     3.10172522E-05    2          23        25   # BR(A -> Z       h      )
     2.62433336E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21276980E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30826752E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.92808103E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04084593E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10476119E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.38984086E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.44989437E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.07046059E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96582883E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02163067E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.18130678E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.97446226E-05    2          24        25   # BR(H+ -> W+      h      )
     7.27350798E-14    2          24        36   # BR(H+ -> W+      A      )
     1.95680355E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.59789350E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.78605743E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
