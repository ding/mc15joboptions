#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.89550441E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04180898E+01   # W+
        25     1.25380831E+02   # h
        35     3.99999900E+03   # H
        36     3.99999486E+03   # A
        37     4.00102400E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01236366E+03   # ~d_L
   2000001     4.01176082E+03   # ~d_R
   1000002     4.01170311E+03   # ~u_L
   2000002     4.01316995E+03   # ~u_R
   1000003     4.01236366E+03   # ~s_L
   2000003     4.01176082E+03   # ~s_R
   1000004     4.01170311E+03   # ~c_L
   2000004     4.01316995E+03   # ~c_R
   1000005     4.27424163E+02   # ~b_1
   2000005     4.01776105E+03   # ~b_2
   1000006     4.08226189E+02   # ~t_1
   2000006     2.02364746E+03   # ~t_2
   1000011     4.00201125E+03   # ~e_L
   2000011     4.00281902E+03   # ~e_R
   1000012     4.00089102E+03   # ~nu_eL
   1000013     4.00201125E+03   # ~mu_L
   2000013     4.00281902E+03   # ~mu_R
   1000014     4.00089102E+03   # ~nu_muL
   1000015     4.00453597E+03   # ~tau_1
   2000015     4.00817821E+03   # ~tau_2
   1000016     4.00352335E+03   # ~nu_tauL
   1000021     1.95755210E+03   # ~g
   1000022     9.38127555E+01   # ~chi_10
   1000023    -1.36494311E+02   # ~chi_20
   1000025     1.52598504E+02   # ~chi_30
   1000035     2.05807203E+03   # ~chi_40
   1000024     1.31613309E+02   # ~chi_1+
   1000037     2.05823658E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.61892999E-01   # N_11
  1  2     1.38342336E-02   # N_12
  1  3     5.33902151E-01   # N_13
  1  4     3.66437124E-01   # N_14
  2  1    -1.35873633E-01   # N_21
  2  2     2.73137615E-02   # N_22
  2  3    -6.85300915E-01   # N_23
  2  4     7.14951027E-01   # N_24
  3  1     6.33290295E-01   # N_31
  3  2     2.39295335E-02   # N_32
  3  3     4.95288682E-01   # N_33
  3  4     5.94188439E-01   # N_34
  4  1    -9.03598072E-04   # N_41
  4  2     9.99244690E-01   # N_42
  4  3    -5.20401458E-04   # N_43
  4  4    -3.88453711E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.40299549E-04   # U_11
  1  2     9.99999726E-01   # U_12
  2  1    -9.99999726E-01   # U_21
  2  2     7.40299549E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49492468E-02   # V_11
  1  2    -9.98489149E-01   # V_12
  2  1    -9.98489149E-01   # V_21
  2  2    -5.49492468E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96130674E-01   # cos(theta_t)
  1  2    -8.78844714E-02   # sin(theta_t)
  2  1     8.78844714E-02   # -sin(theta_t)
  2  2     9.96130674E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999924E-01   # cos(theta_b)
  1  2    -3.89871766E-04   # sin(theta_b)
  2  1     3.89871766E-04   # -sin(theta_b)
  2  2     9.99999924E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.02714409E-01   # cos(theta_tau)
  1  2     7.11472037E-01   # sin(theta_tau)
  2  1    -7.11472037E-01   # -sin(theta_tau)
  2  2    -7.02714409E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00316630E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.95504410E+02  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44508381E+02   # higgs               
         4     1.61730979E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.95504410E+02  # The gauge couplings
     1     3.60880294E-01   # gprime(Q) DRbar
     2     6.36854322E-01   # g(Q) DRbar
     3     1.03874313E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.95504410E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.68083440E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.95504410E+02  # The trilinear couplings
  1  1     2.08019384E-07   # A_d(Q) DRbar
  2  2     2.08039630E-07   # A_s(Q) DRbar
  3  3     3.72194335E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.95504410E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.51978639E-08   # A_mu(Q) DRbar
  3  3     4.56521179E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.95504410E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75561768E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.95504410E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82226601E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.95504410E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03828308E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.95504410E+02  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58492099E+07   # M^2_Hd              
        22    -4.91277268E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40706475E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.40855464E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45343415E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45343415E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54656585E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54656585E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.40989146E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.52378806E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.11828003E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.83753326E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.52039865E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21458867E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.55415006E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23169583E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.40735074E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.37946324E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.47281417E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.10123728E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.31863940E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.09309356E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.24498841E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.29089330E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.34113125E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.51229870E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66186093E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.78419925E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.68379947E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.40260936E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.86034546E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.52250003E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.54945351E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15997897E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.79686000E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.95974310E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.33222545E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.98871923E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77899165E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48030820E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.37653740E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80002436E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77497269E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.27851150E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.53768580E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53064994E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60766781E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.20922416E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.01960090E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.21382328E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.46951946E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44749900E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77918591E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.17715876E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.45313446E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.99905548E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77962313E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.58139422E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.56934331E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53287862E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53984260E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.37344615E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.66032122E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.77625907E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.44024684E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85584256E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77899165E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48030820E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.37653740E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80002436E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77497269E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.27851150E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.53768580E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53064994E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60766781E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.20922416E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.01960090E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.21382328E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.46951946E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44749900E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77918591E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.17715876E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.45313446E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.99905548E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77962313E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.58139422E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.56934331E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53287862E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53984260E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.37344615E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.66032122E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.77625907E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.44024684E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85584256E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13964871E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.97110797E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.26630471E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.51338263E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77514338E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.62448339E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56373889E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07038024E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.80915622E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84527938E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00631142E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.42678327E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13964871E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.97110797E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.26630471E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.51338263E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77514338E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.62448339E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56373889E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07038024E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.80915622E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84527938E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00631142E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.42678327E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09924483E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27897798E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00888006E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60233792E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38972462E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41893873E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78617760E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11144646E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11455533E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33843700E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35207295E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42347643E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22189069E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85386252E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14074015E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01919519E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.57864599E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.74846238E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77797372E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09683523E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54123004E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14074015E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01919519E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.57864599E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.74846238E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77797372E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09683523E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54123004E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47547228E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.21641063E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.04468386E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.19825020E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51444832E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.78067209E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01557155E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.01590579E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33737256E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33737256E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11246529E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11246529E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10032431E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.81791956E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.85560758E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.15832197E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.75091663E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.09484372E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.45819104E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.03106057E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.40160969E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.94904575E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.87390416E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.82056080E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18561493E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53692999E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18561493E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53692999E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37404579E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51865215E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51865215E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47373844E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03349870E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03349870E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03349820E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.64636824E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.64636824E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.64636824E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.64636824E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.82124931E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.82124931E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.82124931E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.82124931E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.67676102E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.67676102E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.30935962E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.69699882E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.99237926E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.80831590E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.21613688E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.80831590E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.21613688E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.78130471E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.40978401E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.40978401E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.39547995E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.85126059E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.85126059E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.85125330E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.92368794E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.97609853E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.92368794E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.97609853E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.71544029E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.05556913E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.05556913E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.91655174E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.10729202E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.10729202E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.10729222E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.90366318E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.90366318E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.90366318E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.90366318E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.96783825E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.96783825E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.96783825E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.96783825E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.86443998E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.86443998E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.81697126E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.93754453E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.19768318E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.10174302E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.95211949E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.95211949E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.83639998E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.90319474E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.34662577E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89671823E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89671823E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.90673776E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.90673776E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.03272175E-03   # h decays
#          BR         NDA      ID1       ID2
     5.98366277E-01    2           5        -5   # BR(h -> b       bb     )
     6.45395699E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28470315E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84639458E-04    2           3        -3   # BR(h -> s       sb     )
     2.10444437E-02    2           4        -4   # BR(h -> c       cb     )
     6.83789452E-02    2          21        21   # BR(h -> g       g      )
     2.36847450E-03    2          22        22   # BR(h -> gam     gam    )
     1.60398714E-03    2          22        23   # BR(h -> Z       gam    )
     2.15770043E-01    2          24       -24   # BR(h -> W+      W-     )
     2.72151506E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.45034744E+01   # H decays
#          BR         NDA      ID1       ID2
     3.41014843E-01    2           5        -5   # BR(H -> b       bb     )
     6.08326714E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15089592E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54413965E-04    2           3        -3   # BR(H -> s       sb     )
     7.13722200E-08    2           4        -4   # BR(H -> c       cb     )
     7.15298226E-03    2           6        -6   # BR(H -> t       tb     )
     1.08678048E-06    2          21        21   # BR(H -> g       g      )
     2.67540619E-10    2          22        22   # BR(H -> gam     gam    )
     1.83560768E-09    2          23        22   # BR(H -> Z       gam    )
     2.06353232E-06    2          24       -24   # BR(H -> W+      W-     )
     1.03105139E-06    2          23        23   # BR(H -> Z       Z      )
     7.56908257E-06    2          25        25   # BR(H -> h       h      )
     1.93086293E-24    2          36        36   # BR(H -> A       A      )
     5.26310293E-20    2          23        36   # BR(H -> Z       A      )
     1.86674707E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66434586E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66434586E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.34995326E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.60050598E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68079075E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.47827710E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.92963815E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.92358183E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.04387053E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.39503087E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.37000218E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.52348167E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.91718330E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.91718330E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.43194136E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.45048770E+01   # A decays
#          BR         NDA      ID1       ID2
     3.41032159E-01    2           5        -5   # BR(A -> b       bb     )
     6.08315371E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15085412E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54450471E-04    2           3        -3   # BR(A -> s       sb     )
     7.18176291E-08    2           4        -4   # BR(A -> c       cb     )
     7.16509728E-03    2           6        -6   # BR(A -> t       tb     )
     1.47224710E-05    2          21        21   # BR(A -> g       g      )
     4.06251725E-08    2          22        22   # BR(A -> gam     gam    )
     1.61864653E-08    2          23        22   # BR(A -> Z       gam    )
     2.05916885E-06    2          23        25   # BR(A -> Z       h      )
     1.86978878E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66432466E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66432466E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.92621760E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.21992985E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.32935340E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.96501295E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.55409687E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.61080749E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.30395657E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.27219325E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.80597780E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.97546942E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.97546942E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43850957E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.43391543E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09812019E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15614590E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.47770287E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22149713E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51301241E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.46438955E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.06602002E-06    2          24        25   # BR(H+ -> W+      h      )
     2.79328516E-14    2          24        36   # BR(H+ -> W+      A      )
     4.83260347E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.25039480E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.06664983E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67014960E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.56542715E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57181767E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.20275107E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.98467804E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.58483058E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
