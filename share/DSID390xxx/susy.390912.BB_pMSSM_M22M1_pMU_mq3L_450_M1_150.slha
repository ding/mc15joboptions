#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11627945E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03995290E+01   # W+
        25     1.24979950E+02   # h
        35     3.00022155E+03   # H
        36     2.99999961E+03   # A
        37     3.00097291E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02092719E+03   # ~d_L
   2000001     3.01565342E+03   # ~d_R
   1000002     3.02000569E+03   # ~u_L
   2000002     3.01780655E+03   # ~u_R
   1000003     3.02092719E+03   # ~s_L
   2000003     3.01565342E+03   # ~s_R
   1000004     3.02000569E+03   # ~c_L
   2000004     3.01780655E+03   # ~c_R
   1000005     5.05177183E+02   # ~b_1
   2000005     3.01521500E+03   # ~b_2
   1000006     5.01700993E+02   # ~t_1
   2000006     2.99408416E+03   # ~t_2
   1000011     3.00677807E+03   # ~e_L
   2000011     3.00097959E+03   # ~e_R
   1000012     3.00538161E+03   # ~nu_eL
   1000013     3.00677807E+03   # ~mu_L
   2000013     3.00097959E+03   # ~mu_R
   1000014     3.00538161E+03   # ~nu_muL
   1000015     2.98595611E+03   # ~tau_1
   2000015     3.02223191E+03   # ~tau_2
   1000016     3.00554913E+03   # ~nu_tauL
   1000021     2.32545750E+03   # ~g
   1000022     1.51348421E+02   # ~chi_10
   1000023     3.19605118E+02   # ~chi_20
   1000025    -3.00324478E+03   # ~chi_30
   1000035     3.00379197E+03   # ~chi_40
   1000024     3.19766166E+02   # ~chi_1+
   1000037     3.00444515E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888326E-01   # N_11
  1  2    -1.15428467E-03   # N_12
  1  3     1.48262924E-02   # N_13
  1  4    -1.47796048E-03   # N_14
  2  1     1.55102766E-03   # N_21
  2  2     9.99642955E-01   # N_22
  2  3    -2.63834336E-02   # N_23
  2  4     3.93339451E-03   # N_24
  3  1    -9.41743608E-03   # N_31
  3  2     1.58877533E-02   # N_32
  3  3     7.06841254E-01   # N_33
  3  4     7.07131057E-01   # N_34
  4  1    -1.14996478E-02   # N_41
  4  2     2.14525004E-02   # N_42
  4  3     7.06724513E-01   # N_43
  4  4    -7.07070019E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99302917E-01   # U_11
  1  2    -3.73320172E-02   # U_12
  2  1     3.73320172E-02   # U_21
  2  2     9.99302917E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984505E-01   # V_11
  1  2    -5.56691690E-03   # V_12
  2  1     5.56691690E-03   # V_21
  2  2     9.99984505E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98898979E-01   # cos(theta_t)
  1  2    -4.69130020E-02   # sin(theta_t)
  2  1     4.69130020E-02   # -sin(theta_t)
  2  2     9.98898979E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99922025E-01   # cos(theta_b)
  1  2     1.24877508E-02   # sin(theta_b)
  2  1    -1.24877508E-02   # -sin(theta_b)
  2  2     9.99922025E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06907279E-01   # cos(theta_tau)
  1  2     7.07306227E-01   # sin(theta_tau)
  2  1    -7.07306227E-01   # -sin(theta_tau)
  2  2     7.06907279E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01945125E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.16279452E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44808900E+02   # higgs               
         4     6.43540575E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.16279452E+03  # The gauge couplings
     1     3.61322453E-01   # gprime(Q) DRbar
     2     6.38589534E-01   # g(Q) DRbar
     3     1.03203987E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.16279452E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.22692465E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.16279452E+03  # The trilinear couplings
  1  1     3.07607345E-07   # A_d(Q) DRbar
  2  2     3.07668699E-07   # A_s(Q) DRbar
  3  3     7.09088508E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.16279452E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.66136200E-08   # A_mu(Q) DRbar
  3  3     6.74010249E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.16279452E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.57559551E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.16279452E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.11798536E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.16279452E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05496225E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.16279452E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.06360742E+04   # M^2_Hd              
        22    -9.14730034E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41556088E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.40983533E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48254302E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48254302E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51745698E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51745698E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.75924255E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.49127150E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.11309468E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     8.63777817E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05514534E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.15967647E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.90967224E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.84214333E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.16285134E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.65968951E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.52259526E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.03014443E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.30861415E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.95882998E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.97224093E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.53187607E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.26060619E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.86428878E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.32953664E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.98121543E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.86255307E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.93834511E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28074450E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.93414648E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.25657835E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.13714570E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02981707E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.79577523E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62312997E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.85743352E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.50230844E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.24650796E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.30800412E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.07240418E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.20754139E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56729740E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.71063835E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.56490752E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.16549774E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43269886E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.03494336E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.93698074E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62057977E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.43885599E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.41943840E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.24078130E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.57359867E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.07926824E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69550966E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45783086E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.05538361E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.26256521E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.45507164E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55421585E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02981707E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.79577523E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62312997E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.85743352E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.50230844E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.24650796E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.30800412E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.07240418E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.20754139E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56729740E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.71063835E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.56490752E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.16549774E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43269886E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.03494336E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.93698074E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62057977E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.43885599E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.41943840E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.24078130E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.57359867E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.07926824E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69550966E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45783086E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.05538361E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.26256521E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.45507164E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55421585E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96150713E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.76464552E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01277705E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.89389342E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71056761E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01075838E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.06489271E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55061880E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997636E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.36395401E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.96150713E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.76464552E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01277705E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.89389342E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71056761E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01075838E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.06489271E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55061880E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997636E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.36395401E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.78483097E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.47771063E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17774498E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34454439E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72549703E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56002420E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15043340E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.03365595E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.17992278E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28931049E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     7.97679597E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96182379E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.83925668E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00051793E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.79883874E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69044941E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01555640E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.40476968E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96182379E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.83925668E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00051793E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.79883874E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69044941E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01555640E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.40476968E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96238895E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.83840723E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00026484E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.25405183E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.28654458E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01589443E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     2.56800635E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.02752787E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.65849967E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.68905002E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.32789484E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.21626575E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.64912998E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.05666283E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.49491957E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.85706838E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.85986883E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.93641202E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.50635880E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.05559537E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.26930705E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.27247938E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.46329312E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.46329312E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.71881994E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.37471882E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.69053822E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.69053822E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.23705456E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.23705456E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.73707793E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.73707793E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     1.73707793E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.73707793E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.65352089E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.65352089E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.96591543E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.59805310E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.36902798E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.53001129E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.53001129E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.31709813E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.49588084E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.66865638E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.66865638E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.26242006E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.26242006E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.03112171E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.03112171E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     4.03112171E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.03112171E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.86782236E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.86782236E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.93042883E-03   # h decays
#          BR         NDA      ID1       ID2
     6.79155372E-01    2           5        -5   # BR(h -> b       bb     )
     5.29621624E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.87487697E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.97920088E-04    2           3        -3   # BR(h -> s       sb     )
     1.71681914E-02    2           4        -4   # BR(h -> c       cb     )
     5.63664407E-02    2          21        21   # BR(h -> g       g      )
     1.90320477E-03    2          22        22   # BR(h -> gam     gam    )
     1.26508689E-03    2          22        23   # BR(h -> Z       gam    )
     1.69419136E-01    2          24       -24   # BR(h -> W+      W-     )
     2.11749980E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.38895305E+01   # H decays
#          BR         NDA      ID1       ID2
     7.40525240E-01    2           5        -5   # BR(H -> b       bb     )
     1.79043871E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.33056344E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.77390239E-04    2           3        -3   # BR(H -> s       sb     )
     2.19473311E-07    2           4        -4   # BR(H -> c       cb     )
     2.18938907E-02    2           6        -6   # BR(H -> t       tb     )
     3.45426770E-05    2          21        21   # BR(H -> g       g      )
     5.30650272E-08    2          22        22   # BR(H -> gam     gam    )
     8.38407928E-09    2          23        22   # BR(H -> Z       gam    )
     3.54196093E-05    2          24       -24   # BR(H -> W+      W-     )
     1.76879678E-05    2          23        23   # BR(H -> Z       Z      )
     8.91335811E-05    2          25        25   # BR(H -> h       h      )
    -1.51518244E-23    2          36        36   # BR(H -> A       A      )
     4.16198863E-17    2          23        36   # BR(H -> Z       A      )
     2.34075475E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12651509E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.16644875E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.27964873E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.25953203E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.34601644E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31625962E+01   # A decays
#          BR         NDA      ID1       ID2
     7.81479544E-01    2           5        -5   # BR(A -> b       bb     )
     1.88922737E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.67984717E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.20423953E-04    2           3        -3   # BR(A -> s       sb     )
     2.31512010E-07    2           4        -4   # BR(A -> c       cb     )
     2.30820982E-02    2           6        -6   # BR(A -> t       tb     )
     6.79745796E-05    2          21        21   # BR(A -> g       g      )
     3.15797306E-08    2          22        22   # BR(A -> gam     gam    )
     6.69428025E-08    2          23        22   # BR(A -> Z       gam    )
     3.72298155E-05    2          23        25   # BR(A -> Z       h      )
     2.66593135E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22513353E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32844581E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.04787825E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.79229914E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.07940545E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.54028235E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.98181878E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.90818386E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.27841216E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.08593911E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.04110674E-01    2           6        -5   # BR(H+ -> t       bb     )
     5.01235779E-05    2          24        25   # BR(H+ -> W+      h      )
     1.17363112E-13    2          24        36   # BR(H+ -> W+      A      )
     2.10829532E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.05374712E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.65794482E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
