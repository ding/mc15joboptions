#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90064322E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.15959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.22900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04211143E+01   # W+
        25     1.24783895E+02   # h
        35     4.00000653E+03   # H
        36     3.99999578E+03   # A
        37     4.00097711E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01268706E+03   # ~d_L
   2000001     4.01218103E+03   # ~d_R
   1000002     4.01203337E+03   # ~u_L
   2000002     4.01271387E+03   # ~u_R
   1000003     4.01268706E+03   # ~s_L
   2000003     4.01218103E+03   # ~s_R
   1000004     4.01203337E+03   # ~c_L
   2000004     4.01271387E+03   # ~c_R
   1000005     2.02696212E+03   # ~b_1
   2000005     4.01875872E+03   # ~b_2
   1000006     4.29481080E+02   # ~t_1
   2000006     2.03789727E+03   # ~t_2
   1000011     4.00160536E+03   # ~e_L
   2000011     4.00360612E+03   # ~e_R
   1000012     4.00049345E+03   # ~nu_eL
   1000013     4.00160536E+03   # ~mu_L
   2000013     4.00360612E+03   # ~mu_R
   1000014     4.00049345E+03   # ~nu_muL
   1000015     4.00376806E+03   # ~tau_1
   2000015     4.00921579E+03   # ~tau_2
   1000016     4.00309268E+03   # ~nu_tauL
   1000021     1.96779849E+03   # ~g
   1000022     2.91199715E+02   # ~chi_10
   1000023    -3.33030116E+02   # ~chi_20
   1000025     3.54703502E+02   # ~chi_30
   1000035     2.06751749E+03   # ~chi_40
   1000024     3.30357545E+02   # ~chi_1+
   1000037     2.06769259E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.60808215E-01   # N_11
  1  2     1.82556286E-02   # N_12
  1  3     4.87832268E-01   # N_13
  1  4     4.27618135E-01   # N_14
  2  1    -5.15780346E-02   # N_21
  2  2     2.48170860E-02   # N_22
  2  3    -7.02867407E-01   # N_23
  2  4     7.09014264E-01   # N_24
  3  1     6.46923120E-01   # N_31
  3  2     2.50248706E-02   # N_32
  3  3     5.17665476E-01   # N_33
  3  4     5.59362751E-01   # N_34
  4  1    -1.02092312E-03   # N_41
  4  2     9.99211990E-01   # N_42
  4  3    -4.42055886E-03   # N_43
  4  4    -3.94311585E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.25710819E-03   # U_11
  1  2     9.99980424E-01   # U_12
  2  1    -9.99980424E-01   # U_21
  2  2     6.25710819E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.57752586E-02   # V_11
  1  2    -9.98443349E-01   # V_12
  2  1    -9.98443349E-01   # V_21
  2  2    -5.57752586E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.43970172E-02   # cos(theta_t)
  1  2     9.96432207E-01   # sin(theta_t)
  2  1    -9.96432207E-01   # -sin(theta_t)
  2  2    -8.43970172E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999101E-01   # cos(theta_b)
  1  2    -1.34089492E-03   # sin(theta_b)
  2  1     1.34089492E-03   # -sin(theta_b)
  2  2     9.99999101E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05645842E-01   # cos(theta_tau)
  1  2     7.08564708E-01   # sin(theta_tau)
  2  1    -7.08564708E-01   # -sin(theta_tau)
  2  2    -7.05645842E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00245032E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.00643224E+02  # DRbar Higgs Parameters
         1    -3.22900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44625889E+02   # higgs               
         4     1.63071736E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.00643224E+02  # The gauge couplings
     1     3.60857157E-01   # gprime(Q) DRbar
     2     6.34993880E-01   # g(Q) DRbar
     3     1.03762365E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.00643224E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.67946220E-07   # A_c(Q) DRbar
  3  3     2.15959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.00643224E+02  # The trilinear couplings
  1  1     2.09513570E-07   # A_d(Q) DRbar
  2  2     2.09533832E-07   # A_s(Q) DRbar
  3  3     3.73889164E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.00643224E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.70801916E-08   # A_mu(Q) DRbar
  3  3     4.75563355E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.00643224E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.73803089E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.00643224E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87839684E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.00643224E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03039692E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.00643224E+02  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57610116E+07   # M^2_Hd              
        22    -1.31344001E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39872407E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.70076438E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.08221875E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.78772358E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.85542555E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.41617001E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.40903194E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.55221187E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.82033139E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.81370292E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.99873728E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.90031618E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.60944486E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.69035091E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.11290573E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.84582058E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.17498988E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63757312E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65106172E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81363334E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54307440E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.45394894E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65333913E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.67379083E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12973097E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.63697416E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.17409136E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.54624699E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.05972447E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74821669E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.39007617E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.98051008E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.90157018E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79978674E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.44373268E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.58735567E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52272751E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57843888E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.23032934E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.48004360E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.32419963E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.18872723E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44306674E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74840029E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.31368861E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.04654051E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.30492036E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80491193E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     6.85155663E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61971648E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52498030E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51205585E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.42920906E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.86201694E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.06474976E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.31910121E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85467413E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74821669E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.39007617E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.98051008E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.90157018E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79978674E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.44373268E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.58735567E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52272751E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57843888E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.23032934E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.48004360E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.32419963E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.18872723E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44306674E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74840029E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.31368861E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.04654051E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.30492036E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80491193E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     6.85155663E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61971648E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52498030E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51205585E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.42920906E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.86201694E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.06474976E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.31910121E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85467413E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10081196E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.78205835E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.03088388E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.85565772E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77380601E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.99793025E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56191951E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04802798E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.80081947E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.65732625E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.17260159E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.67695649E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10081196E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.78205835E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.03088388E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.85565772E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77380601E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.99793025E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56191951E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04802798E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.80081947E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.65732625E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.17260159E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.67695649E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06569531E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.15556224E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.41913460E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66430386E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39323458E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51321476E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79366438E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06551796E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11612000E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.20565284E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.48696337E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42089769E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.06347752E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84910590E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10193216E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03922558E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.49460146E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.97680339E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77735449E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.17460868E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53904749E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10193216E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03922558E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.49460146E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.97680339E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77735449E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.17460868E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53904749E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43025247E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.40381606E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.35245107E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.40836752E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51553549E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.72830769E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01689087E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.45736533E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33706825E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33706825E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11236961E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11236961E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10112429E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.67282039E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.89322373E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.71181211E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44882732E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.05174143E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.39193504E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.57358130E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37404170E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.92346740E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.82457064E-06    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18801162E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54078296E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18801162E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54078296E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36904824E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53170345E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53170345E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48855288E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.06010226E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.06010226E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.06010200E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.07002717E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.07002717E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.07002717E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.07002717E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.56676082E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.56676082E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.56676082E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.56676082E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.31778674E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.31778674E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.50222456E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.33588680E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.17016330E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.94746710E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.64113105E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.94746710E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.64113105E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.30327192E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.69553958E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.69553958E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.69457496E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.52956031E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.52956031E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.52952979E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.01754482E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.61673073E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.01754482E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.61673073E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.67079154E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.99847360E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.99847360E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.77186557E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.19895310E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.19895310E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.19895313E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.23967550E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.23967550E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.23967550E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.23967550E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.13219208E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.13219208E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.13219208E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.13219208E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.02480060E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.02480060E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.67417206E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.43552214E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.49913687E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.03042724E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38562964E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38562964E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.65093909E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.23746127E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.77339100E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.92442144E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.92442144E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.59727488E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.59727488E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.94225823E-03   # h decays
#          BR         NDA      ID1       ID2
     6.06371302E-01    2           5        -5   # BR(h -> b       bb     )
     6.56866966E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32533828E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.93719593E-04    2           3        -3   # BR(h -> s       sb     )
     2.14448441E-02    2           4        -4   # BR(h -> c       cb     )
     6.92699741E-02    2          21        21   # BR(h -> g       g      )
     2.36980038E-03    2          22        22   # BR(h -> gam     gam    )
     1.55579607E-03    2          22        23   # BR(h -> Z       gam    )
     2.06735889E-01    2          24       -24   # BR(h -> W+      W-     )
     2.58394449E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49098466E+01   # H decays
#          BR         NDA      ID1       ID2
     3.56307766E-01    2           5        -5   # BR(H -> b       bb     )
     6.03826230E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13498330E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52531718E-04    2           3        -3   # BR(H -> s       sb     )
     7.08238729E-08    2           4        -4   # BR(H -> c       cb     )
     7.09802661E-03    2           6        -6   # BR(H -> t       tb     )
     6.48428160E-07    2          21        21   # BR(H -> g       g      )
     9.66684165E-10    2          22        22   # BR(H -> gam     gam    )
     1.82857830E-09    2          23        22   # BR(H -> Z       gam    )
     1.66751087E-06    2          24       -24   # BR(H -> W+      W-     )
     8.33177999E-07    2          23        23   # BR(H -> Z       Z      )
     6.33711685E-06    2          25        25   # BR(H -> h       h      )
    -3.38206629E-24    2          36        36   # BR(H -> A       A      )
    -1.22310398E-20    2          23        36   # BR(H -> Z       A      )
     1.86172305E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.59435779E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.59435779E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.79669074E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.22811719E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.80115385E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.01009063E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.39151669E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.73356898E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.78875697E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.04428630E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.21237294E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.53425400E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.33287766E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.33287766E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.49076762E+01   # A decays
#          BR         NDA      ID1       ID2
     3.56347590E-01    2           5        -5   # BR(A -> b       bb     )
     6.03852946E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13507608E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52583890E-04    2           3        -3   # BR(A -> s       sb     )
     7.12907938E-08    2           4        -4   # BR(A -> c       cb     )
     7.11253600E-03    2           6        -6   # BR(A -> t       tb     )
     1.46144665E-05    2          21        21   # BR(A -> g       g      )
     5.79534278E-08    2          22        22   # BR(A -> gam     gam    )
     1.60761843E-08    2          23        22   # BR(A -> Z       gam    )
     1.66413217E-06    2          23        25   # BR(A -> Z       h      )
     1.87159591E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.59439456E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.59439456E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.39736481E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     9.09934128E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.49776415E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.48719617E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.03986172E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.87958031E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.03126291E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.75355780E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.35603257E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.47208600E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.47208600E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.48749779E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.69564721E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.04360996E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13687242E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.64521110E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21058013E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49055261E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.62722240E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.66693228E-06    2          24        25   # BR(H+ -> W+      h      )
     2.18246053E-14    2          24        36   # BR(H+ -> W+      A      )
     4.65280331E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.11382450E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.02155486E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.59740143E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.62959327E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58136994E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.58852464E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     8.98392310E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
