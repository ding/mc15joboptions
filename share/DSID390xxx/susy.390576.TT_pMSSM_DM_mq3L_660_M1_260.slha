#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12089027E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.06900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.23485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04165561E+01   # W+
        25     1.25345723E+02   # h
        35     4.00000798E+03   # H
        36     3.99999682E+03   # A
        37     4.00102154E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02324995E+03   # ~d_L
   2000001     4.02038670E+03   # ~d_R
   1000002     4.02259333E+03   # ~u_L
   2000002     4.02157115E+03   # ~u_R
   1000003     4.02324995E+03   # ~s_L
   2000003     4.02038670E+03   # ~s_R
   1000004     4.02259333E+03   # ~c_L
   2000004     4.02157115E+03   # ~c_R
   1000005     7.31051237E+02   # ~b_1
   2000005     4.02389951E+03   # ~b_2
   1000006     7.19845656E+02   # ~t_1
   2000006     2.25054954E+03   # ~t_2
   1000011     4.00420634E+03   # ~e_L
   2000011     4.00302254E+03   # ~e_R
   1000012     4.00308871E+03   # ~nu_eL
   1000013     4.00420634E+03   # ~mu_L
   2000013     4.00302254E+03   # ~mu_R
   1000014     4.00308871E+03   # ~nu_muL
   1000015     4.00473983E+03   # ~tau_1
   2000015     4.00749585E+03   # ~tau_2
   1000016     4.00475941E+03   # ~nu_tauL
   1000021     1.97681283E+03   # ~g
   1000022     2.51638703E+02   # ~chi_10
   1000023    -3.17507512E+02   # ~chi_20
   1000025     3.26989230E+02   # ~chi_30
   1000035     2.05355476E+03   # ~chi_40
   1000024     3.14791156E+02   # ~chi_1+
   1000037     2.05371933E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.91137534E-01   # N_11
  1  2    -1.16072138E-02   # N_12
  1  3    -3.56524586E-01   # N_13
  1  4    -2.80409321E-01   # N_14
  2  1    -5.74944319E-02   # N_21
  2  2     2.49638265E-02   # N_22
  2  3    -7.02269737E-01   # N_23
  2  4     7.09146257E-01   # N_24
  3  1     4.50074781E-01   # N_31
  3  2     2.83646890E-02   # N_32
  3  3     6.16190493E-01   # N_33
  3  4     6.45706909E-01   # N_34
  4  1    -9.88098536E-04   # N_41
  4  2     9.99218457E-01   # N_42
  4  3    -4.08816395E-03   # N_43
  4  4    -3.93037683E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.78699241E-03   # U_11
  1  2     9.99983255E-01   # U_12
  2  1    -9.99983255E-01   # U_21
  2  2     5.78699241E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.55958523E-02   # V_11
  1  2    -9.98453355E-01   # V_12
  2  1    -9.98453355E-01   # V_21
  2  2    -5.55958523E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96533245E-01   # cos(theta_t)
  1  2    -8.31955023E-02   # sin(theta_t)
  2  1     8.31955023E-02   # -sin(theta_t)
  2  2     9.96533245E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999537E-01   # cos(theta_b)
  1  2    -9.62288827E-04   # sin(theta_b)
  2  1     9.62288827E-04   # -sin(theta_b)
  2  2     9.99999537E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05600063E-01   # cos(theta_tau)
  1  2     7.08610296E-01   # sin(theta_tau)
  2  1    -7.08610296E-01   # -sin(theta_tau)
  2  2    -7.05600063E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00274349E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20890266E+03  # DRbar Higgs Parameters
         1    -3.06900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43873305E+02   # higgs               
         4     1.61842638E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20890266E+03  # The gauge couplings
     1     3.61698257E-01   # gprime(Q) DRbar
     2     6.36280307E-01   # g(Q) DRbar
     3     1.03189253E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20890266E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.16268878E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20890266E+03  # The trilinear couplings
  1  1     4.27418963E-07   # A_d(Q) DRbar
  2  2     4.27459070E-07   # A_s(Q) DRbar
  3  3     7.64683440E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20890266E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.32521562E-08   # A_mu(Q) DRbar
  3  3     9.41997415E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20890266E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67911323E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20890266E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83312170E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20890266E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03416432E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20890266E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57915352E+07   # M^2_Hd              
        22    -1.10076906E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.23485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40459653E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.26773000E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45358881E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45358881E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54641119E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54641119E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.60503528E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     9.15526116E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.27165080E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.69193062E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.12089246E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.31961938E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.92739268E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19603192E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.03055419E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.32256276E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.36743341E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.10735179E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.26442051E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.37968787E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.17327661E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.98978460E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.97167075E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.73855705E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.35633186E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.05727334E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.78478377E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66903817E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51701144E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78126295E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63656372E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.04412496E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59257587E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.19286402E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14260782E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.02022322E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.34447103E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.26463053E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.47896064E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78847787E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.14152342E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.70676563E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.11281713E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78831196E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33117725E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56447533E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52667601E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61295933E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.37972185E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.81505495E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.11145497E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.98189614E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44906696E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78868263E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.82064089E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.10871790E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.79144591E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79323671E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.77654704E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.59648082E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52886391E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54528413E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.14253460E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.73490732E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.89943510E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.77569788E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85627862E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78847787E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.14152342E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.70676563E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.11281713E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78831196E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33117725E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56447533E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52667601E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61295933E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.37972185E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.81505495E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.11145497E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.98189614E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44906696E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78868263E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.82064089E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.10871790E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.79144591E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79323671E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.77654704E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.59648082E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52886391E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54528413E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.14253460E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.73490732E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.89943510E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.77569788E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85627862E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14562490E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.24600893E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01637323E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.08585498E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77687941E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.39123292E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56788540E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06497438E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.95017429E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.29379226E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.01688244E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.34886161E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14562490E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.24600893E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01637323E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.08585498E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77687941E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.39123292E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56788540E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06497438E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.95017429E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.29379226E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.01688244E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.34886161E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09765743E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.85791158E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35344096E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.55109712E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39917419E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46986924E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80547349E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09508615E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.98267774E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.24483202E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60480178E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42518916E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05235478E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85761264E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14668600E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.36478034E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68116465E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.61591965E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78030669E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.12799359E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54522942E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14668600E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.36478034E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68116465E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.61591965E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78030669E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.12799359E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54522942E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47517115E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.23630054E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52290658E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.36966904E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52005413E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65282273E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02616709E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.05708437E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33466179E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33466179E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11156567E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11156567E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10754508E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.97482213E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.73441977E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.61640226E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.73379958E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.91158131E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.57739724E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.65257293E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.63685187E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.65855263E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.56351250E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17563562E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52478562E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17563562E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52478562E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45188691E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49532376E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49532376E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47259911E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98790254E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98790254E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98790229E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.16433051E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.16433051E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.16433051E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.16433051E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.21444299E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.21444299E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.21444299E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.21444299E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.37849806E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.37849806E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.17995451E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.80408568E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.10900094E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.12446870E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.45543749E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.12446870E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.45543749E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.40910761E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.31235210E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.31235210E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.30334076E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.68080197E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.68080197E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.68079427E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.15028602E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.08605571E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.15028602E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.08605571E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     9.36768123E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.36768123E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.52623088E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.87245734E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.87245734E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.87245736E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.74717822E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.74717822E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.74717822E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.74717822E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.24904901E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.24904901E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.24904901E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.24904901E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.12647185E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.12647185E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.97360614E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.06597138E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.07496311E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.25175914E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.56661739E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.56661739E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.99449387E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.62675267E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.49310412E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.83118872E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.83118872E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.84451826E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.84451826E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.00311734E-03   # h decays
#          BR         NDA      ID1       ID2
     5.96017729E-01    2           5        -5   # BR(h -> b       bb     )
     6.49876364E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30056627E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.88032077E-04    2           3        -3   # BR(h -> s       sb     )
     2.11953052E-02    2           4        -4   # BR(h -> c       cb     )
     6.92052800E-02    2          21        21   # BR(h -> g       g      )
     2.37892897E-03    2          22        22   # BR(h -> gam     gam    )
     1.61073839E-03    2          22        23   # BR(h -> Z       gam    )
     2.16589088E-01    2          24       -24   # BR(h -> W+      W-     )
     2.72972054E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48746032E+01   # H decays
#          BR         NDA      ID1       ID2
     3.52672094E-01    2           5        -5   # BR(H -> b       bb     )
     6.04214080E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13635464E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52693912E-04    2           3        -3   # BR(H -> s       sb     )
     7.08776820E-08    2           4        -4   # BR(H -> c       cb     )
     7.10341944E-03    2           6        -6   # BR(H -> t       tb     )
     8.70378549E-07    2          21        21   # BR(H -> g       g      )
     3.88100509E-10    2          22        22   # BR(H -> gam     gam    )
     1.82554486E-09    2          23        22   # BR(H -> Z       gam    )
     1.81986071E-06    2          24       -24   # BR(H -> W+      W-     )
     9.09299912E-07    2          23        23   # BR(H -> Z       Z      )
     7.07924406E-06    2          25        25   # BR(H -> h       h      )
     1.27260356E-24    2          36        36   # BR(H -> A       A      )
     8.22463672E-21    2          23        36   # BR(H -> Z       A      )
     1.85643086E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61629431E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61629431E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.97079478E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.19367658E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.12286561E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.83583876E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.69088766E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.03123253E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.61147832E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.08377405E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.09747407E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.38288457E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.60637859E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.60637859E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.39102762E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48697126E+01   # A decays
#          BR         NDA      ID1       ID2
     3.52729146E-01    2           5        -5   # BR(A -> b       bb     )
     6.04270901E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13655386E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52758705E-04    2           3        -3   # BR(A -> s       sb     )
     7.13401351E-08    2           4        -4   # BR(A -> c       cb     )
     7.11745869E-03    2           6        -6   # BR(A -> t       tb     )
     1.46245766E-05    2          21        21   # BR(A -> g       g      )
     5.68906754E-08    2          22        22   # BR(A -> gam     gam    )
     1.60745461E-08    2          23        22   # BR(A -> Z       gam    )
     1.81621437E-06    2          23        25   # BR(A -> Z       h      )
     1.86457565E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61642889E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61642889E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.71086612E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.02908899E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.35345862E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.47885750E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.37518251E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.09182041E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.06101884E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.90381931E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.21334207E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.68066829E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.68066829E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49448475E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.65716718E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03599175E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13417881E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.62058390E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20905246E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48740970E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.60310146E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.81576021E-06    2          24        25   # BR(H+ -> W+      h      )
     2.70601980E-14    2          24        36   # BR(H+ -> W+      A      )
     6.61860323E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.84524011E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.09378753E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61633297E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.12364965E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59742670E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.23152216E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.25004233E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.26809782E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
