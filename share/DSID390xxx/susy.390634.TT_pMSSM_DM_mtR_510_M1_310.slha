#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10048637E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.22900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04207109E+01   # W+
        25     1.24954215E+02   # h
        35     4.00000704E+03   # H
        36     3.99999612E+03   # A
        37     4.00099261E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01666335E+03   # ~d_L
   2000001     4.01531691E+03   # ~d_R
   1000002     4.01601102E+03   # ~u_L
   2000002     4.01588596E+03   # ~u_R
   1000003     4.01666335E+03   # ~s_L
   2000003     4.01531691E+03   # ~s_R
   1000004     4.01601102E+03   # ~c_L
   2000004     4.01588596E+03   # ~c_R
   1000005     2.03052875E+03   # ~b_1
   2000005     4.02080015E+03   # ~b_2
   1000006     5.34012916E+02   # ~t_1
   2000006     2.04200591E+03   # ~t_2
   1000011     4.00246491E+03   # ~e_L
   2000011     4.00358903E+03   # ~e_R
   1000012     4.00135347E+03   # ~nu_eL
   1000013     4.00246491E+03   # ~mu_L
   2000013     4.00358903E+03   # ~mu_R
   1000014     4.00135347E+03   # ~nu_muL
   1000015     4.00417098E+03   # ~tau_1
   2000015     4.00861832E+03   # ~tau_2
   1000016     4.00360576E+03   # ~nu_tauL
   1000021     1.97450967E+03   # ~g
   1000022     2.91184063E+02   # ~chi_10
   1000023    -3.33349363E+02   # ~chi_20
   1000025     3.54468278E+02   # ~chi_30
   1000035     2.06642156E+03   # ~chi_40
   1000024     3.30719517E+02   # ~chi_1+
   1000037     2.06659747E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.60839213E-01   # N_11
  1  2     1.82390815E-02   # N_12
  1  3     4.87799405E-01   # N_13
  1  4     4.27601180E-01   # N_14
  2  1    -5.15663523E-02   # N_21
  2  2     2.47953913E-02   # N_22
  2  3    -7.02871431E-01   # N_23
  2  4     7.09011884E-01   # N_24
  3  1     6.46887598E-01   # N_31
  3  2     2.50037566E-02   # N_32
  3  3     5.17691012E-01   # N_33
  3  4     5.59381144E-01   # N_34
  4  1    -1.01980602E-03   # N_41
  4  2     9.99213359E-01   # N_42
  4  3    -4.41673537E-03   # N_43
  4  4    -3.93969011E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.25168199E-03   # U_11
  1  2     9.99980458E-01   # U_12
  2  1    -9.99980458E-01   # U_21
  2  2     6.25168199E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.57267407E-02   # V_11
  1  2    -9.98446058E-01   # V_12
  2  1    -9.98446058E-01   # V_21
  2  2    -5.57267407E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -9.00072806E-02   # cos(theta_t)
  1  2     9.95941107E-01   # sin(theta_t)
  2  1    -9.95941107E-01   # -sin(theta_t)
  2  2    -9.00072806E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999109E-01   # cos(theta_b)
  1  2    -1.33491543E-03   # sin(theta_b)
  2  1     1.33491543E-03   # -sin(theta_b)
  2  2     9.99999109E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05706140E-01   # cos(theta_tau)
  1  2     7.08504653E-01   # sin(theta_tau)
  2  1    -7.08504653E-01   # -sin(theta_tau)
  2  2    -7.05706140E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00253126E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00486370E+03  # DRbar Higgs Parameters
         1    -3.22900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44331226E+02   # higgs               
         4     1.62474695E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00486370E+03  # The gauge couplings
     1     3.61209717E-01   # gprime(Q) DRbar
     2     6.35204878E-01   # g(Q) DRbar
     3     1.03520261E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00486370E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.46553191E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00486370E+03  # The trilinear couplings
  1  1     2.75316827E-07   # A_d(Q) DRbar
  2  2     2.75343228E-07   # A_s(Q) DRbar
  3  3     4.91379524E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00486370E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.15260124E-08   # A_mu(Q) DRbar
  3  3     6.21527393E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00486370E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72002147E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00486370E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86951353E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00486370E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03171055E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00486370E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57684557E+07   # M^2_Hd              
        22    -1.15249498E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39967135E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.55163438E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.32800492E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     6.87374413E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.35898258E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     9.65387601E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     6.98825541E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.95955741E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.64884266E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.35507884E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.36856569E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.47679619E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.87399347E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.88979811E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.99042134E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.92287654E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.64700187E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.71304956E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.14946653E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.66451011E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.44210336E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64004942E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65335863E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81670173E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54542035E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.44124777E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65950343E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.67127192E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12838044E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.59580077E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.16248514E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.92994156E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.03462945E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75274465E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.39154002E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.97215327E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.90146130E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80642727E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.42990515E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60064275E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52073586E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58152535E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.23361238E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.48072419E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.32617907E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.19070976E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44253981E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75293079E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.31422489E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.04463797E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.31457569E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81154175E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     6.83417809E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63296450E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52297957E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51491781E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.43822517E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.86399841E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.07023767E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.32461848E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85452889E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75274465E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.39154002E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.97215327E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.90146130E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80642727E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.42990515E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60064275E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52073586E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58152535E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.23361238E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.48072419E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.32617907E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.19070976E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44253981E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75293079E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.31422489E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.04463797E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.31457569E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81154175E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     6.83417809E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63296450E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52297957E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51491781E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.43822517E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.86399841E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.07023767E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.32461848E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85452889E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10701142E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.78555408E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.04532423E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.85491864E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77372081E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.98644306E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56172875E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05204250E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.80124329E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.65602614E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.17219078E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.66881836E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10701142E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.78555408E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.04532423E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.85491864E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77372081E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.98644306E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56172875E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05204250E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.80124329E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.65602614E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.17219078E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.66881836E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07124929E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.15592735E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.41769071E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66424738E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39330273E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.50962542E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79379093E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06971469E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11756604E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.20374644E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.48773434E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42023237E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.06331300E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84776130E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10812159E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03936379E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.49238543E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.97860802E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77728072E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.16548319E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53891601E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10812159E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03936379E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.49238543E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.97860802E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77728072E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.16548319E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53891601E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43630031E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.40641335E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.35063844E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.41077015E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51551157E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.72406502E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01685719E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.69022314E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33699640E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33699640E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11234562E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11234562E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10131596E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.66994792E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.97695319E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.17325794E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44666421E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.04321560E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.38910968E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.57221856E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37825809E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.13889853E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.56380124E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18734114E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54006801E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18734114E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54006801E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37087444E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53094750E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53094750E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48832665E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.05890094E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.05890094E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.05890072E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     9.47255940E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     9.47255940E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     9.47255940E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     9.47255940E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.15752296E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.15752296E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.15752296E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.15752296E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.65927542E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.65927542E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.35295422E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.43445126E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.39628942E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.46596837E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.30782086E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.46596837E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.30782086E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.93563581E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.84363711E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.84363711E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.84243726E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.83876247E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.83876247E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.83873360E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.97237299E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.55840028E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.97237299E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.55840028E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.59138975E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.86622744E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.86622744E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.63353407E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.17257152E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.17257152E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.17257155E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.21718066E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.21718066E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.21718066E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.21718066E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.05721038E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.05721038E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.05721038E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.05721038E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.94651157E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.94651157E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.67086201E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.43381083E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.49820998E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.02871769E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38339652E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38339652E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.66085602E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.25065210E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.79405070E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.96481803E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.96481803E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.26123246E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.26123246E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.96322010E-03   # h decays
#          BR         NDA      ID1       ID2
     6.03805037E-01    2           5        -5   # BR(h -> b       bb     )
     6.54307885E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31627136E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.91663629E-04    2           3        -3   # BR(h -> s       sb     )
     2.13548460E-02    2           4        -4   # BR(h -> c       cb     )
     6.89569245E-02    2          21        21   # BR(h -> g       g      )
     2.37291546E-03    2          22        22   # BR(h -> gam     gam    )
     1.57138677E-03    2          22        23   # BR(h -> Z       gam    )
     2.09526889E-01    2          24       -24   # BR(h -> W+      W-     )
     2.62579218E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48112768E+01   # H decays
#          BR         NDA      ID1       ID2
     3.55334239E-01    2           5        -5   # BR(H -> b       bb     )
     6.04912148E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13882284E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52985865E-04    2           3        -3   # BR(H -> s       sb     )
     7.09535408E-08    2           4        -4   # BR(H -> c       cb     )
     7.11102205E-03    2           6        -6   # BR(H -> t       tb     )
     6.76523452E-07    2          21        21   # BR(H -> g       g      )
     9.17190780E-10    2          22        22   # BR(H -> gam     gam    )
     1.83110158E-09    2          23        22   # BR(H -> Z       gam    )
     1.71166719E-06    2          24       -24   # BR(H -> W+      W-     )
     8.55240846E-07    2          23        23   # BR(H -> Z       Z      )
     6.54451210E-06    2          25        25   # BR(H -> h       h      )
    -1.23233016E-24    2          36        36   # BR(H -> A       A      )
     3.47800060E-21    2          23        36   # BR(H -> Z       A      )
     1.86163796E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.59838937E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.59838937E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.80145621E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.23297755E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.80478128E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.01402113E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.39494928E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.74317081E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.79186867E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.06479565E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.22480698E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.69649534E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.05518863E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.05518863E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.48076274E+01   # A decays
#          BR         NDA      ID1       ID2
     3.55383642E-01    2           5        -5   # BR(A -> b       bb     )
     6.04955303E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13897374E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53044988E-04    2           3        -3   # BR(A -> s       sb     )
     7.14209372E-08    2           4        -4   # BR(A -> c       cb     )
     7.12552014E-03    2           6        -6   # BR(A -> t       tb     )
     1.46411441E-05    2          21        21   # BR(A -> g       g      )
     5.80381123E-08    2          22        22   # BR(A -> gam     gam    )
     1.61043998E-08    2          23        22   # BR(A -> Z       gam    )
     1.70823132E-06    2          23        25   # BR(A -> Z       h      )
     1.87167561E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.59846966E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.59846966E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.40149433E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     9.10620361E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.50075945E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.49201270E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.04249748E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.88882775E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.03487516E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.77343763E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.36724026E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.20333617E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.20333617E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.47782836E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.68045190E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05430157E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14065271E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.63548611E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21272114E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49495738E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.61785603E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.71102225E-06    2          24        25   # BR(H+ -> W+      h      )
     2.36041270E-14    2          24        36   # BR(H+ -> W+      A      )
     4.66175302E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.07714039E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.02796335E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60140087E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.64816582E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58535408E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.61186108E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     8.44329642E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
