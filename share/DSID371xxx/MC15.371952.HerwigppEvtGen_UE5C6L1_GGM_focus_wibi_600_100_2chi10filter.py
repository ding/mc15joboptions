include ( 'MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )

slha_file = 'susy.371952.wino_bino_focus_600_100.slha'

include ( 'MC15JobOptions/Herwigpp_SUSYConfig.py' )
include ( 'MC15JobOptions/Herwigpp_EvtGen.py' )

sparticles['neutralinos'] = ['~chi_20','~chi_30','~chi_40']
cmds = buildHerwigppCommands(['charginos','neutralinos'], slha_file, 'TwoParticleInclusive')

evgenConfig.description = 'GGM wino-bino grid generation with mwino=600 mbino=100'
evgenConfig.keywords = ['SUSY', 'chargino', 'neutralino', 'bino']
evgenConfig.contact = [ 'osamu.jinnouchi@cern.ch']

genSeq.Herwigpp.Commands += cmds.splitlines()

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
filtSeq += ParticleFilter("ParticleFilter")
filtSeq.ParticleFilter.Ptcut = 0.
filtSeq.ParticleFilter.Etacut = 10.0
filtSeq.ParticleFilter.StatusReq = 11
filtSeq.ParticleFilter.PDG = 1000022
filtSeq.ParticleFilter.MinParts = 2 

del cmds
