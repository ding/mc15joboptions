#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13998174E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.65990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.05990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.86485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04183172E+01   # W+
        25     1.25653471E+02   # h
        35     4.00000724E+03   # H
        36     3.99999762E+03   # A
        37     4.00105165E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02851804E+03   # ~d_L
   2000001     4.02461891E+03   # ~d_R
   1000002     4.02786727E+03   # ~u_L
   2000002     4.02528935E+03   # ~u_R
   1000003     4.02851804E+03   # ~s_L
   2000003     4.02461891E+03   # ~s_R
   1000004     4.02786727E+03   # ~c_L
   2000004     4.02528935E+03   # ~c_R
   1000005     1.12306012E+03   # ~b_1
   2000005     4.02683577E+03   # ~b_2
   1000006     1.10013165E+03   # ~t_1
   2000006     1.88241446E+03   # ~t_2
   1000011     4.00505905E+03   # ~e_L
   2000011     4.00341710E+03   # ~e_R
   1000012     4.00394716E+03   # ~nu_eL
   1000013     4.00505905E+03   # ~mu_L
   2000013     4.00341710E+03   # ~mu_R
   1000014     4.00394716E+03   # ~nu_muL
   1000015     4.00351134E+03   # ~tau_1
   2000015     4.00851157E+03   # ~tau_2
   1000016     4.00512890E+03   # ~nu_tauL
   1000021     1.98338666E+03   # ~g
   1000022     5.41230047E+02   # ~chi_10
   1000023    -5.78511375E+02   # ~chi_20
   1000025     6.00734522E+02   # ~chi_30
   1000035     2.05148428E+03   # ~chi_40
   1000024     5.76423570E+02   # ~chi_1+
   1000037     2.05164806E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.25565784E-01   # N_11
  1  2     2.34416015E-02   # N_12
  1  3     5.02178055E-01   # N_13
  1  4     4.69916999E-01   # N_14
  2  1    -2.90091041E-02   # N_21
  2  2     2.23715476E-02   # N_22
  2  3    -7.05371658E-01   # N_23
  2  4     7.07890393E-01   # N_24
  3  1     6.87539970E-01   # N_31
  3  2     2.74936962E-02   # N_32
  3  3     5.00172893E-01   # N_33
  3  4     5.25699499E-01   # N_34
  4  1    -1.24673877E-03   # N_41
  4  2     9.99096643E-01   # N_42
  4  3    -9.75201332E-03   # N_43
  4  4    -4.13429805E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.37975676E-02   # U_11
  1  2     9.99904809E-01   # U_12
  2  1    -9.99904809E-01   # U_21
  2  2     1.37975676E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.84748751E-02   # V_11
  1  2    -9.98288881E-01   # V_12
  2  1    -9.98288881E-01   # V_21
  2  2    -5.84748751E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.86722831E-01   # cos(theta_t)
  1  2    -1.62413222E-01   # sin(theta_t)
  2  1     1.62413222E-01   # -sin(theta_t)
  2  2     9.86722831E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998220E-01   # cos(theta_b)
  1  2    -1.88679539E-03   # sin(theta_b)
  2  1     1.88679539E-03   # -sin(theta_b)
  2  2     9.99998220E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06397634E-01   # cos(theta_tau)
  1  2     7.07815218E-01   # sin(theta_tau)
  2  1    -7.07815218E-01   # -sin(theta_tau)
  2  2    -7.06397634E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00230817E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39981739E+03  # DRbar Higgs Parameters
         1    -5.65990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43560377E+02   # higgs               
         4     1.60982669E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39981739E+03  # The gauge couplings
     1     3.62084301E-01   # gprime(Q) DRbar
     2     6.35642288E-01   # g(Q) DRbar
     3     1.02835544E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39981739E+03  # The trilinear couplings
  1  1     1.59783245E-06   # A_u(Q) DRbar
  2  2     1.59784762E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39981739E+03  # The trilinear couplings
  1  1     5.92730223E-07   # A_d(Q) DRbar
  2  2     5.92784124E-07   # A_s(Q) DRbar
  3  3     1.05603913E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39981739E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.31955186E-07   # A_mu(Q) DRbar
  3  3     1.33304756E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39981739E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65224996E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39981739E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88291494E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39981739E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02738360E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39981739E+03  # The soft SUSY breaking masses at the scale Q
         1     5.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.55778384E+07   # M^2_Hd              
        22    -2.67227871E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.05990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.86485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40170418E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.01193611E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37530837E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37530837E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62469163E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62469163E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.78479112E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.10696805E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.11435143E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.62855226E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.15012827E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.00480658E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.10343202E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12803197E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.09562796E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.19902598E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.64714318E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.24341832E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.36247455E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.29682190E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.64099039E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.19173736E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.70259595E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.84646763E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65886348E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.61937579E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83567501E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53596528E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.29135945E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67604745E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.54396704E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.11683185E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.46303419E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.79729771E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.97817416E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.24935167E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77734244E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.10750320E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.39716503E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.09871481E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82989017E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.76644704E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64766730E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51417591E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59752113E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.86080711E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.54955775E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.54734963E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.82178026E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45872889E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77754528E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.20220500E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.55263395E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.92428377E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83579200E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.21071913E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68157316E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51634081E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53209081E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.45610269E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.18574617E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.63912169E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.25642270E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85892906E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77734244E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.10750320E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.39716503E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.09871481E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82989017E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.76644704E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64766730E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51417591E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59752113E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.86080711E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.54955775E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.54734963E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.82178026E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45872889E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77754528E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.20220500E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.55263395E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.92428377E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83579200E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.21071913E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68157316E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51634081E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53209081E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.45610269E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.18574617E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.63912169E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.25642270E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85892906E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13071664E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.53205631E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.68513000E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.63069560E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78844624E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.87734873E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59323271E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.00453279E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.28602034E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.40497321E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.70556588E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.80579675E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13071664E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.53205631E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.68513000E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.63069560E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78844624E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.87734873E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59323271E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.00453279E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.28602034E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.40497321E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.70556588E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.80579675E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.04637599E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.89342423E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.52720460E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.83570490E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41603206E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.61742509E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.84037585E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.03447300E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.89104289E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.81226261E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67496700E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45710435E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.73014283E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92264521E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13175901E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.44654222E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.45271521E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.51056081E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79357489E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.36979230E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56956417E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13175901E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.44654222E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.45271521E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.51056081E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79357489E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.36979230E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56956417E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44785322E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.58321786E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.77162938E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.91558814E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53927920E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.41427301E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.06264127E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.10551856E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33796461E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33796461E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11266770E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11266770E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09873538E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.45075912E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.38614280E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.20717813E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.18160979E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.71518853E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.48745075E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.48782444E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.00418196E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.61575663E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.35086064E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.91539886E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.19082920E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54501983E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.19082920E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54501983E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.33492857E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.54481839E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.54481839E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49335373E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.08711406E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.08711406E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.08711398E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.62857139E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.62857139E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.62857139E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.62857139E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.54285815E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.54285815E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.54285815E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.54285815E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.07155638E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.07155638E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.44061902E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.34232854E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.00746222E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.59329507E-04    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     5.76480584E-04    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.59329507E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     5.76480584E-04    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.62014147E-04    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.17880789E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.17880789E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.20697673E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.72068472E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.72068472E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.72064667E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.26341080E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.93667902E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.26341080E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.93667902E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.92408853E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.73805105E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.73805105E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.49589402E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.34701492E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.34701492E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.34701493E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.21865003E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.21865003E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.21865003E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.21865003E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.06210814E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.06210814E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.06210814E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.06210814E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.95631677E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.95631677E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.44870920E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.06015748E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.30472958E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.35442742E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.49206642E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.49206642E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.48534577E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.38256427E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.72602902E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.64235204E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.64235204E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.64277864E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.64277864E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01329670E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88251024E-01    2           5        -5   # BR(h -> b       bb     )
     6.49710382E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29996516E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87672877E-04    2           3        -3   # BR(h -> s       sb     )
     2.11833262E-02    2           4        -4   # BR(h -> c       cb     )
     6.92430022E-02    2          21        21   # BR(h -> g       g      )
     2.40006421E-03    2          22        22   # BR(h -> gam     gam    )
     1.65105509E-03    2          22        23   # BR(h -> Z       gam    )
     2.23298042E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82847784E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.56517999E+01   # H decays
#          BR         NDA      ID1       ID2
     3.69808403E-01    2           5        -5   # BR(H -> b       bb     )
     5.95776171E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10652024E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49165022E-04    2           3        -3   # BR(H -> s       sb     )
     6.98756872E-08    2           4        -4   # BR(H -> c       cb     )
     7.00299869E-03    2           6        -6   # BR(H -> t       tb     )
     5.67260773E-07    2          21        21   # BR(H -> g       g      )
     1.21000376E-08    2          22        22   # BR(H -> gam     gam    )
     1.80392904E-09    2          23        22   # BR(H -> Z       gam    )
     1.57528491E-06    2          24       -24   # BR(H -> W+      W-     )
     7.87096823E-07    2          23        23   # BR(H -> Z       Z      )
     6.50192919E-06    2          25        25   # BR(H -> h       h      )
    -5.22916789E-25    2          36        36   # BR(H -> A       A      )
     6.72166543E-20    2          23        36   # BR(H -> Z       A      )
     1.87025463E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.49427123E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.49427123E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.53864127E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.43777750E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.69271926E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.90879794E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.82445582E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.37382603E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.78466997E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.27726434E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.34644988E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.58162996E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.78456254E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.46035095E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.46035095E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.31901928E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.56502057E+01   # A decays
#          BR         NDA      ID1       ID2
     3.69844848E-01    2           5        -5   # BR(A -> b       bb     )
     5.95796132E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10658916E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49213812E-04    2           3        -3   # BR(A -> s       sb     )
     7.03396034E-08    2           4        -4   # BR(A -> c       cb     )
     7.01763770E-03    2           6        -6   # BR(A -> t       tb     )
     1.44194660E-05    2          21        21   # BR(A -> g       g      )
     7.15570622E-08    2          22        22   # BR(A -> gam     gam    )
     1.58539612E-08    2          23        22   # BR(A -> Z       gam    )
     1.57201269E-06    2          23        25   # BR(A -> Z       h      )
     1.94581133E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.49381886E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.49381886E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.27120281E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.58713080E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.50740698E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.21072716E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.01179105E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.15856474E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.93328882E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.64299727E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.18423653E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.62640803E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.62640803E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.59589521E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.97032665E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.92665033E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.09551836E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.82100585E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18714946E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44234821E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.79741820E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.56512301E-06    2          24        25   # BR(H+ -> W+      h      )
     3.05933089E-14    2          24        36   # BR(H+ -> W+      A      )
     3.90484885E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.18756476E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.09915400E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.48805903E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.51067635E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.48337280E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.51128827E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.32514699E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.10768395E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
