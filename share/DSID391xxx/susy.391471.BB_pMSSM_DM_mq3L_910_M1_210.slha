#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13016668E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.59900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.88485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04175247E+01   # W+
        25     1.25991833E+02   # h
        35     4.00000638E+03   # H
        36     3.99999668E+03   # A
        37     4.00105734E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02594585E+03   # ~d_L
   2000001     4.02258784E+03   # ~d_R
   1000002     4.02529474E+03   # ~u_L
   2000002     4.02337621E+03   # ~u_R
   1000003     4.02594585E+03   # ~s_L
   2000003     4.02258784E+03   # ~s_R
   1000004     4.02529474E+03   # ~c_L
   2000004     4.02337621E+03   # ~c_R
   1000005     9.68194919E+02   # ~b_1
   2000005     4.02541205E+03   # ~b_2
   1000006     9.45943267E+02   # ~t_1
   2000006     1.90081624E+03   # ~t_2
   1000011     4.00457774E+03   # ~e_L
   2000011     4.00344835E+03   # ~e_R
   1000012     4.00346500E+03   # ~nu_eL
   1000013     4.00457774E+03   # ~mu_L
   2000013     4.00344835E+03   # ~mu_R
   1000014     4.00346500E+03   # ~nu_muL
   1000015     4.00500760E+03   # ~tau_1
   2000015     4.00732776E+03   # ~tau_2
   1000016     4.00490261E+03   # ~nu_tauL
   1000021     1.97813384E+03   # ~g
   1000022     2.01387945E+02   # ~chi_10
   1000023    -2.70463959E+02   # ~chi_20
   1000025     2.79278695E+02   # ~chi_30
   1000035     2.05216559E+03   # ~chi_40
   1000024     2.67413176E+02   # ~chi_1+
   1000037     2.05232994E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.96956439E-01   # N_11
  1  2    -1.05295772E-02   # N_12
  1  3    -3.55319309E-01   # N_13
  1  4    -2.62881081E-01   # N_14
  2  1    -6.92345965E-02   # N_21
  2  2     2.54688748E-02   # N_22
  2  3    -7.00610795E-01   # N_23
  2  4     7.09719960E-01   # N_24
  3  1     4.36663262E-01   # N_31
  3  2     2.78524312E-02   # N_32
  3  3     6.18775305E-01   # N_33
  3  4     6.52431268E-01   # N_34
  4  1    -9.54967456E-04   # N_41
  4  2     9.99232058E-01   # N_42
  4  3    -3.13439717E-03   # N_43
  4  4    -3.90455771E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.43787141E-03   # U_11
  1  2     9.99990153E-01   # U_12
  2  1    -9.99990153E-01   # U_21
  2  2     4.43787141E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.52311836E-02   # V_11
  1  2    -9.98473593E-01   # V_12
  2  1    -9.98473593E-01   # V_21
  2  2    -5.52311836E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.89942378E-01   # cos(theta_t)
  1  2    -1.41471157E-01   # sin(theta_t)
  2  1     1.41471157E-01   # -sin(theta_t)
  2  2     9.89942378E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999653E-01   # cos(theta_b)
  1  2    -8.33066552E-04   # sin(theta_b)
  2  1     8.33066552E-04   # -sin(theta_b)
  2  2     9.99999653E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05395269E-01   # cos(theta_tau)
  1  2     7.08814161E-01   # sin(theta_tau)
  2  1    -7.08814161E-01   # -sin(theta_tau)
  2  2    -7.05395269E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00299287E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30166684E+03  # DRbar Higgs Parameters
         1    -2.59900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43565490E+02   # higgs               
         4     1.60881488E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30166684E+03  # The gauge couplings
     1     3.61997545E-01   # gprime(Q) DRbar
     2     6.36447009E-01   # g(Q) DRbar
     3     1.03008590E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30166684E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37092627E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30166684E+03  # The trilinear couplings
  1  1     5.02555586E-07   # A_d(Q) DRbar
  2  2     5.02602825E-07   # A_s(Q) DRbar
  3  3     9.00770134E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30166684E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09377860E-07   # A_mu(Q) DRbar
  3  3     1.10501940E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30166684E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68375486E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30166684E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82791826E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30166684E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03754375E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30166684E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58227259E+07   # M^2_Hd              
        22    -2.75770066E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.88485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40529022E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.95768210E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40070435E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40070435E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59929565E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59929565E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.24005823E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     6.07113097E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.31023800E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.01598634E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.06666257E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19187577E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.62477434E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.13028175E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.56707622E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.20525258E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.61765761E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.23321373E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.39440928E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.28735385E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.26796736E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.87489058E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.80146107E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.90556810E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66151347E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54251846E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79349975E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64938283E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.84902636E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63039433E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.55603306E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13520950E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.12251346E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.24462859E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.54671211E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.61357364E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78216988E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.22055828E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.32421761E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.06349877E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82080956E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.30440023E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62940181E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51670147E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60549152E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.47949209E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.65883429E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.05703921E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.80885589E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44368776E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78236702E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.84568532E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29493178E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.62206489E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82564598E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.42485127E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.66136135E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51889117E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53782447E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16891384E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.93816073E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.75831642E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.32767236E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85483156E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78216988E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.22055828E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.32421761E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.06349877E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82080956E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.30440023E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62940181E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51670147E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60549152E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.47949209E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.65883429E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.05703921E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.80885589E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44368776E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78236702E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.84568532E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29493178E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.62206489E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82564598E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.42485127E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.66136135E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51889117E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53782447E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16891384E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.93816073E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.75831642E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.32767236E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85483156E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15259003E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27124471E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.81330650E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.86705698E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77567585E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.99807356E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56519260E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07495494E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.05265226E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.77826163E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.89956014E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.98647849E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15259003E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27124471E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.81330650E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.86705698E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77567585E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.99807356E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56519260E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07495494E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.05265226E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.77826163E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.89956014E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.98647849E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10815990E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.90991711E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28319521E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.18434168E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39723495E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44652976E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80144127E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10742633E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.02745927E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40407377E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.55076117E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42118658E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10733171E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84945243E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15364873E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.37981466E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13141390E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.46306609E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77891035E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09287287E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54272551E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15364873E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.37981466E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13141390E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.46306609E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77891035E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09287287E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54272551E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48396096E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.24944832E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.93004135E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.23036055E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51763447E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.68989455E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02159129E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.72342885E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33451556E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33451556E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11151396E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11151396E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10794096E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.14910109E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.54471279E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.37680121E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.37461660E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.82448083E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.01141386E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.64721452E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.69148418E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.73700835E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.10145485E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.33808108E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17492358E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52398566E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17492358E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52398566E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45589919E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49417889E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49417889E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47264866E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98608543E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98608543E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98608519E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.30116500E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.30116500E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.30116500E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.30116500E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.10038976E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.10038976E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.10038976E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.10038976E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95604597E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95604597E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.11436353E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.89045166E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.22078844E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.14916890E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.48840291E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.14916890E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.48840291E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.44545281E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.39501911E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.39501911E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.38336026E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.83068278E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.83068278E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.83067792E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.17046765E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.51828254E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.17046765E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.51828254E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.48159761E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.48159761E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.68633411E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.95939982E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.95939982E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.95939988E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.74195221E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.74195221E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.74195221E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.74195221E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.80645972E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.80645972E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.80645972E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.80645972E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.20639028E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.20639028E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.14791724E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.38393085E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.56123904E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.82393965E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.62804718E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.62804718E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.80047139E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.23858986E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.99862663E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.72997226E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.72997226E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.73018126E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.73018126E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.09939849E-03   # h decays
#          BR         NDA      ID1       ID2
     5.86895378E-01    2           5        -5   # BR(h -> b       bb     )
     6.37955553E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25833874E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78594496E-04    2           3        -3   # BR(h -> s       sb     )
     2.07833332E-02    2           4        -4   # BR(h -> c       cb     )
     6.81392758E-02    2          21        21   # BR(h -> g       g      )
     2.37738761E-03    2          22        22   # BR(h -> gam     gam    )
     1.66501180E-03    2          22        23   # BR(h -> Z       gam    )
     2.26772577E-01    2          24       -24   # BR(h -> W+      W-     )
     2.88670526E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48166644E+01   # H decays
#          BR         NDA      ID1       ID2
     3.49306128E-01    2           5        -5   # BR(H -> b       bb     )
     6.04852316E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13861129E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52960845E-04    2           3        -3   # BR(H -> s       sb     )
     7.09596399E-08    2           4        -4   # BR(H -> c       cb     )
     7.11163330E-03    2           6        -6   # BR(H -> t       tb     )
     7.94178393E-07    2          21        21   # BR(H -> g       g      )
     2.80191171E-10    2          22        22   # BR(H -> gam     gam    )
     1.82612437E-09    2          23        22   # BR(H -> Z       gam    )
     1.95576999E-06    2          24       -24   # BR(H -> W+      W-     )
     9.77207524E-07    2          23        23   # BR(H -> Z       Z      )
     7.50474690E-06    2          25        25   # BR(H -> h       h      )
     4.29632351E-24    2          36        36   # BR(H -> A       A      )
    -2.25017238E-20    2          23        36   # BR(H -> Z       A      )
     1.84927683E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63299888E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63299888E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.98398028E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03779437E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.07594528E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.81306906E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.79149248E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.06786386E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.63331797E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.98822180E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.32265475E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.91242957E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.66260401E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.37922044E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.37922044E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.29798763E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48127792E+01   # A decays
#          BR         NDA      ID1       ID2
     3.49356650E-01    2           5        -5   # BR(A -> b       bb     )
     6.04898529E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13877301E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53021234E-04    2           3        -3   # BR(A -> s       sb     )
     7.14142332E-08    2           4        -4   # BR(A -> c       cb     )
     7.12485130E-03    2           6        -6   # BR(A -> t       tb     )
     1.46397672E-05    2          21        21   # BR(A -> g       g      )
     5.32747011E-08    2          22        22   # BR(A -> gam     gam    )
     1.60939565E-08    2          23        22   # BR(A -> Z       gam    )
     1.95175838E-06    2          23        25   # BR(A -> Z       h      )
     1.85308536E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63313660E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63313660E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.72846349E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29506005E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.88473262E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.48298647E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.44200464E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.07423677E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.06063788E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.08601312E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.21523642E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.49451450E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.49451450E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.48146176E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.58950718E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05038634E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13926838E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.57728153E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21193444E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49333887E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.56101122E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.95392953E-06    2          24        25   # BR(H+ -> W+      h      )
     3.22264673E-14    2          24        36   # BR(H+ -> W+      A      )
     6.77882871E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.90271201E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.01112892E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63522227E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.14770162E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60848429E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.25571342E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.52694603E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.86484043E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
