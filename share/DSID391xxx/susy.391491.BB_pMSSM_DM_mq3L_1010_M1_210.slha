#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14032515E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.59990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.00990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.96485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04159209E+01   # W+
        25     1.25549274E+02   # h
        35     4.00000679E+03   # H
        36     3.99999699E+03   # A
        37     4.00107306E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02861528E+03   # ~d_L
   2000001     4.02469090E+03   # ~d_R
   1000002     4.02796245E+03   # ~u_L
   2000002     4.02543332E+03   # ~u_R
   1000003     4.02861528E+03   # ~s_L
   2000003     4.02469090E+03   # ~s_R
   1000004     4.02796245E+03   # ~c_L
   2000004     4.02543332E+03   # ~c_R
   1000005     1.07345047E+03   # ~b_1
   2000005     4.02685192E+03   # ~b_2
   1000006     1.05517020E+03   # ~t_1
   2000006     1.97841697E+03   # ~t_2
   1000011     4.00513573E+03   # ~e_L
   2000011     4.00350352E+03   # ~e_R
   1000012     4.00402051E+03   # ~nu_eL
   1000013     4.00513573E+03   # ~mu_L
   2000013     4.00350352E+03   # ~mu_R
   1000014     4.00402051E+03   # ~nu_muL
   1000015     4.00494924E+03   # ~tau_1
   2000015     4.00729043E+03   # ~tau_2
   1000016     4.00521975E+03   # ~nu_tauL
   1000021     1.98412401E+03   # ~g
   1000022     2.01189115E+02   # ~chi_10
   1000023    -2.70774818E+02   # ~chi_20
   1000025     2.79428143E+02   # ~chi_30
   1000035     2.05144468E+03   # ~chi_40
   1000024     2.67660044E+02   # ~chi_1+
   1000037     2.05160968E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.97136940E-01   # N_11
  1  2    -1.05153999E-02   # N_12
  1  3    -3.55064648E-01   # N_13
  1  4    -2.62609660E-01   # N_14
  2  1    -6.92383059E-02   # N_21
  2  2     2.54602112E-02   # N_22
  2  3    -7.00612659E-01   # N_23
  2  4     7.09718069E-01   # N_24
  3  1     4.36291710E-01   # N_31
  3  2     2.78501033E-02   # N_32
  3  3     6.18919356E-01   # N_33
  3  4     6.52543291E-01   # N_34
  4  1    -9.54926590E-04   # N_41
  4  2     9.99232493E-01   # N_42
  4  3    -3.13527486E-03   # N_43
  4  4    -3.90343747E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.43910612E-03   # U_11
  1  2     9.99990147E-01   # U_12
  2  1    -9.99990147E-01   # U_21
  2  2     4.43910612E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.52152981E-02   # V_11
  1  2    -9.98474472E-01   # V_12
  2  1    -9.98474472E-01   # V_21
  2  2    -5.52152981E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.90777640E-01   # cos(theta_t)
  1  2    -1.35497853E-01   # sin(theta_t)
  2  1     1.35497853E-01   # -sin(theta_t)
  2  2     9.90777640E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999647E-01   # cos(theta_b)
  1  2    -8.40237987E-04   # sin(theta_b)
  2  1     8.40237987E-04   # -sin(theta_b)
  2  2     9.99999647E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05457567E-01   # cos(theta_tau)
  1  2     7.08752158E-01   # sin(theta_tau)
  2  1    -7.08752158E-01   # -sin(theta_tau)
  2  2    -7.05457567E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00292829E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.40325153E+03  # DRbar Higgs Parameters
         1    -2.59990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43458848E+02   # higgs               
         4     1.60648385E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.40325153E+03  # The gauge couplings
     1     3.62245012E-01   # gprime(Q) DRbar
     2     6.36535970E-01   # g(Q) DRbar
     3     1.02833868E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.40325153E+03  # The trilinear couplings
  1  1     1.60616583E-06   # A_u(Q) DRbar
  2  2     1.60618126E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.40325153E+03  # The trilinear couplings
  1  1     5.90296020E-07   # A_d(Q) DRbar
  2  2     5.90350767E-07   # A_s(Q) DRbar
  3  3     1.05559706E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.40325153E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.27748138E-07   # A_mu(Q) DRbar
  3  3     1.29065232E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.40325153E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65226642E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.40325153E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.81702615E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.40325153E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03764726E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.40325153E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58275625E+07   # M^2_Hd              
        22    -2.13808608E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.00990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.96485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40575333E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.31561354E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40277064E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40277064E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59722936E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59722936E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.44119571E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     5.94545361E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.35260555E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.01244913E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.04039996E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13331000E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.03759070E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23100563E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.04655029E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.40540481E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.52838723E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.11016996E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.17472301E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.47856184E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.21003841E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.81526174E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.76658521E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.92081146E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66030110E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54451203E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79146050E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64865625E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.87702828E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62632195E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.61237975E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13575891E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.97747839E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.05718680E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.51744027E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.36193810E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78277206E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.22604484E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.32157493E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.06369749E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83097455E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.30496932E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64971467E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51359653E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60509737E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.49124185E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.66495540E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.05756828E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.81822224E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44245375E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78297313E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.85122927E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29575326E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.62165841E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83581324E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.42909091E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68170508E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51578417E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53733606E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.17207831E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.95471805E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.75992900E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.35284527E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85449733E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78277206E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.22604484E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.32157493E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.06369749E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83097455E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.30496932E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64971467E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51359653E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60509737E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.49124185E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.66495540E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.05756828E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.81822224E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44245375E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78297313E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.85122927E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29575326E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.62165841E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83581324E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.42909091E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68170508E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51578417E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53733606E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.17207831E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.95471805E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.75992900E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.35284527E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85449733E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15631738E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27229572E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.85107430E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.86187637E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77549850E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.99763317E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56483326E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07783740E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.05591363E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.77863745E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.89629501E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.98860836E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15631738E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27229572E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.85107430E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.86187637E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77549850E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.99763317E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56483326E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07783740E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.05591363E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.77863745E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.89629501E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.98860836E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11100848E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.91182942E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.27945819E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.17423373E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39724223E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44105315E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80145385E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11016453E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.03084372E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.39952804E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54942972E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42076307E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10409473E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84860121E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15736718E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38072071E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13073111E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.45952244E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77874232E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.08870186E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54239040E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15736718E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38072071E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13073111E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.45952244E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77874232E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.08870186E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54239040E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48732340E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25046079E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.92971849E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.22749253E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51765538E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.68204430E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02163296E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.88913746E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33449489E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33449489E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11150691E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11150691E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10799639E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.71768884E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.40868242E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.19281157E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.23284640E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.52969623E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.80004855E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.33427731E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.29547729E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.44406330E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.23661813E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.34600575E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17467486E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52379159E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17467486E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52379159E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45641207E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49447937E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49447937E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47315584E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98692806E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98692806E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98692786E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.51045496E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.51045496E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.51045496E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.51045496E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.17015317E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.17015317E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.17015317E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.17015317E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.25406898E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.25406898E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.25539866E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.70387580E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.58252367E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.15971361E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50221599E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.15971361E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50221599E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.45972771E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.42747886E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.42747886E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.41598507E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.89567821E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.89567821E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.89567417E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.03371611E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.34100700E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.03371611E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.34100700E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.07573976E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.07573976E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.34602277E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.14834237E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.14834237E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.14834241E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.61846044E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.61846044E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.61846044E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.61846044E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.39482393E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.39482393E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.39482393E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.39482393E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.82866392E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.82866392E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.71660587E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.76893042E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.97253691E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.07927842E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.31364176E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.31364176E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.22635814E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.53286754E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.36633182E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.65801941E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.65801941E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.66308923E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.66308923E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04048304E-03   # h decays
#          BR         NDA      ID1       ID2
     5.93958228E-01    2           5        -5   # BR(h -> b       bb     )
     6.44962131E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28316096E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84186391E-04    2           3        -3   # BR(h -> s       sb     )
     2.10267352E-02    2           4        -4   # BR(h -> c       cb     )
     6.86866803E-02    2          21        21   # BR(h -> g       g      )
     2.37380666E-03    2          22        22   # BR(h -> gam     gam    )
     1.62481248E-03    2          22        23   # BR(h -> Z       gam    )
     2.19385646E-01    2          24       -24   # BR(h -> W+      W-     )
     2.77353765E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46551998E+01   # H decays
#          BR         NDA      ID1       ID2
     3.47361135E-01    2           5        -5   # BR(H -> b       bb     )
     6.06639296E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14492962E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53708191E-04    2           3        -3   # BR(H -> s       sb     )
     7.11674423E-08    2           4        -4   # BR(H -> c       cb     )
     7.13245943E-03    2           6        -6   # BR(H -> t       tb     )
     9.03195776E-07    2          21        21   # BR(H -> g       g      )
     2.12319976E-10    2          22        22   # BR(H -> gam     gam    )
     1.83127995E-09    2          23        22   # BR(H -> Z       gam    )
     1.92629073E-06    2          24       -24   # BR(H -> W+      W-     )
     9.62478016E-07    2          23        23   # BR(H -> Z       Z      )
     7.40312159E-06    2          25        25   # BR(H -> h       h      )
     2.92800245E-24    2          36        36   # BR(H -> A       A      )
     6.35605179E-20    2          23        36   # BR(H -> Z       A      )
     1.85351437E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63854095E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63854095E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.98794146E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.04064769E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.07770000E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.82315027E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.80077326E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.07218100E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.64937318E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.01573318E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.34713481E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.65326444E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.75029449E-06    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.29646961E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.29646961E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.25587864E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.46507756E+01   # A decays
#          BR         NDA      ID1       ID2
     3.47414960E-01    2           5        -5   # BR(A -> b       bb     )
     6.06691702E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14511322E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53771294E-04    2           3        -3   # BR(A -> s       sb     )
     7.16259341E-08    2           4        -4   # BR(A -> c       cb     )
     7.14597227E-03    2           6        -6   # BR(A -> t       tb     )
     1.46831640E-05    2          21        21   # BR(A -> g       g      )
     5.34375756E-08    2          22        22   # BR(A -> gam     gam    )
     1.61371575E-08    2          23        22   # BR(A -> Z       gam    )
     1.92239886E-06    2          23        25   # BR(A -> Z       h      )
     1.85739589E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63869530E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63869530E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.73197952E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29868609E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.89962725E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.49538103E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.44956394E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.07823206E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.06230731E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.11305642E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.23939599E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.39546803E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.39546803E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.46477563E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.55733542E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.06888442E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14580885E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.55669162E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21563913E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50096061E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.54113266E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.92473059E-06    2          24        25   # BR(H+ -> W+      h      )
     3.47430531E-14    2          24        36   # BR(H+ -> W+      A      )
     6.80364540E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.91395303E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.01428779E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.64094823E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.15644881E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61410652E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.26071401E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.39167654E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.68083875E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
