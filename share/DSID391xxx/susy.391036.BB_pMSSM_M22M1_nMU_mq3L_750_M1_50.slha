#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14980421E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03984110E+01   # W+
        25     1.25544462E+02   # h
        35     3.00007840E+03   # H
        36     2.99999994E+03   # A
        37     3.00110523E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03533977E+03   # ~d_L
   2000001     3.02998828E+03   # ~d_R
   1000002     3.03441679E+03   # ~u_L
   2000002     3.03191867E+03   # ~u_R
   1000003     3.03533977E+03   # ~s_L
   2000003     3.02998828E+03   # ~s_R
   1000004     3.03441679E+03   # ~c_L
   2000004     3.03191867E+03   # ~c_R
   1000005     8.42231324E+02   # ~b_1
   2000005     3.02861165E+03   # ~b_2
   1000006     8.39866569E+02   # ~t_1
   2000006     3.02274511E+03   # ~t_2
   1000011     3.00681648E+03   # ~e_L
   2000011     3.00122230E+03   # ~e_R
   1000012     3.00541093E+03   # ~nu_eL
   1000013     3.00681648E+03   # ~mu_L
   2000013     3.00122230E+03   # ~mu_R
   1000014     3.00541093E+03   # ~nu_muL
   1000015     2.98606269E+03   # ~tau_1
   2000015     3.02208622E+03   # ~tau_2
   1000016     3.00547137E+03   # ~nu_tauL
   1000021     2.34353415E+03   # ~g
   1000022     5.05751101E+01   # ~chi_10
   1000023     1.10514143E+02   # ~chi_20
   1000025    -2.99909142E+03   # ~chi_30
   1000035     2.99925656E+03   # ~chi_40
   1000024     1.10665657E+02   # ~chi_1+
   1000037     3.00011966E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890674E-01   # N_11
  1  2     1.91319104E-03   # N_12
  1  3    -1.46540187E-02   # N_13
  1  4     4.89099498E-04   # N_14
  2  1    -1.53047820E-03   # N_21
  2  2     9.99658881E-01   # N_22
  2  3     2.60689464E-02   # N_23
  2  4    -4.34894688E-04   # N_24
  3  1     1.00472421E-02   # N_31
  3  2    -1.81086121E-02   # N_32
  3  3     7.06792423E-01   # N_33
  3  4     7.07117813E-01   # N_34
  4  1    -1.07401278E-02   # N_41
  4  2     1.87226956E-02   # N_42
  4  3    -7.06788611E-01   # N_43
  4  4     7.07095446E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99321812E-01   # U_11
  1  2     3.68227667E-02   # U_12
  2  1    -3.68227667E-02   # U_21
  2  2     9.99321812E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999811E-01   # V_11
  1  2     6.15548163E-04   # V_12
  2  1     6.15548163E-04   # V_21
  2  2    -9.99999811E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98558618E-01   # cos(theta_t)
  1  2    -5.36720264E-02   # sin(theta_t)
  2  1     5.36720264E-02   # -sin(theta_t)
  2  2     9.98558618E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99705503E-01   # cos(theta_b)
  1  2    -2.42674117E-02   # sin(theta_b)
  2  1     2.42674117E-02   # -sin(theta_b)
  2  2     9.99705503E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06879708E-01   # cos(theta_tau)
  1  2     7.07333781E-01   # sin(theta_tau)
  2  1    -7.07333781E-01   # -sin(theta_tau)
  2  2    -7.06879708E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00172325E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.49804215E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44106431E+02   # higgs               
         4     1.05475832E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.49804215E+03  # The gauge couplings
     1     3.62105049E-01   # gprime(Q) DRbar
     2     6.41582172E-01   # g(Q) DRbar
     3     1.02637828E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.49804215E+03  # The trilinear couplings
  1  1     2.13031523E-06   # A_u(Q) DRbar
  2  2     2.13034867E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.49804215E+03  # The trilinear couplings
  1  1     7.43044400E-07   # A_d(Q) DRbar
  2  2     7.43136344E-07   # A_s(Q) DRbar
  3  3     1.56489092E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.49804215E+03  # The trilinear couplings
  1  1     3.54782429E-07   # A_e(Q) DRbar
  2  2     3.54800351E-07   # A_mu(Q) DRbar
  3  3     3.59944000E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.49804215E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53042663E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.49804215E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.96137143E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.49804215E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03826711E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.49804215E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.98751303E+04   # M^2_Hd              
        22    -9.06965487E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42890928E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.19974710E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48000077E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48000077E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51999923E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51999923E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.01088634E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.20183655E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.11834868E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.76146767E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.17153495E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.64458563E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.45060262E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.93437303E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.92259844E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.92354294E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68494984E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62344807E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.19532335E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.90461209E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.25501021E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.47041568E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.40408330E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.55616770E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.99790246E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.83258745E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.43339013E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.33616609E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.77170729E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.28521640E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.52910060E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.08227351E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.57913156E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.11538156E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.99383455E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65520643E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.71528007E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.85356676E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.31456040E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.38621663E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.97029427E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18457967E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59666296E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.73483768E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.70794304E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.85333000E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40333312E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12040620E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.75261816E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65646577E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.33648946E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.62413682E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.30877154E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.78041964E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.97723283E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66784420E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.55102620E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.06455187E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.20896024E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.49796967E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54489627E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.11538156E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.99383455E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65520643E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.71528007E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.85356676E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.31456040E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.38621663E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.97029427E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18457967E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59666296E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.73483768E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.70794304E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.85333000E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40333312E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12040620E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.75261816E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65646577E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.33648946E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.62413682E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.30877154E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.78041964E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.97723283E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66784420E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.55102620E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.06455187E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.20896024E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.49796967E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54489627E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07170429E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.68944634E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00825083E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.23188624E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.22253597E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02280434E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.62335626E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56454056E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997662E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.33784861E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.03567113E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.97966110E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07170429E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.68944634E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00825083E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.23188624E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.22253597E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02280434E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.62335626E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56454056E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997662E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.33784861E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.03567113E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.97966110E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.84693818E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43930078E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18569061E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37500861E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78737061E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.51976572E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15854701E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.18896393E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.00686596E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32135083E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.16855436E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07204595E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.55366155E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01699451E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.01575676E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.10728398E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02763928E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.83437401E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07204595E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.55366155E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01699451E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.01575676E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.10728398E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02763928E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.83437401E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07246791E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.55286400E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01674290E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.07327618E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.16815918E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02796335E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.28312734E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.51045844E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.39914305E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.73458213E-08    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.39914305E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.73458213E-08    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.06877632E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.78201861E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.06877632E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.78201861E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.06416079E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.31790555E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.41592996E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.48039408E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.54175262E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.34545412E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.99573490E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.18423600E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.51128917E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.44680141E-10   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.13960891E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     4.41035217E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     5.48246668E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     4.41035217E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     5.48246668E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     7.73592675E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     4.86236052E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.86236052E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     4.85115490E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.59600375E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.59600375E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.59782656E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     9.36352688E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.07040223E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.26798125E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.16581820E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.16581820E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.99034654E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.72744353E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.28885121E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.28885121E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.96318698E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.96318698E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.18542735E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.18542735E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.30501173E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.42734692E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.07787583E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.20463373E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.20463373E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.18224936E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.88615181E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.22426712E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.22426712E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.00814860E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.00814860E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.05941002E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.05941002E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.71130388E-03   # h decays
#          BR         NDA      ID1       ID2
     5.57306280E-01    2           5        -5   # BR(h -> b       bb     )
     7.01803028E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.48437752E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.26865311E-04    2           3        -3   # BR(h -> s       sb     )
     2.28910516E-02    2           4        -4   # BR(h -> c       cb     )
     7.49729238E-02    2          21        21   # BR(h -> g       g      )
     2.57878523E-03    2          22        22   # BR(h -> gam     gam    )
     1.76706849E-03    2          22        23   # BR(h -> Z       gam    )
     2.39325548E-01    2          24       -24   # BR(h -> W+      W-     )
     3.01774502E-02    2          23        23   # BR(h -> Z       Z      )
     2.52864012E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     3.73999075E+01   # H decays
#          BR         NDA      ID1       ID2
     9.00991223E-01    2           5        -5   # BR(H -> b       bb     )
     6.64910969E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.35096629E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.88699457E-04    2           3        -3   # BR(H -> s       sb     )
     8.09301176E-08    2           4        -4   # BR(H -> c       cb     )
     8.07329727E-03    2           6        -6   # BR(H -> t       tb     )
     8.48040745E-06    2          21        21   # BR(H -> g       g      )
     5.95173518E-08    2          22        22   # BR(H -> gam     gam    )
     3.30207629E-09    2          23        22   # BR(H -> Z       gam    )
     8.16662450E-07    2          24       -24   # BR(H -> W+      W-     )
     4.07827640E-07    2          23        23   # BR(H -> Z       Z      )
     5.43731649E-06    2          25        25   # BR(H -> h       h      )
     1.68458030E-24    2          36        36   # BR(H -> A       A      )
     1.00137864E-19    2          23        36   # BR(H -> Z       A      )
     9.11527272E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.12366166E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.57308962E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.74948508E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.22148068E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.47288090E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.65724360E+01   # A decays
#          BR         NDA      ID1       ID2
     9.21399355E-01    2           5        -5   # BR(A -> b       bb     )
     6.79942120E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.40410949E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.95274567E-04    2           3        -3   # BR(A -> s       sb     )
     8.33222976E-08    2           4        -4   # BR(A -> c       cb     )
     8.30735932E-03    2           6        -6   # BR(A -> t       tb     )
     2.44643782E-05    2          21        21   # BR(A -> g       g      )
     5.01991776E-08    2          22        22   # BR(A -> gam     gam    )
     2.40883961E-08    2          23        22   # BR(A -> Z       gam    )
     8.31959543E-07    2          23        25   # BR(A -> Z       h      )
     9.40371627E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.24996720E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.71770832E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.83292045E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.97975318E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46885811E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.25071464E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.21010024E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.40068033E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.29881860E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.67208752E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.24194569E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.65864414E-07    2          24        25   # BR(H+ -> W+      h      )
     5.45372407E-14    2          24        36   # BR(H+ -> W+      A      )
     5.21303464E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.22036109E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07967478E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
