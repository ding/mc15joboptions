#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11983734E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.25990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.09990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04194718E+01   # W+
        25     1.26125115E+02   # h
        35     4.00001131E+03   # H
        36     3.99999737E+03   # A
        37     4.00099347E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02294918E+03   # ~d_L
   2000001     4.02017445E+03   # ~d_R
   1000002     4.02229838E+03   # ~u_L
   2000002     4.02118148E+03   # ~u_R
   1000003     4.02294918E+03   # ~s_L
   2000003     4.02017445E+03   # ~s_R
   1000004     4.02229838E+03   # ~c_L
   2000004     4.02118148E+03   # ~c_R
   1000005     7.76892589E+02   # ~b_1
   2000005     4.02386824E+03   # ~b_2
   1000006     7.58913463E+02   # ~t_1
   2000006     2.07051183E+03   # ~t_2
   1000011     4.00403274E+03   # ~e_L
   2000011     4.00305350E+03   # ~e_R
   1000012     4.00292205E+03   # ~nu_eL
   1000013     4.00403274E+03   # ~mu_L
   2000013     4.00305350E+03   # ~mu_R
   1000014     4.00292205E+03   # ~nu_muL
   1000015     4.00371871E+03   # ~tau_1
   2000015     4.00840044E+03   # ~tau_2
   1000016     4.00460241E+03   # ~nu_tauL
   1000021     1.97460640E+03   # ~g
   1000022     4.95751958E+02   # ~chi_10
   1000023    -5.37339903E+02   # ~chi_20
   1000025     5.56124642E+02   # ~chi_30
   1000035     2.05339158E+03   # ~chi_40
   1000024     5.35342427E+02   # ~chi_1+
   1000037     2.05355587E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.79840236E-01   # N_11
  1  2    -2.04885273E-02   # N_12
  1  3    -4.60616112E-01   # N_13
  1  4    -4.23393699E-01   # N_14
  2  1    -3.15189851E-02   # N_21
  2  2     2.27554332E-02   # N_22
  2  3    -7.05151999E-01   # N_23
  2  4     7.07989691E-01   # N_24
  3  1     6.25183440E-01   # N_31
  3  2     2.86133669E-02   # N_32
  3  3     5.38995025E-01   # N_33
  3  4     5.63747555E-01   # N_34
  4  1    -1.19464654E-03   # N_41
  4  2     9.99121457E-01   # N_42
  4  3    -8.82151905E-03   # N_43
  4  4    -4.09520192E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.24817152E-02   # U_11
  1  2     9.99922100E-01   # U_12
  2  1    -9.99922100E-01   # U_21
  2  2     1.24817152E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.79231348E-02   # V_11
  1  2    -9.98321046E-01   # V_12
  2  1    -9.98321046E-01   # V_21
  2  2    -5.79231348E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94373443E-01   # cos(theta_t)
  1  2    -1.05931373E-01   # sin(theta_t)
  2  1     1.05931373E-01   # -sin(theta_t)
  2  2     9.94373443E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998571E-01   # cos(theta_b)
  1  2    -1.69056143E-03   # sin(theta_b)
  2  1     1.69056143E-03   # -sin(theta_b)
  2  2     9.99998571E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06263879E-01   # cos(theta_tau)
  1  2     7.07948680E-01   # sin(theta_tau)
  2  1    -7.07948680E-01   # -sin(theta_tau)
  2  2    -7.06263879E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00248062E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.19837337E+03  # DRbar Higgs Parameters
         1    -5.25990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43865761E+02   # higgs               
         4     1.62612474E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.19837337E+03  # The gauge couplings
     1     3.61569235E-01   # gprime(Q) DRbar
     2     6.35633662E-01   # g(Q) DRbar
     3     1.03206376E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.19837337E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14384983E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.19837337E+03  # The trilinear couplings
  1  1     4.22259681E-07   # A_d(Q) DRbar
  2  2     4.22298824E-07   # A_s(Q) DRbar
  3  3     7.55613110E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.19837337E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.44121229E-08   # A_mu(Q) DRbar
  3  3     9.53664590E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.19837337E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69725815E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.19837337E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.89142168E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.19837337E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02757577E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.19837337E+03  # The soft SUSY breaking masses at the scale Q
         1     5.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56111351E+07   # M^2_Hd              
        22    -2.73857342E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.09989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40161573E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.03498381E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43424918E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43424918E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56575082E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56575082E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.96055040E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.81153241E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.61573611E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.95622134E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.61651014E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20991335E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.07166247E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.08184474E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.73150559E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.14172958E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.08334225E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.72753130E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.19905394E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.46952343E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.43875021E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.23823960E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.03855937E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.79367811E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.39295229E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.67040389E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.56867728E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82035642E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.56760344E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.62379450E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64708202E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.21400883E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12493331E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.10639234E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.28737404E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.19105314E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.15808325E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78591491E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.37374640E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.37118895E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.81919797E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78381618E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.65298717E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55557158E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52824167E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60880194E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.28704710E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.34130480E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.09612873E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.36644582E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.46114785E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78611658E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.39992258E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.95315277E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.79438318E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78948087E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.62512398E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58889766E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53041071E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54291238E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.56623559E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.39197132E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.46261154E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.13754173E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85957222E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78591491E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.37374640E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.37118895E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.81919797E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78381618E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.65298717E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55557158E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52824167E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60880194E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.28704710E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.34130480E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.09612873E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.36644582E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.46114785E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78611658E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.39992258E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.95315277E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.79438318E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78948087E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.62512398E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58889766E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53041071E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54291238E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.56623559E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.39197132E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.46261154E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.13754173E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85957222E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12739239E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.93136196E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.15582159E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.30652265E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78621773E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.54646621E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58833176E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.01252441E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.10069984E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.91161670E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.88938053E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.01790276E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12739239E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.93136196E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.15582159E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.30652265E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78621773E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.54646621E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58833176E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.01252441E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.10069984E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.91161670E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.88938053E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.01790276E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05182642E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.18675263E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.51957512E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.55556692E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41257992E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.59910003E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.83323302E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04139105E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.20404684E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.86404432E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.36899883E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45121926E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.78695396E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.91063524E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12845059E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07382888E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     8.20775974E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.28881912E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79098267E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.32827978E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56481598E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12845059E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07382888E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     8.20775974E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.28881912E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79098267E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.32827978E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56481598E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44725131E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.74956093E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.45206499E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.80188420E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53545544E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.46563853E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.05538413E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.70766976E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33698297E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33698297E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11234201E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11234201E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10135004E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.78537761E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.74023308E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.59554554E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.77336430E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.16822812E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.60271152E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.42805278E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.66985713E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.83716117E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.80451701E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18622276E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53870434E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18622276E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53870434E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37004780E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52834241E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52834241E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48520813E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.05363494E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.05363494E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.05363480E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.56931017E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.56931017E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.56931017E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.56931017E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.56437321E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.56437321E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.56437321E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.56437321E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.18687642E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.18687642E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.07179532E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.02029918E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.11714031E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.66103734E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     5.96372466E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.66103734E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     5.96372466E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.81584994E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.30397103E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.30397103E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.31441792E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.76568165E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.76568165E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.76565918E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.00226577E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.59728721E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.00226577E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.59728721E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.40867933E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.95601619E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.95601619E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.66086684E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.19057643E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.19057643E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.19057645E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.13470955E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.13470955E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.13470955E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.13470955E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.78231872E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.78231872E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.78231872E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.78231872E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.64846540E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.64846540E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.78402056E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.06955748E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.78197676E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.28504125E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.60287324E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.60287324E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49230150E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.96379650E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.19765897E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.83138973E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.83138973E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.83693641E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.83693641E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.07631695E-03   # h decays
#          BR         NDA      ID1       ID2
     5.80588503E-01    2           5        -5   # BR(h -> b       bb     )
     6.42116791E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27306365E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81617303E-04    2           3        -3   # BR(h -> s       sb     )
     2.09188260E-02    2           4        -4   # BR(h -> c       cb     )
     6.86777942E-02    2          21        21   # BR(h -> g       g      )
     2.40377409E-03    2          22        22   # BR(h -> gam     gam    )
     1.69412998E-03    2          22        23   # BR(h -> Z       gam    )
     2.31288128E-01    2          24       -24   # BR(h -> W+      W-     )
     2.95082419E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.59098505E+01   # H decays
#          BR         NDA      ID1       ID2
     3.71697913E-01    2           5        -5   # BR(H -> b       bb     )
     5.93026881E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.09679941E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.48015184E-04    2           3        -3   # BR(H -> s       sb     )
     6.95580310E-08    2           4        -4   # BR(H -> c       cb     )
     6.97116300E-03    2           6        -6   # BR(H -> t       tb     )
     7.13570730E-07    2          21        21   # BR(H -> g       g      )
     5.18558346E-09    2          22        22   # BR(H -> gam     gam    )
     1.79483766E-09    2          23        22   # BR(H -> Z       gam    )
     1.65273875E-06    2          24       -24   # BR(H -> W+      W-     )
     8.25796972E-07    2          23        23   # BR(H -> Z       Z      )
     6.72979409E-06    2          25        25   # BR(H -> h       h      )
    -5.09333092E-24    2          36        36   # BR(H -> A       A      )
     5.33672548E-22    2          23        36   # BR(H -> Z       A      )
     1.85724306E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.50437875E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.50437875E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.44581004E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.80866676E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.61298095E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.19347559E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.76597906E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.90460882E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.50271016E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.20967578E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.97870646E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.14125014E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.30298159E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.30298159E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.43581790E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.59128208E+01   # A decays
#          BR         NDA      ID1       ID2
     3.71703720E-01    2           5        -5   # BR(A -> b       bb     )
     5.92997720E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.09669466E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.48043275E-04    2           3        -3   # BR(A -> s       sb     )
     7.00092237E-08    2           4        -4   # BR(A -> c       cb     )
     6.98467640E-03    2           6        -6   # BR(A -> t       tb     )
     1.43517400E-05    2          21        21   # BR(A -> g       g      )
     6.91705043E-08    2          22        22   # BR(A -> gam     gam    )
     1.57826694E-08    2          23        22   # BR(A -> Z       gam    )
     1.64912873E-06    2          23        25   # BR(A -> Z       h      )
     1.91616954E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.50391711E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.50391711E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.16766893E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.01916918E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.41764430E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.56483921E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.31998386E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.47333290E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.63824881E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.80076631E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.81276253E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.36319201E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.36319201E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.62299360E+01   # H+ decays
#          BR         NDA      ID1       ID2
     6.00218809E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.89800279E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08538929E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.84139716E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18141332E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.43054711E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.81698538E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.64162771E-06    2          24        25   # BR(H+ -> W+      h      )
     2.29500382E-14    2          24        36   # BR(H+ -> W+      A      )
     4.59939728E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.51674403E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.47696526E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.49775800E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.35687820E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.49222565E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.76498649E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.83851678E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.65926677E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
