#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13998643E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.77990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.05990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.86485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04178820E+01   # W+
        25     1.25654183E+02   # h
        35     4.00000870E+03   # H
        36     3.99999739E+03   # A
        37     4.00106041E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02852333E+03   # ~d_L
   2000001     4.02462594E+03   # ~d_R
   1000002     4.02787237E+03   # ~u_L
   2000002     4.02530730E+03   # ~u_R
   1000003     4.02852333E+03   # ~s_L
   2000003     4.02462594E+03   # ~s_R
   1000004     4.02787237E+03   # ~c_L
   2000004     4.02530730E+03   # ~c_R
   1000005     1.12182445E+03   # ~b_1
   2000005     4.02683421E+03   # ~b_2
   1000006     1.09891708E+03   # ~t_1
   2000006     1.88191553E+03   # ~t_2
   1000011     4.00507080E+03   # ~e_L
   2000011     4.00346422E+03   # ~e_R
   1000012     4.00395843E+03   # ~nu_eL
   1000013     4.00507080E+03   # ~mu_L
   2000013     4.00346422E+03   # ~mu_R
   1000014     4.00395843E+03   # ~nu_muL
   1000015     4.00393658E+03   # ~tau_1
   2000015     4.00816784E+03   # ~tau_2
   1000016     4.00514767E+03   # ~nu_tauL
   1000021     1.98339006E+03   # ~g
   1000022     4.45068318E+02   # ~chi_10
   1000023    -4.90151705E+02   # ~chi_20
   1000025     5.07520516E+02   # ~chi_30
   1000035     2.05150731E+03   # ~chi_40
   1000024     4.87965848E+02   # ~chi_1+
   1000037     2.05167117E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.89452159E-01   # N_11
  1  2    -1.93014757E-02   # N_12
  1  3    -4.54094429E-01   # N_13
  1  4    -4.12542108E-01   # N_14
  2  1    -3.48099958E-02   # N_21
  2  2     2.31737615E-02   # N_22
  2  3    -7.04838523E-01   # N_23
  2  4     7.08134096E-01   # N_24
  3  1     6.12823177E-01   # N_31
  3  2     2.80444859E-02   # N_32
  3  3     5.44923219E-01   # N_33
  3  4     5.71594215E-01   # N_34
  4  1    -1.14301047E-03   # N_41
  4  2     9.99151608E-01   # N_42
  4  3    -7.71957356E-03   # N_43
  4  4    -4.04371746E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.09231893E-02   # U_11
  1  2     9.99940340E-01   # U_12
  2  1    -9.99940340E-01   # U_21
  2  2     1.09231893E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.71957549E-02   # V_11
  1  2    -9.98362983E-01   # V_12
  2  1    -9.98362983E-01   # V_21
  2  2    -5.71957549E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.86762669E-01   # cos(theta_t)
  1  2    -1.62171006E-01   # sin(theta_t)
  2  1     1.62171006E-01   # -sin(theta_t)
  2  2     9.86762669E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998747E-01   # cos(theta_b)
  1  2    -1.58303456E-03   # sin(theta_b)
  2  1     1.58303456E-03   # -sin(theta_b)
  2  2     9.99998747E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06255596E-01   # cos(theta_tau)
  1  2     7.07956943E-01   # sin(theta_tau)
  2  1    -7.07956943E-01   # -sin(theta_tau)
  2  2    -7.06255596E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00248274E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39986433E+03  # DRbar Higgs Parameters
         1    -4.77990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43529865E+02   # higgs               
         4     1.60822498E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39986433E+03  # The gauge couplings
     1     3.62119113E-01   # gprime(Q) DRbar
     2     6.35828961E-01   # g(Q) DRbar
     3     1.02835689E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39986433E+03  # The trilinear couplings
  1  1     1.59785587E-06   # A_u(Q) DRbar
  2  2     1.59787087E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39986433E+03  # The trilinear couplings
  1  1     5.90971641E-07   # A_d(Q) DRbar
  2  2     5.91025746E-07   # A_s(Q) DRbar
  3  3     1.05429019E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39986433E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.30614070E-07   # A_mu(Q) DRbar
  3  3     1.31953171E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39986433E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65356615E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39986433E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86463202E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39986433E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03014156E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39986433E+03  # The soft SUSY breaking masses at the scale Q
         1     4.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56684313E+07   # M^2_Hd              
        22    -1.74988325E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.05990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.86485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40254661E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.01978682E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37560909E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37560909E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62439091E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62439091E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.09275419E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.55042716E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.18950866E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.13900609E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.12105810E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.03306037E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.55774880E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16170220E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.10662863E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.26516264E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.59975399E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.20909163E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.29785180E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.14100990E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.09761128E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.05659426E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.08966264E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.87561318E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65825161E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.58611814E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83269163E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.58840491E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.09697103E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67493857E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.16556552E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12028679E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.43733873E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.34556216E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.56582812E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     8.65337305E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77757109E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.46200205E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.34123970E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.77483383E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83276732E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.58358212E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65340304E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51319690E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59868623E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.42443510E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.62393046E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.04860388E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.05062086E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45203330E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77777253E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.45442821E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.56797575E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.63768212E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83829452E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.03662240E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68658834E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51536929E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53239642E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.92981961E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.72730388E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.34208336E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.05603212E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85710813E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77757109E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.46200205E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.34123970E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.77483383E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83276732E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.58358212E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65340304E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51319690E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59868623E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.42443510E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.62393046E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.04860388E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.05062086E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45203330E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77777253E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.45442821E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.56797575E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.63768212E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83829452E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.03662240E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68658834E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51536929E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53239642E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.92981961E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.72730388E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.34208336E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.05603212E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85710813E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13890403E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.26791174E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.58278517E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.06278432E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78341862E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.18836985E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58226758E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03174331E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.25000047E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.20870342E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.73790520E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.30384289E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13890403E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.26791174E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.58278517E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.06278432E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78341862E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.18836985E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58226758E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03174331E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.25000047E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.20870342E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.73790520E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.30384289E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06964873E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.25311792E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49860789E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.50651426E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40879999E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56283718E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82542333E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06056530E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.27113221E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.91663229E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.31410253E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44347134E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.84749909E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89488078E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13995556E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09984479E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.19910130E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.11461820E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78787005E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.25616887E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55906255E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13995556E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09984479E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.19910130E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.11461820E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78787005E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.25616887E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55906255E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46071379E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.98216278E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     8.34910724E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.64203524E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53129574E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.50449868E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04748548E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.83846709E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33643257E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33643257E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11215861E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11215861E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10281763E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.47450752E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.37454096E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.25338709E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.17424767E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.76130105E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.65800271E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.53666205E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.87184336E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.67177067E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.00322849E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.76729284E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18375397E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53578714E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18375397E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53578714E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38770513E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52331812E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52331812E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48514024E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04417611E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04417611E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04417601E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.78536126E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.78536126E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.78536126E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.78536126E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.28454444E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.28454444E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.28454444E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.28454444E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36758261E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36758261E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     6.51346434E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.20128867E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.57184227E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.03597355E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.32952756E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.03597355E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.32952756E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.27068930E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.93707353E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.93707353E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.95158547E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.15416719E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.15416719E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.15413926E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.71415756E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.22397864E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.71415756E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.22397864E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.06130797E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.10240603E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.10240603E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.80783304E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.02001905E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.02001905E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.02001906E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.05778402E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.05778402E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.05778402E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.05778402E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.52590404E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.52590404E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.52590404E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.52590404E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.38544004E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.38544004E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.47258450E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.31464450E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.99898157E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.83463378E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.53754500E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.53754500E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.77431114E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.73293861E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.14134984E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.63757402E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.63757402E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.63798751E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.63798751E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02511266E-03   # h decays
#          BR         NDA      ID1       ID2
     5.89417228E-01    2           5        -5   # BR(h -> b       bb     )
     6.47851975E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29338639E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86276972E-04    2           3        -3   # BR(h -> s       sb     )
     2.11212340E-02    2           4        -4   # BR(h -> c       cb     )
     6.90436263E-02    2          21        21   # BR(h -> g       g      )
     2.39287790E-03    2          22        22   # BR(h -> gam     gam    )
     1.64628632E-03    2          22        23   # BR(h -> Z       gam    )
     2.22673712E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82042232E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.53490846E+01   # H decays
#          BR         NDA      ID1       ID2
     3.63944884E-01    2           5        -5   # BR(H -> b       bb     )
     5.99034705E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11804162E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50527792E-04    2           3        -3   # BR(H -> s       sb     )
     7.02627735E-08    2           4        -4   # BR(H -> c       cb     )
     7.04179282E-03    2           6        -6   # BR(H -> t       tb     )
     5.27274554E-07    2          21        21   # BR(H -> g       g      )
     6.83159650E-09    2          22        22   # BR(H -> gam     gam    )
     1.81235392E-09    2          23        22   # BR(H -> Z       gam    )
     1.67054632E-06    2          24       -24   # BR(H -> W+      W-     )
     8.34694507E-07    2          23        23   # BR(H -> Z       Z      )
     6.76427712E-06    2          25        25   # BR(H -> h       h      )
    -1.62616224E-24    2          36        36   # BR(H -> A       A      )
     5.18482317E-20    2          23        36   # BR(H -> Z       A      )
     1.86065187E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.54258624E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.54258624E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.48920008E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.36211486E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.63465597E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.25879100E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.44728374E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.95652079E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.49305891E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.26778843E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.25648978E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.72275382E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.05087555E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     9.34161469E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     9.34161469E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.28599532E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.53440225E+01   # A decays
#          BR         NDA      ID1       ID2
     3.64003880E-01    2           5        -5   # BR(A -> b       bb     )
     5.99092258E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11824345E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50592542E-04    2           3        -3   # BR(A -> s       sb     )
     7.07287440E-08    2           4        -4   # BR(A -> c       cb     )
     7.05646145E-03    2           6        -6   # BR(A -> t       tb     )
     1.44992401E-05    2          21        21   # BR(A -> g       g      )
     6.72218066E-08    2          22        22   # BR(A -> gam     gam    )
     1.59404633E-08    2          23        22   # BR(A -> Z       gam    )
     1.66717872E-06    2          23        25   # BR(A -> Z       h      )
     1.90452552E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.54246281E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.54246281E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.18345024E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.67595410E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.41619536E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67788088E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.85476535E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.40895279E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.64520722E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.10882187E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.95283217E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.03994214E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.03994214E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.55611038E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.86146290E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.96910150E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11052805E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.75133308E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19565240E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45984148E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.72991923E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.66257093E-06    2          24        25   # BR(H+ -> W+      h      )
     3.21490359E-14    2          24        36   # BR(H+ -> W+      A      )
     4.84015899E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.97641557E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.44658225E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.53874526E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.35471214E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.53170395E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.19622379E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     8.87343460E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.98777090E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
