#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13998419E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.19990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.05990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.86485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04180997E+01   # W+
        25     1.25653710E+02   # h
        35     4.00000816E+03   # H
        36     3.99999749E+03   # A
        37     4.00105636E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02852077E+03   # ~d_L
   2000001     4.02462261E+03   # ~d_R
   1000002     4.02786991E+03   # ~u_L
   2000002     4.02529864E+03   # ~u_R
   1000003     4.02852077E+03   # ~s_L
   2000003     4.02462261E+03   # ~s_R
   1000004     4.02786991E+03   # ~c_L
   2000004     4.02529864E+03   # ~c_R
   1000005     1.12238264E+03   # ~b_1
   2000005     4.02683533E+03   # ~b_2
   1000006     1.09946385E+03   # ~t_1
   2000006     1.88213724E+03   # ~t_2
   1000011     4.00506507E+03   # ~e_L
   2000011     4.00344175E+03   # ~e_R
   1000012     4.00395295E+03   # ~nu_eL
   1000013     4.00506507E+03   # ~mu_L
   2000013     4.00344175E+03   # ~mu_R
   1000014     4.00395295E+03   # ~nu_muL
   1000015     4.00373357E+03   # ~tau_1
   2000015     4.00833197E+03   # ~tau_2
   1000016     4.00513864E+03   # ~nu_tauL
   1000021     1.98338845E+03   # ~g
   1000022     4.92522818E+02   # ~chi_10
   1000023    -5.32342604E+02   # ~chi_20
   1000025     5.52765618E+02   # ~chi_30
   1000035     2.05149646E+03   # ~chi_40
   1000024     5.30215522E+02   # ~chi_1+
   1000037     2.05166027E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.48050620E-01   # N_11
  1  2    -2.17516512E-02   # N_12
  1  3    -4.86754236E-01   # N_13
  1  4    -4.50574577E-01   # N_14
  2  1    -3.17074992E-02   # N_21
  2  2     2.27835891E-02   # N_22
  2  3    -7.05127015E-01   # N_23
  2  4     7.08005251E-01   # N_24
  3  1     6.62882707E-01   # N_31
  3  2     2.74325222E-02   # N_32
  3  3     5.15549212E-01   # N_33
  3  4     5.42257304E-01   # N_34
  4  1    -1.19183798E-03   # N_41
  4  2     9.99127234E-01   # N_42
  4  3    -8.67276873E-03   # N_43
  4  4    -4.08427735E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.22712632E-02   # U_11
  1  2     9.99924705E-01   # U_12
  2  1    -9.99924705E-01   # U_21
  2  2     1.22712632E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.77684550E-02   # V_11
  1  2    -9.98330008E-01   # V_12
  2  1    -9.98330008E-01   # V_21
  2  2    -5.77684550E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.86743640E-01   # cos(theta_t)
  1  2    -1.62286749E-01   # sin(theta_t)
  2  1     1.62286749E-01   # -sin(theta_t)
  2  2     9.86743640E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998507E-01   # cos(theta_b)
  1  2    -1.72800398E-03   # sin(theta_b)
  2  1     1.72800398E-03   # -sin(theta_b)
  2  2     9.99998507E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06329619E-01   # cos(theta_tau)
  1  2     7.07883090E-01   # sin(theta_tau)
  2  1    -7.07883090E-01   # -sin(theta_tau)
  2  2    -7.06329619E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00239867E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39984194E+03  # DRbar Higgs Parameters
         1    -5.19990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43544805E+02   # higgs               
         4     1.60894505E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39984194E+03  # The gauge couplings
     1     3.62101788E-01   # gprime(Q) DRbar
     2     6.35735959E-01   # g(Q) DRbar
     3     1.02835624E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39984194E+03  # The trilinear couplings
  1  1     1.59785141E-06   # A_u(Q) DRbar
  2  2     1.59786644E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39984194E+03  # The trilinear couplings
  1  1     5.91826722E-07   # A_d(Q) DRbar
  2  2     5.91880921E-07   # A_s(Q) DRbar
  3  3     1.05513066E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39984194E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.31250377E-07   # A_mu(Q) DRbar
  3  3     1.32594305E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39984194E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65293186E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39984194E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87335132E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39984194E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02880038E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39984194E+03  # The soft SUSY breaking masses at the scale Q
         1     5.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56270910E+07   # M^2_Hd              
        22    -2.17087949E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.05990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.86485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40212683E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.01625303E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37546718E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37546718E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62453282E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62453282E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.92043860E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.88941982E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.15642260E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.82039749E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.13376009E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.02002609E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.96553116E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.14633421E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.48526898E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.23515319E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.62138184E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.22474587E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.32730487E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.04182363E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.42395749E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.11705727E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.82847971E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.86305055E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65855861E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.61125184E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83471238E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.55446637E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.62421408E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67639845E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.21545014E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.11856211E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.90294782E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.00611546E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.66354581E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.03977215E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77745875E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.23354443E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.38443674E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.98829189E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83136202E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.66477065E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65060929E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51368129E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59813130E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.05882358E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.46821177E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.38347805E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.40520188E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45522258E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77766081E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.28742514E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.03995345E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.19749876E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83706563E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.55626709E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68411780E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51585028E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53225115E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.97440684E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.42557000E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.21375285E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14818833E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85797573E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77745875E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.23354443E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.38443674E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.98829189E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83136202E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.66477065E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65060929E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51368129E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59813130E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.05882358E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.46821177E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.38347805E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.40520188E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45522258E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77766081E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.28742514E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.03995345E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.19749876E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83706563E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.55626709E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68411780E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51585028E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53225115E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.97440684E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.42557000E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.21375285E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14818833E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85797573E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13495931E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.14424195E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10587819E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.10641725E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78582550E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.49307608E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58750492E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.01875465E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.61547807E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.00369142E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.37447703E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.99146777E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13495931E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.14424195E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10587819E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.10641725E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78582550E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.49307608E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58750492E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.01875465E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.61547807E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.00369142E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.37447703E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.99146777E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05862515E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.02207165E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.51586725E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.72268129E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41223297E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.58910606E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.83251676E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04818446E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.02112606E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.86423245E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.55540962E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44985596E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.79303079E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.90788204E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13600590E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.98481799E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     8.26267589E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.05499427E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79059603E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.30682124E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56409186E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13600590E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.98481799E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     8.26267589E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.05499427E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79059603E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.30682124E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56409186E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45460222E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.06682981E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.50304207E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.49833426E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53505511E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.46299391E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.05462605E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.09159277E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33736655E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33736655E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11246896E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11246896E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10032898E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.46339543E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.37996660E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.23290161E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.17775326E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.73928848E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.19941671E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.51320463E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.31279336E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.64576923E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.34092002E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.27014141E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18807127E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54141573E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18807127E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54141573E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35535996E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53639448E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53639448E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49012242E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07027940E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07027940E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07027931E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.92182659E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.92182659E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.92182659E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.92182659E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.30727644E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.30727644E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.30727644E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.30727644E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.26541355E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.26541355E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.07013844E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.08814060E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.47825265E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.66402291E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.11551583E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.66402291E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.11551583E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.02655310E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.52366078E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.52366078E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.57956186E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     9.87338278E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     9.87338278E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     9.87329829E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.11288061E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.74133145E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.11288061E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.74133145E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.64707795E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.28959762E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.28959762E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.02428051E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.25735530E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.25735530E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.25735531E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.18823383E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.18823383E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.18823383E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.18823383E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.96072522E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.96072522E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.96072522E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.96072522E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.84120227E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.84120227E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.46138551E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.04612945E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.14750690E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.54987324E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.51584078E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.51584078E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.20914427E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.56316694E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.85073087E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.63983312E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.63983312E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.64025521E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.64025521E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01943136E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88861593E-01    2           5        -5   # BR(h -> b       bb     )
     6.48743451E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29654223E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86946687E-04    2           3        -3   # BR(h -> s       sb     )
     2.11510257E-02    2           4        -4   # BR(h -> c       cb     )
     6.91390949E-02    2          21        21   # BR(h -> g       g      )
     2.39632490E-03    2          22        22   # BR(h -> gam     gam    )
     1.64855725E-03    2          22        23   # BR(h -> Z       gam    )
     2.22970017E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82424412E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.54893845E+01   # H decays
#          BR         NDA      ID1       ID2
     3.66805150E-01    2           5        -5   # BR(H -> b       bb     )
     5.97520069E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11268624E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49894347E-04    2           3        -3   # BR(H -> s       sb     )
     7.00827585E-08    2           4        -4   # BR(H -> c       cb     )
     7.02375156E-03    2           6        -6   # BR(H -> t       tb     )
     5.34290211E-07    2          21        21   # BR(H -> g       g      )
     9.15876338E-09    2          22        22   # BR(H -> gam     gam    )
     1.80846665E-09    2          23        22   # BR(H -> Z       gam    )
     1.62441370E-06    2          24       -24   # BR(H -> W+      W-     )
     8.11644187E-07    2          23        23   # BR(H -> Z       Z      )
     6.63848897E-06    2          25        25   # BR(H -> h       h      )
     3.25007371E-24    2          36        36   # BR(H -> A       A      )
     1.12148901E-20    2          23        36   # BR(H -> Z       A      )
     1.86482240E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.52025529E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.52025529E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.56269693E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.87072707E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70549387E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.02667421E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.06569736E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.28970893E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.69895910E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.27824517E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.68984838E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.11158441E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.27046688E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.17122803E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.17122803E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.30093811E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.54856299E+01   # A decays
#          BR         NDA      ID1       ID2
     3.66855736E-01    2           5        -5   # BR(A -> b       bb     )
     5.97563303E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11283744E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49952999E-04    2           3        -3   # BR(A -> s       sb     )
     7.05482355E-08    2           4        -4   # BR(A -> c       cb     )
     7.03845249E-03    2           6        -6   # BR(A -> t       tb     )
     1.44622357E-05    2          21        21   # BR(A -> g       g      )
     6.93518747E-08    2          22        22   # BR(A -> gam     gam    )
     1.59003835E-08    2          23        22   # BR(A -> Z       gam    )
     1.62110136E-06    2          23        25   # BR(A -> Z       h      )
     1.92257461E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.51999274E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.51999274E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.26666538E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.09428755E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.49586056E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.37914499E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     8.64412023E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.91504872E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.86105942E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.88955375E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.44209540E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.30413800E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.30413800E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.57464934E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.91447784E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.94924473E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10350719E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.78526263E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19167511E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45165891E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.76279963E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.61536231E-06    2          24        25   # BR(H+ -> W+      h      )
     3.14215283E-14    2          24        36   # BR(H+ -> W+      A      )
     4.24267644E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.32330850E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.91128828E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.51525992E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.18842700E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.50950850E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.11582141E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.08572558E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.49253769E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
