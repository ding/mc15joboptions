#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.24400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05410185E+01   # W+
        25     1.25000000E+02   # h
        35     2.00402295E+03   # H
        36     2.00000000E+03   # A
        37     2.00150757E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989244E+03   # ~u_L
   2000002     4.99994927E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989244E+03   # ~c_L
   2000004     4.99994927E+03   # ~c_R
   1000005     4.99957297E+03   # ~b_1
   2000005     5.00058670E+03   # ~b_2
   1000006     4.98954833E+03   # ~t_1
   2000006     5.01488720E+03   # ~t_2
   1000011     5.00008220E+03   # ~e_L
   2000011     5.00007609E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008220E+03   # ~mu_L
   2000013     5.00007609E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99974600E+03   # ~tau_1
   2000015     5.00041289E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.40000000E+03   # ~g
   1000022     1.20171233E+03   # ~chi_10
   1000023    -1.25008717E+03   # ~chi_20
   1000025     1.28888745E+03   # ~chi_30
   1000035     3.00348739E+03   # ~chi_40
   1000024     1.24662780E+03   # ~chi_1+
   1000037     3.00348536E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.17341456E-01   # N_11
  1  2    -2.99892646E-02   # N_12
  1  3     4.94135091E-01   # N_13
  1  4    -4.90257474E-01   # N_14
  2  1     3.50041610E-03   # N_21
  2  2    -3.65189910E-03   # N_22
  2  3    -7.06974457E-01   # N_23
  2  4    -7.07220990E-01   # N_24
  3  1     6.96712094E-01   # N_31
  3  2     3.24926378E-02   # N_32
  3  3    -5.05168406E-01   # N_33
  3  4     5.08272926E-01   # N_34
  4  1     1.11378460E-03   # N_41
  4  2    -9.99015283E-01   # N_42
  4  3    -2.86794438E-02   # N_43
  4  4     3.38336055E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.05320474E-02   # U_11
  1  2     9.99178239E-01   # U_12
  2  1     9.99178239E-01   # U_21
  2  2     4.05320474E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.78224577E-02   # V_11
  1  2     9.98855852E-01   # V_12
  2  1     9.98855852E-01   # V_21
  2  2     4.78224577E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07899014E-01   # cos(theta_t)
  1  2     7.06313660E-01   # sin(theta_t)
  2  1    -7.06313660E-01   # -sin(theta_t)
  2  2     7.07899014E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.68542249E-01   # cos(theta_b)
  1  2     7.43674163E-01   # sin(theta_b)
  2  1    -7.43674163E-01   # -sin(theta_b)
  2  2     6.68542249E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03860055E-01   # cos(theta_tau)
  1  2     7.10338668E-01   # sin(theta_tau)
  2  1    -7.10338668E-01   # -sin(theta_tau)
  2  2     7.03860055E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202969E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52154265E+02   # vev(Q)              
         4     3.43389888E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52784073E-01   # gprime(Q) DRbar
     2     6.27182998E-01   # g(Q) DRbar
     3     1.08619150E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02524826E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71787845E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79805223E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.24400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.29736438E+06   # M^2_Hd              
        22    -6.92401272E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37052204E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.42399911E-06   # gluino decays
#          BR         NDA      ID1       ID2
     1.52578939E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.45332753E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.19712505E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.26374609E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.85965428E-09    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.57953000E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     9.86151748E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.58412247E-09    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     6.07698206E-05    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.26374609E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.85965428E-09    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.57953000E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     9.86151748E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.58412247E-09    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     6.07698206E-05    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.22713210E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.22279414E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.66117831E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     3.76850259E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.76850259E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.76850259E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.76850259E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.36044185E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.33889604E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.62308664E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.75645277E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.00609349E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.00815401E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.99538966E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.61985413E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.27972595E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.35659170E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.53297493E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.65105991E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.35788817E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.08655317E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.69750287E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.63174292E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.57754356E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.23573373E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.40210082E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.22446899E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.25936684E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01472745E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.53734644E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.54025899E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.72289318E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.20069044E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.53401251E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.69217329E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.64698562E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.37121074E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.31906235E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.08230242E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.43958483E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.51626792E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.11314202E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.79753001E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.16561355E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.11828455E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.33186298E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02163638E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.29452826E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.11775321E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.99202536E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.96102859E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.35419814E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59211659E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.43962555E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.86198910E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.26449137E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.50639146E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.16870271E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.24010457E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.33683313E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02206738E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.22438110E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.46144825E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.28738783E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.05727604E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.07139739E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89481141E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.43958483E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.51626792E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.11314202E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.79753001E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.16561355E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.11828455E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.33186298E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02163638E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.29452826E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.11775321E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.99202536E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.96102859E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.35419814E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59211659E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.43962555E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.86198910E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.26449137E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.50639146E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.16870271E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.24010457E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.33683313E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02206738E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.22438110E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.46144825E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.28738783E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.05727604E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.07139739E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89481141E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.94774133E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.22047719E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.65217319E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.04178172E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.70280441E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.91805014E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.41416913E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.17866000E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.19203532E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.22388558E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.80783653E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.75977081E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.94774133E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.22047719E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.65217319E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.04178172E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.70280441E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.91805014E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.41416913E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.17866000E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.19203532E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.22388558E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.80783653E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.75977081E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.56758640E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.75211590E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.48547245E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.60513191E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.53332627E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.20179077E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.07152166E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.40088036E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.56765335E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.60254319E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.64118958E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67599053E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.56963082E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     9.98169277E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.14419610E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.94779512E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.10717044E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.84281653E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.47098419E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.70911871E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.66988345E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.40972932E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.94779512E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.10717044E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.84281653E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.47098419E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.70911871E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.66988345E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.40972932E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.95062075E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.10611017E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.84105178E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.46382969E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.70652435E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.62372721E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.40456114E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.16371517E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.46821347E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.29259100E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.27649118E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.09753164E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.09750148E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08906336E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.67459281E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53122132E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12688572E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46307563E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34073275E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53800829E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.62951642E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     6.73252139E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.98396188E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.90396737E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12070746E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.36618790E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.41436770E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.08774678E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.85726930E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.18920361E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.17196090E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.51748275E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16414322E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.51727170E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38346219E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.46553845E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.46541595E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.43096269E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.92078771E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.92078771E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.92078771E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.33719931E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.33719931E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.93259720E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.93259720E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.45733132E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.45733132E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.43665693E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.43665693E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.21046631E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.21046631E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.23005074E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.70775014E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.07935136E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     7.13114117E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.21507598E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.76131641E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.08225908E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.29071087E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.37782244E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.29624337E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.33900385E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.26524645E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.26522045E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.87594990E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.30457839E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.30457839E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.30457839E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.38624070E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.08976554E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.36339501E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.08911764E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.68395096E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.05624185E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.05589661E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.95846783E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.40914432E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.40914432E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.40914432E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31458535E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31458535E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30731016E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30731016E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.38194454E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.38194454E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.38180815E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.38180815E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.34369135E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.34369135E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.67461985E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.92089506E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51326801E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     9.13162099E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46390274E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46390274E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15290250E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67882384E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37210696E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82963276E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.79902972E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.61222062E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     6.60088776E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     9.53958774E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08489766E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17685968E-01    2           5        -5   # BR(h -> b       bb     )
     6.37610860E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25690762E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78259066E-04    2           3        -3   # BR(h -> s       sb     )
     2.06330444E-02    2           4        -4   # BR(h -> c       cb     )
     6.69959228E-02    2          21        21   # BR(h -> g       g      )
     2.30447516E-03    2          22        22   # BR(h -> gam     gam    )
     1.53779546E-03    2          22        23   # BR(h -> Z       gam    )
     2.00755660E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56220985E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78105229E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47128261E-03    2           5        -5   # BR(H -> b       bb     )
     2.46426263E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71208585E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546428E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667932E-05    2           4        -4   # BR(H -> c       cb     )
     9.96062981E-01    2           6        -6   # BR(H -> t       tb     )
     7.97667223E-04    2          21        21   # BR(H -> g       g      )
     2.70979443E-06    2          22        22   # BR(H -> gam     gam    )
     1.15999966E-06    2          23        22   # BR(H -> Z       gam    )
     3.34918329E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67002416E-04    2          23        23   # BR(H -> Z       Z      )
     9.03799130E-04    2          25        25   # BR(H -> h       h      )
     7.59918828E-24    2          36        36   # BR(H -> A       A      )
     3.00710028E-11    2          23        36   # BR(H -> Z       A      )
     7.00191028E-12    2          24       -37   # BR(H -> W+      H-     )
     7.00191028E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382535E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47418748E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897551E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62265951E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677317E-06    2           3        -3   # BR(A -> s       sb     )
     9.96175793E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995301E-01    2           6        -6   # BR(A -> t       tb     )
     9.43674993E-04    2          21        21   # BR(A -> g       g      )
     3.18686446E-06    2          22        22   # BR(A -> gam     gam    )
     1.35257375E-06    2          23        22   # BR(A -> Z       gam    )
     3.26438725E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472697E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36458408E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237033E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81142944E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50334520E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693151E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731430E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402013E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34561453E-04    2          24        25   # BR(H+ -> W+      h      )
     2.73478131E-13    2          24        36   # BR(H+ -> W+      A      )
