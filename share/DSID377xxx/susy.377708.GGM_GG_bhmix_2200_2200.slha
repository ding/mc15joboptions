#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.18200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.20000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05423841E+01   # W+
        25     1.25000000E+02   # h
        35     2.00398612E+03   # H
        36     2.00000000E+03   # A
        37     2.00152191E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002539E+03   # ~d_R
   1000002     4.99989247E+03   # ~u_L
   2000002     4.99994921E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002539E+03   # ~s_R
   1000004     4.99989247E+03   # ~c_L
   2000004     4.99994921E+03   # ~c_R
   1000005     4.99919223E+03   # ~b_1
   2000005     5.00096737E+03   # ~b_2
   1000006     4.97988259E+03   # ~t_1
   2000006     5.02448721E+03   # ~t_2
   1000011     5.00008214E+03   # ~e_L
   2000011     5.00007618E+03   # ~e_R
   1000012     4.99984168E+03   # ~nu_eL
   1000013     5.00008214E+03   # ~mu_L
   2000013     5.00007618E+03   # ~mu_R
   1000014     4.99984168E+03   # ~nu_muL
   1000015     4.99949240E+03   # ~tau_1
   2000015     5.00066648E+03   # ~tau_2
   1000016     4.99984168E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     2.14351768E+03   # ~chi_10
   1000023    -2.20006365E+03   # ~chi_20
   1000025     2.23103385E+03   # ~chi_30
   1000035     3.00751212E+03   # ~chi_40
   1000024     2.19260127E+03   # ~chi_1+
   1000037     3.00749121E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.48540528E-01   # N_11
  1  2    -5.97507780E-02   # N_12
  1  3     4.68180190E-01   # N_13
  1  4    -4.65751256E-01   # N_14
  2  1     1.99254403E-03   # N_21
  2  2    -2.98306936E-03   # N_22
  2  3    -7.07051086E-01   # N_23
  2  4    -7.07153373E-01   # N_24
  3  1     6.63066447E-01   # N_31
  3  2     7.51157842E-02   # N_32
  3  3    -5.25902328E-01   # N_33
  3  4     5.27377709E-01   # N_34
  4  1     5.09849554E-03   # N_41
  4  2    -9.95378604E-01   # N_42
  4  3    -6.56720103E-02   # N_43
  4  4     6.98758014E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.26145125E-02   # U_11
  1  2     9.95702040E-01   # U_12
  2  1     9.95702040E-01   # U_21
  2  2     9.26145125E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.85512196E-02   # V_11
  1  2     9.95131980E-01   # V_12
  2  1     9.95131980E-01   # V_21
  2  2     9.85512196E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07556194E-01   # cos(theta_t)
  1  2     7.06657083E-01   # sin(theta_t)
  2  1    -7.06657083E-01   # -sin(theta_t)
  2  2     7.07556194E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.85355848E-01   # cos(theta_b)
  1  2     7.28208323E-01   # sin(theta_b)
  2  1    -7.28208323E-01   # -sin(theta_b)
  2  2     6.85355848E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05312281E-01   # cos(theta_tau)
  1  2     7.08896739E-01   # sin(theta_tau)
  2  1    -7.08896739E-01   # -sin(theta_tau)
  2  2     7.05312281E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90194861E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.20000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52381281E+02   # vev(Q)              
         4     2.91518220E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52689103E-01   # gprime(Q) DRbar
     2     6.26581320E-01   # g(Q) DRbar
     3     1.07892817E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02450966E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71205334E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79703266E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.18200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -2.36822476E+06   # M^2_Hd              
        22    -9.85307330E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36783567E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.93364639E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.92823013E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.24127414E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.36088647E-07    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.24127414E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.36088647E-07    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.09804726E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.68875718E-13    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.68875718E-13    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.68875718E-13    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.68875718E-13    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.63330639E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     6.61542302E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.13411641E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.98946660E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.10031287E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.01031259E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.12657443E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.69309808E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.53191328E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.77029278E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.00277087E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.48671941E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.02785349E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     7.99358949E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.95980662E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.67589673E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.05683184E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.48712510E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.12352872E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.60448081E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.68469108E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.02343524E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.46513718E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.39995352E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.12043198E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.76372489E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.65076474E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.19941494E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.97406437E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.20195248E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.06093126E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.14405148E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96403804E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     4.16420995E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.25010887E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.45173114E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.87979056E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.21657024E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.77078671E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.79409080E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.79682739E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.20612279E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.52600415E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.66707463E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.27938565E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61267245E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96412177E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.56314905E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.03534075E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.49648126E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.89517681E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.07446822E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.78057673E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.79454496E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.74468243E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.68031282E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.92915064E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.29238064E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.61684595E-07    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.90027106E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96403804E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     4.16420995E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.25010887E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.45173114E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.87979056E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.21657024E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.77078671E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.79409080E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.79682739E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.20612279E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.52600415E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.66707463E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.27938565E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61267245E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96412177E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.56314905E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.03534075E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.49648126E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.89517681E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.07446822E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.78057673E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.79454496E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.74468243E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.68031282E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.92915064E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.29238064E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.61684595E-07    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.90027106E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80426114E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.06529527E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.56906154E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.97831652E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.79386344E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.79307314E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.62382896E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.62171157E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.69627752E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.93965698E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.30352152E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.61567086E-05    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80426114E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.06529527E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.56906154E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.97831652E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.79386344E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.79307314E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.62382896E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.62171157E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.69627752E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.93965698E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.30352152E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.61567086E-05    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.21729565E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.53106202E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.06059504E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.11355083E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.74763854E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     8.41098660E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.51795637E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.62178158E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.21530321E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.40856905E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.41209610E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.17665629E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.78761414E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.38287335E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.59791969E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80458023E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07346416E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.62723427E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.96861460E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.82550500E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.82235393E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.61586956E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80458023E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07346416E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.62723427E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.96861460E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.82550500E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.82235393E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.61586956E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80666808E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.07266563E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.62156045E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.96566240E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.82340314E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.55402251E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.61174855E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.04236770E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.43357300E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     1.09312325E-04    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     2.86104758E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.84939140E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     9.53683541E-02    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     9.53661721E-02    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.47549634E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.01567325E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.55151662E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.96665852E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.45826986E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.49334785E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.50010653E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.32558375E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.03822856E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.98558906E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.91246157E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.01239261E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
     6.79092171E-05    2     1000039        35   # BR(~chi_10 -> ~G        H)
     3.10203601E-06    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     3.21998678E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.56780440E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.37493849E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.52464338E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.75414664E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
     2.91411734E-06    2     1000039        35   # BR(~chi_20 -> ~G        H)
     8.04713600E-05    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.07513904E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.39227559E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.06951539E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.39213035E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.29936724E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.18035043E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.18025963E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.15474998E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.35174058E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.35174058E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.35174058E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.62840016E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.62840016E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.20940355E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.20940355E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.76133469E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.76133469E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.75256461E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.75256461E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     6.67420163E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     6.67420163E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
     8.86883946E-27    3     1000021        -2         2   # BR(~chi_20 -> ~g      ub      u)
     1.15960235E-26    3     1000021        -1         1   # BR(~chi_20 -> ~g      db      d)
     8.86883946E-27    3     1000021        -4         4   # BR(~chi_20 -> ~g      cb      c)
     1.15960235E-26    3     1000021        -3         3   # BR(~chi_20 -> ~g      sb      s)
#
#         PDG            Width
DECAY   1000025     1.79938908E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.63267745E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.46066430E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.33618926E-01    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.17448938E-01    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.69607677E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.31391054E-04    2     1000039        35   # BR(~chi_30 -> ~G        H)
     5.53325407E-06    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     2.73365287E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.31942245E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.51635621E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.37146467E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.32814178E-04    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     6.08637520E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     6.08630571E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.12255695E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.59295860E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.59295860E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.59295860E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.01301269E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.31182483E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     9.98667081E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.31140310E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.05308517E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.99658017E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.99636962E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.93658611E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     5.98469296E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     5.98469296E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     5.98469296E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.10461088E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.10461088E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.09720956E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.09720956E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.68203122E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.68203122E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.68189231E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.68189231E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.64313930E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.64313930E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     4.11095565E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     9.20072399E-09    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     4.11095565E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     9.20072399E-09    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     6.20903342E-09    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.01536016E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.09206401E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52676432E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.38557069E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.48021798E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.48021798E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.00330549E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.35076705E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.50457253E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.19894912E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.12505082E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.15779364E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.92042930E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.14684924E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08913071E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18125547E-01    2           5        -5   # BR(h -> b       bb     )
     6.36935389E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25451670E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77752626E-04    2           3        -3   # BR(h -> s       sb     )
     2.06119091E-02    2           4        -4   # BR(h -> c       cb     )
     6.69252276E-02    2          21        21   # BR(h -> g       g      )
     2.30078923E-03    2          22        22   # BR(h -> gam     gam    )
     1.53627776E-03    2          22        23   # BR(h -> Z       gam    )
     2.00507931E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55955756E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78084489E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46457153E-03    2           5        -5   # BR(H -> b       bb     )
     2.46437929E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71249827E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11552016E-06    2           3        -3   # BR(H -> s       sb     )
     1.00669423E-05    2           4        -4   # BR(H -> c       cb     )
     9.96076718E-01    2           6        -6   # BR(H -> t       tb     )
     7.97640609E-04    2          21        21   # BR(H -> g       g      )
     2.71738447E-06    2          22        22   # BR(H -> gam     gam    )
     1.16045738E-06    2          23        22   # BR(H -> Z       gam    )
     3.32454245E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65773772E-04    2          23        23   # BR(H -> Z       Z      )
     9.00472267E-04    2          25        25   # BR(H -> h       h      )
     7.13657272E-24    2          36        36   # BR(H -> A       A      )
     2.87221155E-11    2          23        36   # BR(H -> Z       A      )
     6.31855586E-12    2          24       -37   # BR(H -> W+      H-     )
     6.31855586E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82379022E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46747304E-03    2           5        -5   # BR(A -> b       bb     )
     2.43899792E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62273872E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678361E-06    2           3        -3   # BR(A -> s       sb     )
     9.96184944E-06    2           4        -4   # BR(A -> c       cb     )
     9.97004460E-01    2           6        -6   # BR(A -> t       tb     )
     9.43683662E-04    2          21        21   # BR(A -> g       g      )
     3.12945254E-06    2          22        22   # BR(A -> gam     gam    )
     1.35304637E-06    2          23        22   # BR(A -> Z       gam    )
     3.24040317E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74470657E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34968046E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49240176E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81154056E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49380679E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45699504E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732695E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404477E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32109286E-04    2          24        25   # BR(H+ -> W+      h      )
     2.86734232E-13    2          24        36   # BR(H+ -> W+      A      )
