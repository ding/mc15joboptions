evgenConfig.description = "Single photon with flat eta-phi and log Pt in [50, 2000] GeV"
evgenConfig.keywords = ["singleParticle", "photon"]
 
include("MC15JobOptions/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 22
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.LogSampler(50, 2000000.), eta=[-2.4, 2.4])

