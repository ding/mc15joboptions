from MadGraphControl.MadGraphUtils import *

mode=0

parameters={
	'frblock':{ 
		'Lambda':'1.000000e+03',
		'cosa':  '0.300000e+00',  #### 0 for CP odd 1 for CP even
		'kSM':   '1.000000e+00',  #### 0 for CP odd and CP even
		'kHll':  '1.000000e+00',
		'kAll':  '1.000000e+00',
		'kHaa':  '1.000000e+00',
		'kAaa':  '1.000000e+00',
		'kHza':  '1.000000e+00',
		'kAza':  '1.000000e+00',
                'kHgg':  '1.000000e+00',  ####	0 for CP odd 1 for CP even
                'kAgg':  '1.000000e+00',  ####	1 for CP odd 0 for CP even
		'kHzz':  '1.000000e+00',
		'kAzz':  '1.000000e+00',   
		'kHww':  '16.000000e+00',  #### 0 for CP odd 1 for CP even
		'kAww':  '16.000000e+00',  #### 1 for CP odd 0 for CP even
		'kHda':  '1.000000e+00',
		'kHdz':  '1.000000e+00',
		'kHdwR': '8.000000e+00',   
		'kHdwI': '8.000000e+00',
                }  
	}

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
higgsMass={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------

extras = { 'pdlabel':"'nn23lo1'",
           'lhaid':'247000',
           'req_acc':'0.001',
           'gridpack':'False',
	   'fixed_ren_scale':'True',
	   'fixed_fac_scale':'True',
           'reweight_scale' :'True', ### enable for scale uncertainties
           'rw_Rscale_down':'0.5',
           'rw_Rscale_up':'2.0',
           'rw_Fscale_down':'0.5',
           'rw_Fscale_up':'2.0', 
           'reweight_PDF':'True',
           'PDF_set_min':'247001',
           'PDF_set_max':'247101',
           'scale':'125', 
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',
	   'lhe_version':'2.0', 
           'auto_ptj_mjj':'True',
           'parton_shower':'PYTHIA8',
           'bwcutoff':'15',
	   'maxjetflavor':'5',
           'cut_decays':'T',  ####
           'ptj':'0',  
           'ptb':'0',
           'pta':'0',
           'ptl':'6',  
	   'ptl1min':'12.5',
	   'ptl2min':'6',
           'ptjmax':'-1',
           'ptbmax':'-1',
           'ptamax':'-1',
           'etaj':'5.0',
           'etab':'-1',
           'etaa':'3.0',
	   'etal':'3.0',
           'etajmin':'0',
           'etabmin':'0',
           'etaamin':'0',
           'mmaa':'0',
           'mmaamax':'-1',
           'mmbb':'0',
           'mmbbmax':'-1',
           'drjj':'0',
           'drbb':'0',
           'draa':'0',
           'drbj':'0',
           'draj':'0',
           'drab':'0',
           'drll':'0',
           'drjl':'0',
           'dral':'0',
           'drjjmax':'-1',
           'drbbmax':'-1',
           'draamax':'-1',
           'drbjmax':'-1',
           'drajmax':'-1',
           'drabmax':'-1',
           'ptonium':'0',
           'etaonium':'-1'}

### DSID lists (change once official runnumber is assigned.. same for name of this script)
test=[344300]
Proccard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in test:
    Proccard.write("""
    import model HC_NLO_X0_UFO-heft
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > x0 / t, x0 > w+ w- > e+ ve ta- vt~
    add process p p > x0 / t, x0 > w+ w- > ta+ vt e- ve~
    add process p p > x0 / t, x0 > w+ w- > mu+ vm ta- vt~
    add process p p > x0 / t, x0 > w+ w- > ta+ vt mu- vm~
    add process p p > x0 / t, x0 > w+ w- > ta+ vt ta- vt~
    output -f""")
    Proccard.close()
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
	beamEnergy = runArgs.ecmEnergy / 2.
else: 
	raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Setting the number of generated events to 'safefactor' times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------

safefactor = 2
nevents    = 5000*safefactor
if runArgs.maxEvents > 0:
	nevents=runArgs.maxEvents*safefactor

runName='run_01'
if hasattr(runArgs, 'outputTXTFile'):
	runName=runArgs.outputTXTFile
process_dir = new_process()

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used
#---------------------------------------------------------------------------------------------------
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras,xqcut=0.)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for EFT parameters, if not set there, default values are used
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------

build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card_new.dat',masses=higgsMass,params=parameters)

print_cards()
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz', lhe_version=1)


#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------

evgenConfig.generators += ["MadGraph", "Pythia8","EvtGen"]
evgenConfig.description = 'MadGraph_HWWCPmixed'
evgenConfig.keywords = ["BSMHiggs",  "CPmixing", "WW", "spin0"]
evgenConfig.contact = ['Dominik Duda <dominik.duda@cern.ch>']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO, tau decay only leptonically
#---------------------------------------------------------------------------------------------------

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ["15:onMode=off",
                            "15:onIfAny=11 13"]

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")

Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 12500.
Multi1TLeptonFilter.Etacut = 3.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 6000.
Multi2LLeptonFilter.Etacut = 3.0
Multi2LLeptonFilter.NLeptons = 2
filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter)"
