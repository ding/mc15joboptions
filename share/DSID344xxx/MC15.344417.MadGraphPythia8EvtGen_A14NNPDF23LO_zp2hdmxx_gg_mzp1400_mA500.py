include( 'MC15JobOptions/MadGraphControl_monoHiggs_zp2hdm_fixParam_Tb_gz.py' )
evgenConfig.description = 'Simplified Model of vector mediator in 2HDM for MonoHiggs(h->gg) with mZp='+str(mZp)+'GeV and mA0='+str(mA0)+'GeV'
evgenConfig.keywords = ['BSM', 'BSMHiggs', 'Higgs', 'diphoton','simplifiedModel']
evgenConfig.contact = ['Alvaro Lopez Solis <alopezs@lpnhe.in2p3.fr>']
genSeq.Pythia8.Commands += [
       '25:onMode=off',
       '25:onIfMatch = 22 22'
       ]

if not hasattr( filtSeq, 'DiPhotonFilter' ):
 from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
 filtSeq += DiPhotonFilter()
DiPhotonFilter = filtSeq.DiPhotonFilter
DiPhotonFilter.PtCut1st = 30000.
DiPhotonFilter.PtCut2nd = 25000.
