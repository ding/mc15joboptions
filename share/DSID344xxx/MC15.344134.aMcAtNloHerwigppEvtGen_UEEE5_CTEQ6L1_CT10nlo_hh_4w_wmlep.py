#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------


include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")
## To modify Higgs BR
cmds = """
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
set /Herwig/Particles/W+/W+->nu_mu,mu+;:OnOff Off
set /Herwig/Particles/W+/W+->nu_e,e+;:OnOff Off
set /Herwig/Particles/W+/W+->nu_tau,tau+;:OnOff Off
set /Herwig/Particles/W+/W+->u,dbar;:OnOff On
set /Herwig/Particles/W+/W+->c,sbar;:OnOff On
set /Herwig/Particles/W+/W+->sbar,u;:OnOff On
set /Herwig/Particles/W+/W+->c,dbar;:OnOff On
set /Herwig/Particles/W+/W+->bbar,c;:OnOff On
##set W- decay
set /Herwig/Particles/W-/W-->ubar,d;:OnOff Off
set /Herwig/Particles/W-/W-->cbar,s;:OnOff Off
set /Herwig/Particles/W-/W-->s,ubar;:OnOff Off
set /Herwig/Particles/W-/W-->cbar,d;:OnOff Off
set /Herwig/Particles/W-/W-->b,cbar;:OnOff Off
set /Herwig/Particles/W-/W-->nu_ebar,e-;:OnOff On
set /Herwig/Particles/W-/W-->nu_mubar,mu-;:OnOff On
set /Herwig/Particles/W-/W-->nu_taubar,tau-;:OnOff On
"""

from Herwigpp_i import config as hw
genSeq.Herwigpp.Commands += cmds.splitlines()
del cmds

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
if (runArgs.runNumber == 344134):
    evgenConfig.description = "SM diHiggs production, decay to WWWW, with MG5_aMC@NLOh, inclusive of box diagrami FF, W- to lep, W+ to had."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "WW"]

evgenConfig.contact = ['Biagio Di Miccol <Biagio.di.micco@cern.ch>']
evgenConfig.inputfilecheck = 'aMCatNLO_2.2.3.342053.hh_NLO_EFT_FF_HERWIGPP_CT10' 
