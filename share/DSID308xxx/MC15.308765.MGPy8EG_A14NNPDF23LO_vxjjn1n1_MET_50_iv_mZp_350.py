model="InelasticVectorEFT"
mDM1 = 175.
mDM2 = 700.
mZp = 350.
mHD = 125.
widthZp = 1.468759e+00
widthN2 = 1.565094e-01
filteff = 9.861933e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
