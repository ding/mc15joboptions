model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 880.
mZp = 850.
mHD = 125.
widthZp = 4.051237e+00
widthN2 = 3.764956e-01
filteff = 8.361204e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
