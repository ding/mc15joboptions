model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 730.
mZp = 700.
mHD = 125.
widthZp = 3.328931e+00
widthN2 = 2.148842e-01
filteff = 8.196721e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
