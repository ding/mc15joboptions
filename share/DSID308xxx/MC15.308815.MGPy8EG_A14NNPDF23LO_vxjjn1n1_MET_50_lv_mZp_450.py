model="LightVector"
mDM1 = 5.
mDM2 = 480.
mZp = 450.
mHD = 125.
widthZp = 2.088811e+00
widthN2 = 3.618066e-01
filteff = 7.496252e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
