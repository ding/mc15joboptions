include("MC15JobOptions/MadGraphControl_ttu_offshellV.py")

evgenConfig.contact  = ["sergey.senkin@cern.ch"]
evgenConfig.description = "Same-sign tt production (off-shell V) with mDM="+str(mDM)+"GeV and mMed="+str(mMed)+"GeV"
evgenConfig.keywords = ["exotic"]
