model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 930.
mZp = 900.
mHD = 125.
widthZp = 4.291147e+00
widthN2 = 4.444026e-01
filteff = 8.818342e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
