model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 950.
mHD = 950.
widthZp = 4.530812e+00
widthhd = 3.779720e-01
filteff = 9.861933e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
