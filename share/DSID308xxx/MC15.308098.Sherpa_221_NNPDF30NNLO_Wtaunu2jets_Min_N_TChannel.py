include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Electroweak Sherpa W+/W- -> tau nu + 2,3,4j@LO using Min_N_TChannels option."
evgenConfig.keywords = ["SM", "W", "tau", "jets", "VBF" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Wtaunu2jets_Min_N_TChannel"

evgenConfig.process="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  EXCLUSIVE_CLUSTER_MODE=1;

  % tags for process setup
  NJET:=2; QCUT:=15.;

  EW_TCHAN_MODE=1
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 -> 15 -16 93 93 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  Min_N_TChannels 1
  Integration_Error 0.05
  End process;

  Process 93 93 -> 16 -15 93 93 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  Min_N_TChannels 1
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  Mass 15 -16 40 E_CMS
  Mass 16 -15 40 E_CMS
  NJetFinder 2 15.0 0.0 0.4 -1
}(selector)

(model){
  MASSIVE[5]=1
}(model)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

