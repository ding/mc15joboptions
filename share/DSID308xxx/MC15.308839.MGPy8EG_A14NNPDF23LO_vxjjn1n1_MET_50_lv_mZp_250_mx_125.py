model="LightVector"
mDM1 = 125.
mDM2 = 500.
mZp = 250.
mHD = 125.
widthZp = 9.947182e-01
widthN2 = 1.642399e+01
filteff = 9.523810e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
