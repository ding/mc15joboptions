###############################################################
# Pythia8 minimum bias (SD) samples
#==============================================================

evgenConfig.description = "SD Minbias with the MBR model by Goulianos et al. tuned on CDF data"
evgenConfig.keywords = ["minBias"]
evgenConfig.generators = ["Pythia8"]

# ... Main generator : Pythia8
include ("MC15JobOptions/nonStandard/Pythia8_Monash_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]

# this uses the MBR model by Dino Goulianos etc. tuned on CDF data
genSeq.Pythia8.Commands += ["Diffraction:PomFlux = 5"]

